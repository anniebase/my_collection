﻿<?php
header('Content-Type:text/html; charset=utf-8');
//include_once("../db/sql_start.php");
include_once("../config.php");
require_once('../libraries/basic.php');    #基本函數

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><head>
		<meta charset="utf-8">
        <meta name="author" content="新據點廣告_媒體創意設計">
        <title>新據點廣告_媒體創意設計</title>
        <meta property="og:type" content="website" />
        <meta property="og:locale" content="utf-8" />
	<meta property="og:title" content="新據點廣告_媒體創意設計" />
	<meta property="og:site_name" content="新據點廣告_媒體創意設計" />
	    <link rel="shortcut icon" type="image/x-icon" href="icon.png">
	<link rel="apple-touch-icon" href="icon.png"/>
	<meta property="og:description" content="戶外大型大樓彩妝,戶外看板,戶外霓虹,活動燈箱車,整合行銷,媒體購買,標案" />
        <meta property="og:url" content="index.html" />
	<meta name="description" content="戶外大型大樓彩妝,戶外看板,戶外霓虹,活動燈箱車,整合行銷,媒體購買,標案" />

        <link rel="apple-touch-icon" href="../icon.png" />
        <link rel="shortcut icon" type="image/x-icon" href="../icon.png" />
        <link rel="icon" type="image/x-icon" href="../icon.png" />
	 
	
	<!-- !Meta -->
	    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
	<!-- /!Meta --> 
	
	<!-- !Load Fonts -->
<!-- /!Load Fonts -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-90539080-3', 'auto');
  ga('send', 'pageview');

</script>
	<!-- !Load Stylesheets -->
	<link href='../css/base2.css' rel='stylesheet' type='text/css'>
    <link href='../css/nav/navigation2.css' rel='stylesheet' type='text/css'>
    <link href='../css/base.css' rel='stylesheet' type='text/css'>
    <link href='../css/nav/navigation.css' rel='stylesheet' type='text/css'>

    <script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
    </script>
    <script>
    	
      $(function() {
            var nav_bar=$('#contentheader')

            nav_bar.hide();



    });
    </script>
<noscript>
        <link href='../css/noscript.css' rel='stylesheet' type='text/css'>
    </noscript>	<!-- /!Load Stylesheets -->
	
	<link href="../css/rc_modalvideoplayer.css" rel="stylesheet" type="text/css">
<link href="../css/categories/categories2.css" rel="stylesheet" type="text/css">
<link href="../css/categories/categories.css" rel="stylesheet" type="text/css">
	
	<link rel="stylesheet" href="colorbox.css" />
    
    
    <!-- Load JavaScripts -->


    <!--[if lt IE 9]>
        <script src="../js/lib/html5shiv-printshiv.min.js" type="text/javascript" charset="utf-8"></script>
    <![endif]--><!-- /Load JavaScripts -->



 <link rel="stylesheet" type="text/css" href="css/style.css" />
 <style type="text/css">
 #img_other_list ul li{
	float:left;
	}
.row1{ min-height:880px; height:880px;}
.carousel-content{ padding-top:15%;}
@media screen and (max-width:768px){
.row1{ min-height:auto;}
}

@media screen and (max-width:460px){
.row1{ min-height:auto;}
}
</style> 


<link rel="stylesheet" type="text/css" href="css/slider-pro.min.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="css/examples.css" media="screen"/>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="js/jquery.sliderPro.min.js"></script>
<script type="text/javascript">
	$( document ).ready(function( $ ) {
		$( '#example5' ).sliderPro({
			width: 770,
			height: 539,
			orientation: 'vertical',
			loop: false,
			arrows: true,
			buttons: false,
			thumbnailsPosition: 'right',
			thumbnailPointer: true,
			thumbnailWidth: 290,
			breakpoints: {
				800: {
					thumbnailsPosition: 'bottom',
					thumbnailWidth: 270,
					thumbnailHeight: 100
				},
				500: {
					thumbnailsPosition: 'bottom',
					thumbnailWidth: 120,
					thumbnailHeight: 50
				}
			}
		});
	});
</script>

            </head>
<body class="categories" onLoad="MM_preloadImages('../images/btn_top_on.gif')">
	<div class="head2">
 
	<nav id="globalheader" role="navigation">
    <?php include("../top_menu_04.php")?>
</nav>	</div>
		<!-- !BILLBOARD -->
<nav id="contentheader">
<div class="row cf">
<ul id="contentnav">
<li><a href="index.php" style="color: #09B5FF; font-size: 17px;" >7月份</a></li>
<li><a href="index201906.php" >6月份</a></li>
</ul>
</div>
</nav>



<div id="example5" class=" slider-pro">

  <div class="sp-slides"> 
   <div class="sp-slide">
   <a href="show/20190601.php" class='iframe'> 
   <img class="sp-image" src="css/images/blank.gif" data-src="images/20190601 (1).jpg"/>
     <div class="sp-caption">明曜感謝祭 108.06</div>
     </a> 
    </div>

    <div class="sp-slide">
   <a href="show/20190609.php" class='iframe'> 
   <img class="sp-image" src="css/images/blank.gif" data-src="images/20190609 (1).jpg"/>
     <div class="sp-caption">HTC 108.06</div>
     </a> 
    </div>

    <div class="sp-slide">
   <a href="show/20190608.php" class='iframe'> 
   <img class="sp-image" src="css/images/blank.gif" data-src="images/20190608 (1).jpg"/>
     <div class="sp-caption">中欣開發 108.06</div>
     </a> 
    </div>

    <div class="sp-slide">
   <a href="show/20190607.php" class='iframe'> 
   <img class="sp-image" src="css/images/blank.gif" data-src="images/20190607 (1).jpg"/>
     <div class="sp-caption">鐵三角 108.06</div>
     </a> 
    </div>

    <div class="sp-slide">
   <a href="show/20190606.php" class='iframe'> 
   <img class="sp-image" src="css/images/blank.gif" data-src="images/20190606 (1).jpg"/>
     <div class="sp-caption">生活市集 108.06</div>
     </a> 
    </div>

    <div class="sp-slide">
   <a href="show/20190605.php" class='iframe'> 
   <img class="sp-image" src="css/images/blank.gif" data-src="images/20190605 (1).jpg"/>
     <div class="sp-caption">任天堂明星大亂鬥 108.06</div>
     </a> 
    </div>
  </div>
    
  
  
<div class="sp-thumbnails">

<div class="sp-thumbnail">
      <div class="sp-thumbnail-image-container"> 
      <img class="sp-thumbnail-image" src="images/20190601 (1).jpg"/> </div>
      <div class="sp-thumbnail-text">
        <div class="sp-thumbnail-title">明曜感謝祭</div>
        <div class="sp-thumbnail-description">106.06</div>
      </div>
    </div>
<div class="sp-thumbnail">
      <div class="sp-thumbnail-image-container"> 
      <img class="sp-thumbnail-image" src="images/20190609 (1).jpg"/> </div>
      <div class="sp-thumbnail-text">
        <div class="sp-thumbnail-title">HTC</div>
        <div class="sp-thumbnail-description">107.12</div>
      </div>
    </div>
<div class="sp-thumbnail">
      <div class="sp-thumbnail-image-container"> 
      <img class="sp-thumbnail-image" src="images/20190608 (1).jpg"/> </div>
      <div class="sp-thumbnail-text">
        <div class="sp-thumbnail-title">中欣開發</div>
        <div class="sp-thumbnail-description">107.12</div>
      </div>
    </div>
<div class="sp-thumbnail">
      <div class="sp-thumbnail-image-container"> 
      <img class="sp-thumbnail-image" src="images/20190607 (1).jpg"/> </div>
      <div class="sp-thumbnail-text">
        <div class="sp-thumbnail-title">鐵三角</div>
        <div class="sp-thumbnail-description">107.12</div>
      </div>
    </div>
<div class="sp-thumbnail">
      <div class="sp-thumbnail-image-container"> 
      <img class="sp-thumbnail-image" src="images/20190606 (1).jpg"/> </div>
      <div class="sp-thumbnail-text">
        <div class="sp-thumbnail-title">生活市集</div>
        <div class="sp-thumbnail-description">107.12</div>
      </div>
    </div>
<div class="sp-thumbnail">
      <div class="sp-thumbnail-image-container"> 
      <img class="sp-thumbnail-image" src="images/20190605 (1).jpg"/> </div>
      <div class="sp-thumbnail-text">
        <div class="sp-thumbnail-title">任天堂明星大亂鬥</div>
        <div class="sp-thumbnail-description">107.12</div>
      </div>
    </div>


</div>

</div>


      <!-- /!BILLBOARD --><!-- !INTRO -->
<!--<section class="intro">    <h1>精品服飾 - Boutiques</h1>
            <p>流行服飾、時尚背包、手錶、香水、配件飾品...</p>
    </section>//--><!-- /!INTRO --><!-- !NAVIGATION -->  
<!--<span class="billboard-content carousel-content"><a href="#"><img src="../images/b5.jpg" alt="" class="billboard-hero" /></a></span>//-->

<!-- /!NAVIGATION --><!-- !MODAL VIDEO PLAYER -->            
<div id="modal-youtube" class="modal-video-player">
    <div class="video-container youtube-video-container">
    </div>
    <i class="button-close"></i></div><!-- /!MODAL VIDEO PLAYER -->	
<div class="iw_wrapper">  
  
<ul class="iw_thumbs" id="iw_thumbs">
<li image_id=""><a href="./show/20190601.php" class='iframe'><img src="images/20190601 (1).jpg"  /><br>
<span style="font-weight: normal; font-size: 10pt; color: #09B5FF;">明曜感謝祭</span></a></li>
<li image_id=""><a href="./show/20190609.php" class='iframe'><img src="images/20190609 (1).jpg"  /><br>
<span style="font-weight: normal; font-size: 10pt; color: #09B5FF;">HTC</span></a></li>
<li image_id=""><a href="./show/20190608.php" class='iframe'><img src="images/20190608 (1).jpg"  /><br>
<span style="font-weight: normal; font-size: 10pt; color: #09B5FF;">中欣開發</span></a></li>
<li image_id=""><a href="./show/20190607.php" class='iframe'><img src="images/20190607 (1).jpg"  /><br>
<span style="font-weight: normal; font-size: 10pt; color: #09B5FF;">鐵三角</span></a></li>
<li image_id=""><a href="./show/20190606.php" class='iframe'><img src="images/20190606 (1).jpg"  /><br>
<span style="font-weight: normal; font-size: 10pt; color: #09B5FF;">生活市集</span></a></li>
<li image_id=""><a href="./show/20190605.php" class='iframe'><img src="images/20190605 (1).jpg"  /><br>
<span style="font-weight: normal; font-size: 10pt; color: #09B5FF;">任天堂明星大亂鬥</span></a></li>


</ul>
</div>
<div id="iw_ribbon" class="iw_ribbon">
            <span id="photolist"></span>
				<span class="iw_close"></span>
				<span class="iw_zoom"></span>
		
</div>
<!-- !PROMOS -->
<!--<aside id="promos">
	<div class="row">
      <div class="flushrow">
      <?php //include("../news_list.php")?>
       <ul class="carousel-container cf">
		<?php $sql_news_list = sprintf("SELECT * FROM web_news where `news_published`='%s' order by news_id desc",mysql_real_escape_string("1"));
$result_news_list = mysql_query($sql_news_list);
while($newsRS=mysql_fetch_array($result_news_list)){

$result_image_other = mysql_query(sprintf("select * FROM photo_list where p_id='%s' and lan='%s' and `type_name`='%s' order by id ASC LIMIT 1",mysql_real_escape_string($newsRS["news_id"]),mysql_real_escape_string("cn"),mysql_real_escape_string("news")));
$ps_other_RS=mysql_fetch_array($result_image_other);
$src="../upload/news/".$ps_other_RS['image_name_s']."";
?>
        <li>
		    <a href="../about/news_post.php?news_id=<?php echo $newsRS["news_id"]?>" >
			<figure>
	
			 <img src="<?php echo $src?>" alt="Roland Featured Products" />
			    <figcaption class="viewmore"><?php echo $newsRS["news_subject"]?></figcaption>
			</figure>
		    </a>
		</li>
        <?php }?>
		</ul>
	    </div>
  </div>
  <nav class="paddle-nav">
	    <ul>
		<li><a href="javascript:void(0)" class="paddle paddle-left carousel-prev">&lt;</a></li>
		<li><a href="javascript:void(0)" class="paddle paddle-right carousel-next">&gt;</a></li>
	    </ul>
	</nav>
</aside>//-->
<!-- !PROMOS -->	<!-- !BACK TO TOP -->
<div class="btn-top"> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image15','','../images/btn_top_on.gif',1)"><img src="../images/btn_top.gif" width="68" height="68" id="Image15"></a></div>
<!-- /!BACK TO TOP -->
<footer id="globalfooter" role="contentinfo">
  <?php include("../footer_.php")?>
</footer>
<!-- Google Tag Manager -->


<!-- End Google Tag Manager -->   
	<script type="text/javascript" src="js/jquery.masonry.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript">
			$(window).load(function(){
				var $iw_thumbs			= $('#iw_thumbs'),
					$iw_ribbon			= $('#iw_ribbon'),
					$iw_ribbon_close	= $iw_ribbon.children('span.iw_close'),
					$iw_ribbon_zoom		= $iw_ribbon.children('span.iw_zoom');
					
					ImageWall	= (function() {
							// window width and height
						var w_dim,
						    // index of current image
							current				= -1,
							isRibbonShown		= false,
							isFullMode			= false,
							// ribbon / images animation settings
							ribbonAnim			= {speed : 500, easing : 'easeOutExpo'},
							imgAnim				= {speed : 400, easing : 'jswing'},
							// init function : call masonry, calculate window dimentions, initialize some events
							init				= function() {
								$iw_thumbs.imagesLoaded(function(){
									$iw_thumbs.masonry({
										isAnimated	: true
									});
								});
								getWindowsDim();
								initEventsHandler();
							},
							// calculate window dimentions
							getWindowsDim		= function() {
								w_dim = {
									width	: $(window).width(),
									height	: $(window).height()
								};
							},
							// initialize some events
							initEventsHandler	= function() {
								
								// click on a image
								$iw_thumbs.delegate('li', 'click', function() {
									
									return;//中斷原本的指令 20150820 chung
									
									
									if($iw_ribbon.is(':animated')) return false;
									
									var $el = $(this);
				
									if($el.data('ribbon')) {
										showFullImage($el);
										
								  
									}
									else if(!isRibbonShown) {
										isRibbonShown = true;
										
										$el.data('ribbon',true);
										
										// set the current
										current = $el.index();
									
										showRibbon($el);
									}
								});
								
								// click ribbon close
								$iw_ribbon_close.bind('click', closeRibbon);
								
								// on window resize we need to recalculate the window dimentions
								$(window).bind('resize', function() {
											getWindowsDim();
											if($iw_ribbon.is(':animated'))
												return false;
											closeRibbon();
										 })
								         .bind('scroll', function() {
											if($iw_ribbon.is(':animated'))
												return false;
											closeRibbon();
										 });
								
							},
							showRibbon			= function($el) {
								var	$img	= $el.children('img'),
									$descrp	= $img.next();
								
								// fadeOut all the other images
								$iw_thumbs.children('li').not($el).animate({opacity : 0.2}, imgAnim.speed);
								
								// increase the image z-index, and set the height to 100px (default height)
								$img.css('z-index', 100)
									.data('originalHeight',$img.height())
									.stop()
									.animate({
										height 		: '200px'
									}, imgAnim.speed, imgAnim.easing);
								
								// the ribbon will animate from the left or right
								// depending on the position of the image
								var ribbonCssParam 		= {
										top	: $el.offset().top - $(window).scrollTop() - 6 + 'px'
									},
									descriptionCssParam,
									dir;
								
								if( $el.offset().left < (w_dim.width / 2) ) {
									dir = 'left';
									ribbonCssParam.left 	= 0;
									ribbonCssParam.right 	= 'auto';
								}
								else {
									dir = 'right';
									ribbonCssParam.right 	= 0;
									ribbonCssParam.left 	= 'auto';
								}
								
								$iw_ribbon.css(ribbonCssParam)
								          .show()
										  .stop()
										  .animate({width : '100%'}, ribbonAnim.speed, ribbonAnim.easing, function() {
												switch(dir) {
													case 'left' :
														descriptionCssParam		= {
															'left' 			: $img.outerWidth(true) + 'px',
															'text-align' 	: 'left'

														};
														break;
													case 'right' :	
														descriptionCssParam		= {
															'left' 			: '-200px',
															'text-align' 	: 'right'
														};
														break;
												};
												$descrp.css(descriptionCssParam).fadeIn();
												// show close button and zoom
												$iw_ribbon_close.show();
												$iw_ribbon_zoom.show();
										  });
								
							},
							// close the ribbon
							// when in full mode slides in the middle of the page
							// when not slides left
							closeRibbon			= function() {
								isRibbonShown 	= false
								
								$iw_ribbon_close.hide();
								$iw_ribbon_zoom.hide();
								
								if(!isFullMode) {
								
									// current wall image
									var $el	 		= $iw_thumbs.children('li').eq(current);
									
									resetWall($el);
									
									// slide out ribbon
									$iw_ribbon.stop()
											  .animate({width : '0%'}, ribbonAnim.speed, ribbonAnim.easing); 
										  
								}
								else {
									$iw_ribbon.stop().animate({
										opacity		: 0.8,
										height 		: '0px',
										marginTop	: w_dim.height/2 + 'px' // half of window height
									}, ribbonAnim.speed, function() {
										$iw_ribbon.css({
											'width'		: '0%',
											'height'	: '226px',
											'margin-top': '0px'
										}).children('img').remove();
									});
									
									isFullMode	= false;
								}
							},
							resetWall			= function($el) {
								var $img		= $el.children('img'),
									$descrp		= $img.next();
									
								$el.data('ribbon',false);
								
								// reset the image z-index and height
								$img.css('z-index',1).stop().animate({
									height 		: $img.data('originalHeight')
								}, imgAnim.speed,imgAnim.easing);
								
								// fadeOut the description
								$descrp.fadeOut();

								// fadeIn all the other images
								$iw_thumbs.children('li').not($el).animate({opacity : 1}, imgAnim.speed);								
							},
							showFullImage		= function($el) {
								
								
								//alert(1)
								
								isFullMode	= true;
								
								$iw_ribbon_close.hide();
								
								var	$img	= $el.children('img'),
									large	= $img.data('img'),
								
									// add a loading image on top of the image
									$loading = $('<span class="iw_loading"></span>');
								
								$el.append($loading);
								
								// preload large image
								$('<img/>').load(function() {
									var $largeImage	= $(this);
									
									$loading.remove();
									
									$iw_ribbon_zoom.hide();
									
									resizeImage($largeImage);
									
									// reset the current image in the wall
									resetWall($el);
									
									// animate ribbon in and out
									$iw_ribbon.stop().animate({
										opacity		: 1,
										height 		: '0px',
										marginTop	: '63px' // half of ribbons height
									}, ribbonAnim.speed, function() {
										// add the large image to the DOM
										$iw_ribbon.prepend($largeImage);
										
										$iw_ribbon_close.show();
										
										$iw_ribbon.animate({
											height 		: '100%',
											marginTop	: '0px',
											top			: '0px'
										}, ribbonAnim.speed);
									});
								}).attr('src',large);
									
							},
							resizeImage			= function($image) {
								var widthMargin		= 100,
									heightMargin 	= 100,
								
									windowH      	= w_dim.height - heightMargin,
									windowW      	= w_dim.width - widthMargin,
									theImage     	= new Image();
									
								theImage.src     	= $image.attr("src");
								
								var imgwidth     	= theImage.width,
									imgheight    	= theImage.height;

								if((imgwidth > windowW) || (imgheight > windowH)) {
									if(imgwidth > imgheight) {
										var newwidth 	= windowW,
											ratio 		= imgwidth / windowW,

											newheight 	= imgheight / ratio;
											
										theImage.height = newheight;
										theImage.width	= newwidth;
										
										if(newheight > windowH) {
											var newnewheight 	= windowH,
												newratio 		= newheight/windowH,
												newnewwidth 	= newwidth/newratio;
										
											theImage.width 		= newnewwidth;
											theImage.height		= newnewheight;
										}
									}
									else {
										var newheight 	= windowH,
											ratio 		= imgheight / windowH,
											newwidth 	= imgwidth / ratio;
										
										theImage.height = newheight;
										theImage.width	= newwidth;
										
										if(newwidth > windowW) {
											var newnewwidth 	= windowW,
											    newratio 		= newwidth/windowW,
												newnewheight 	= newheight/newratio;
									
											theImage.height 	= newnewheight;
											theImage.width		= newnewwidth;
										}
									}
								}
									
								$image.css({
									'width'			: theImage.width + 'px',
									'height'		: theImage.height + 'px',
									'margin-left'	: -theImage.width / 2 + 'px',
									'margin-top'	: -theImage.height / 2 + 'px'
								});							
							};
							
						return {init : init};	
					})();
				
				ImageWall.init();
			});
		</script> </body>


</html>


		<script src="./js/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				$(".group1").colorbox({rel:'group1'});
				$(".group2").colorbox({rel:'group2', transition:"fade"});
				$(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
				$(".group4").colorbox({rel:'group4', slideshow:true});
				$(".ajax").colorbox();
				$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
				$(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
				$(".iframe").colorbox({iframe:true, width:"98%", height:"98%"});
				$(".inline").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});

				$('.non-retina').colorbox({rel:'group5', transition:'none'})
				$('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});
				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>