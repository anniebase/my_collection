﻿var ecmSetConfig = {};
ecmSetConfig.now = new Date();
ecmSetConfig.todayDate = ecmSetConfig.now.getDate();
ecmSetConfig.month = ecmSetConfig.now.getMonth()+1;
ecmSetConfig.Year = ecmSetConfig.now.getFullYear();
ecmSetConfig.today = ecmSetConfig.Year + "/" + ((ecmSetConfig.month.toString().length < 2)?"0":"") + ecmSetConfig.month + "/" + ((ecmSetConfig.todayDate.toString().length < 2)?"0":"") + ecmSetConfig.todayDate;//取得圖片位置

ecmSetConfig.itjsSrc = momoj('#itjs').attr('src');
ecmSetConfig.imgEcm = ecmSetConfig.itjsSrc.substring(0,ecmSetConfig.itjsSrc.indexOf("images\/"));
ecmSetConfig.timestamp = ecmSetConfig.itjsSrc.split('?')[1];
// 圖檔路徑
function getImgPath(imgName){
  return ecmSetConfig.imgEcm + 'images/' + imgName + '?' + ecmSetConfig.timestamp;
}

ecmSetConfig.DailyReg_mpn = 'M20180301002';
ecmSetConfig.buy9999_mpn = 'M20180301018';
ecmSetConfig.DailyBuy_mpn = 'M20180301019';
ecmSetConfig.DailyReg = 'D20180301003';
ecmSetConfig.game = 'D20180301004';
ecmSetConfig.giftCodeMap = {
  'M20180301018_D20180301001' : 'power',
  'M20180301018_D20180301002' : 'pillow',
  'M20180301018_D20180301003' : 'bag',
  'M20180301018_D20180301004' : 'pot',
  'M20180301018_D20180301005' : 'bag',
  'M20180301018_D20180301006' : 'spacebag',
  'M20180301018_D20180301007' : 'latexpillow',
  'M20180301019_D20180301001' : 'Switch',
  'M20180301019_D20180301002' : 'ticket'
};
ecmSetConfig.qryMsg = {
  'M20180301018_D20180301001_power'       : '已登記[滿額加購行動電源]',
  'M20180301018_D20180301002_pillow'      : '已登記[滿額加購棉麻抱枕]',
  'M20180301018_D20180301003_bag'         : '已登記[滿額加購護手霜禮盒]',
  'M20180301018_D20180301004_pot'         : '已登記[滿額加購康寧湯鍋]',
  'M20180301018_D20180301005_bag'         : '已登記[滿額加購文青風帆布袋]',
  'M20180301018_D20180301006_spacebag'    : '已登記[滿額加購7件組旅行收納袋]',
  'M20180301018_D20180301007_latexpillow' : '已登記[滿額加購泰國乳膠枕]',
  'M20180301019_D20180301001_Switch'      : '已登記[消費抽Switch]',
  'M20180301019_D20180301002_ticket'      : '已登記[消費抽雲品飯店]',
  'M20180301002_D20180301003_DEFAULT_GIFT': '簽到禮[銘謝惠顧]',
  'M20180301002_D20180301003_D333_990'    : '簽到禮[$333折價券]',
  'M20180301002_D20180301003_D500_1600'   : '簽到禮[$500折價券]',
  'M20180301002_D20180301003_D666_1999'   : '簽到禮[$666折價券]',
  'M20180301002_D20180301003_D888_3333'   : '簽到禮[$888折價券]',
  'M20180301002_D20180301003_D1300_9999'  : '簽到禮[$1300折價券]',
  'M20180301002_D20180301004_DEFAULT_GIFT': '拉霸加碼[$555折價券]',
  'M20180301002_D20180301004_D400_999'    : '拉霸加碼[$400折價券]',
  'M20180301002_D20180301004_gift'        : '拉霸加碼[小福袋]',
  'M20180301002_D20180301004_cafe'        : '拉霸加碼[全家中杯拿鐵]',
  'M20180301002_D20180301004_Donut'       : '拉霸加碼[Mister Donut甜甜圈]',
  'M20180301002_D20180301004_postman'     : '拉霸加碼[Snoopy-快樂小郵差]',
  'M20180301002_D20180301004_phone'       : '拉霸加碼[Snoopy-紅色電話亭]',
  'M20180301002_D20180301004_movie'       : '拉霸加碼[威秀電影票]',
};
ecmSetConfig.imgSrc = {
  'M20180301002_D20180301003_DEFAULT_GIFT': 'coupons_0.jpg',
  'M20180301002_D20180301003_D333_990'    : 'coupons_333.gif',
  'M20180301002_D20180301003_D500_1600'   : 'coupons_500.gif',
  'M20180301002_D20180301003_D666_1999'   : 'coupons_666.gif',
  'M20180301002_D20180301003_D888_3333'   : 'coupons_888.gif',
  'M20180301002_D20180301003_D1300_9999'  : 'coupons_1300.gif',
  'M20180301002_D20180301004_FULL'        : 'slot_end.jpg',
  'M20180301002_D20180301004_DEFAULT_GIFT': 'slot_get_555.gif',
  'M20180301002_D20180301004_D400_999'    : 'slot_get_400.gif',
  'M20180301002_D20180301004_gift'        : 'slot_get_bag.gif',
  'M20180301002_D20180301004_cafe'        : 'slot_get_FamilyMart.gif',
  'M20180301002_D20180301004_Donut'       : 'slot_get_MisterDonut.gif',
  'M20180301002_D20180301004_postman'     : 'slot_get_Snoopy_2.gif',
  'M20180301002_D20180301004_phone'       : 'slot_get_Snoopy_1.gif',
  'M20180301002_D20180301004_movie'       : 'slot_get_VIESHOW.gif',
};
ecmSetConfig.A_msg = {
  'M20180301002_D20180301003' : '今日已經簽到過囉！',
  'M20180301002_D20180301004' : '今日已經玩過囉！',
  'M20180301018_D20180301001' : '本週已登記過囉！',
  'M20180301018_D20180301002' : '本週已登記過囉！',
  'M20180301018_D20180301003' : '本週已登記過囉！',
  'M20180301018_D20180301004' : '本週已登記過囉！',
  'M20180301018_D20180301005' : '本週已登記過囉！',
  'M20180301018_D20180301006' : '本週已登記過囉！',
  'M20180301018_D20180301007' : '本週已登記過囉！',
  'M20180301019_D20180301001' : '今日已經登記過囉!',
  'M20180301019_D20180301002' : '今日已經登記過囉!',
};
ecmSetConfig.EA_msg = {//不符資格文案
  'M20180301002_D20180301004' : '累積簽到達第6天，當日可玩一次加碼拉霸，隔日將重新累計，逾期未玩視同放棄。\n您尚不符合資格喔！',
  'M20180301018_D20180301001' : '您尚不符合資格喔！\n本週消費滿額可登記一次，已排除取消的訂單及退貨訂單。',
  'M20180301018_D20180301002' : '您尚不符合資格喔！\n本週消費滿額可登記一次，已排除取消的訂單及退貨訂單。',
  'M20180301018_D20180301003' : '您尚不符合資格喔！\n本週消費滿額可登記一次，已排除取消的訂單及退貨訂單。',
  'M20180301018_D20180301004' : '您尚不符合資格喔！\n本週消費滿額可登記一次，已排除取消的訂單及退貨訂單。',
  'M20180301018_D20180301005' : '您尚不符合資格喔！\n本週消費滿額可登記一次，已排除取消的訂單及退貨訂單。',
  'M20180301018_D20180301006' : '您尚不符合資格喔！\n本週消費滿額可登記一次，已排除取消的訂單及退貨訂單。',
  'M20180301018_D20180301007' : '您尚不符合資格喔！\n本週消費滿額可登記一次，已排除取消的訂單及退貨訂單。',
  'M20180301019_D20180301001' : '您目前無符合資格訂單喔！\n消費一筆可登記一次，此活動當日限登記一次，如退貨或取消訂單則取消資格。',
  'M20180301019_D20180301002' : '您目前無符合資格訂單喔！\n消費一筆可登記一次，此活動當日限登記一次，如退貨或取消訂單則取消資格。',
};
ecmSetConfig.A_EX_msg = {//已登記過其他活動文案
  'M20180301018_D20180301001' : '本週您已登記過其他加價購活動！',
  'M20180301018_D20180301002' : '本週您已登記過其他加價購活動！',
  'M20180301018_D20180301003' : '本週您已登記過其他加價購活動！',
  'M20180301018_D20180301004' : '本週您已登記過其他加價購活動！',
  'M20180301018_D20180301005' : '本週您已登記過其他加價購活動！',
  'M20180301018_D20180301006' : '本週您已登記過其他加價購活動！',
  'M20180301018_D20180301007' : '本週您已登記過其他加價購活動！',
  'M20180301019_D20180301001' : '好禮2選1，今日您已登記過其他活動！',
  'M20180301019_D20180301002' : '好禮2選1，今日您已登記過其他活動！',
};
ecmSetConfig.INS_msg = {//登記成功文案
  'M20180301002_D20180301003' : '簽到成功',
  'M20180301018_D20180301001' : '您已登記成功，卡娜赫拉行動電源折價券將於4/20前歸戶完畢!',
  'M20180301018_D20180301002' : '您已登記成功，大象抱枕折價券將於4/20前歸戶完畢!',
  'M20180301018_D20180301003' : '您已登記成功，瑰珀翠護手霜禮盒2入折價券將於4/20前歸戶完畢!',
  'M20180301018_D20180301004' : '您已登記成功，康寧晶鑽鍋折價券將於4/20前歸戶完畢!',
  'M20180301018_D20180301005' : '您已登記成功，文青風帆布袋折價券將於4/20前歸戶完畢!',
  'M20180301018_D20180301006' : '您已登記成功，7件組旅行收納袋折價券將於4/20前歸戶完畢!',
  'M20180301018_D20180301007' : '您已登記成功，泰國乳膠枕折價券將於4/20前歸戶完畢!',
  'M20180301019_D20180301001' : '您已登記成功，此活動當日限登記一次，如退貨或取消訂單則取消資格。',
  'M20180301019_D20180301002' : '您已登記成功，此活動當日限登記一次，如退貨或取消訂單則取消資格。',
};
ecmSetConfig.returnMsg = {
  'D'        : '請於活動時間內參加活動',
  'W'        : '請於指定星期參加活動',
  'WP'       : '競標金額錯誤',
  'L'        : '請先登入會員',
  'A'        : '您已登記過此活動',
  'A_EX'     : '您已登記過其他活動',
  'EA'       : '您不符合登記資格',
  'FULL'     : '登記已額滿',
  'NOT_USED' : '很抱歉，活動暫不開放',
  'NOT_APP'  : '請在momo APP參加活動',
  'NOT_WEB'  : '請在momo網頁版參加活動',
  'NOT_NC'   : '您非活動期間新客',
  'NOT_WFB'  : '您非活動期間首購',
  'NO_PT'    : '點數不足',
  'INS'      : '登記成功，感謝您對本活動的支持',
};
ecmSetConfig.promoAjaxKey = false;
function promoAjax(data){
  if(!ecmSetConfig.promoAjaxKey){
    ecmSetConfig.promoAjaxKey = true;
    var result = '-1';
    momoj.ajax({
      url         : '/ajax/promoMech.jsp',
      async       : false,
      cache       : false,
      type        : 'POST',
      dataType    : 'json',
      contentType : 'application/x-www-form-urlencoded; charset=big5',
      data        : data,
      timeout     : 30000,
      success     : function(rtnData){
        ecmSetConfig.promoAjaxKey = false;
        result = rtnData;
      },
      error       : function(err, msg1, msg2){
        ecmSetConfig.promoAjaxKey = false;
        alert('ERROR\n很抱歉！伺服器暫時無法連線，請稍候再試');
      }
    });
    return result;
  }
}

/* ▼剩餘可登記人數 */
ecmSetConfig.gameFULL = false;
function getRegNum(mpn, dtpnArr){
  var data = {
    doAction    : 'cnt',
    m_promo_no  : mpn,
    dt_promo_no : dtpnArr.toString(),
    cnt_type    : '1002',//#1002: 指定多個(已登記人數), #1006: 指定多個(剩餘可登記人數), #1007: 所有(剩餘可登記人數)
  };
  
  var rtnData = promoAjax(data);
  if(rtnData != '-1'){
    var returnMsg = rtnData.returnMsg;
    if(returnMsg == 'OK'){
      for(var i = 0, arrLength = dtpnArr.length; i < arrLength; i++){
        var dtpn = dtpnArr[i];
        var selector = mpn + '_' + dtpn;
        var regNum = rtnData[dtpn];
        momoj('#' + selector).find('.count').html(regNum);
        //if(mpn == ecmSetConfig.DailyReg_mpn && dtpn == ecmSetConfig.game){
        //  momoj('#gameCnt').html(regNum);
        //}else{
        //  momoj('#' + selector).find('.count').html(regNum);
        //}
        
        var isFull = rtnData[dtpn + '_status'] == 'FULL';
        if(isFull){
          momoj('#' + selector).attr('onClick', "alert('" + ecmSetConfig.returnMsg['FULL'] + "')");
          if(mpn == ecmSetConfig.DailyReg_mpn && dtpn == ecmSetConfig.game){
            ecmSetConfig.gameFULL = true;
            momoj('#result').attr('src', getImgPath(ecmSetConfig.imgSrc[selector + '_FULL']));
          }else{
            momoj('#' + selector).find('i').attr('class', 'FULL'); //換圖class切換
          }
        }

      }
    }else{
      alert('很抱歉，伺服器暫時無法連線，請稍候再試' + '(' + returnMsg + ')');
    }
  }
}

function qry1003(mpn) {
  var data = {
    doAction    : 'qry',
    m_promo_no  : mpn,
    qry_type    : '1003',//#1001: 指定, #1002: 指定多個, #1003: 所有, #1004: 剩餘點數
  };
  var rtnData = promoAjax(data);
  return rtnData;
}

/* 
 * ▼個人紀錄(已領狀態、簽到紀錄戳章、兌換紀錄) 
 * 因為兌換紀錄，此方法會直接導登入
 * 若要判斷未登入直接回傳，請包在呼叫此方法外層
 */
ecmSetConfig.ref_history_selector = '#ref_history .ref_List table';
ecmSetConfig.qryKey = false;
ecmSetConfig.game_A_today = false;
ecmSetConfig.DailyReg_A_today = false;
function getTakenStatus(flag){
  if(!ecmSetConfig.qryKey){
    ecmSetConfig.qryKey = true;
    momoj(ecmSetConfig.ref_history_selector).find('.table3').remove();
    momoj('[class="opendate"]').removeClass('opendate');

    momoj().MomoLogin({flag:false, LoginSuccess:function(){
      var rtnData = qry1003(ecmSetConfig.DailyReg_mpn);
      if(rtnData != '-1'){

        if(flag == 'open'){
          var buy9999_rtnData = qry1003(ecmSetConfig.buy9999_mpn);
          var DailyBuy_rtnData = qry1003(ecmSetConfig.DailyBuy_mpn);
          if(buy9999_rtnData != '-1' && DailyBuy_rtnData != '-1'){
            if(buy9999_rtnData.returnMsg == 'OK'){
              for(var i = 0, arrLength = buy9999_rtnData.dt_promo_no.length; i < arrLength; i++){
                var qryMsgKey = ecmSetConfig.buy9999_mpn + '_' + buy9999_rtnData.dt_promo_no[i] + '_' + buy9999_rtnData.gift_code[i];
                momoj(ecmSetConfig.ref_history_selector + ' tr:last').after('<tr class="table3" style=""><td>' + buy9999_rtnData.insert_date[i].substring(0,10) + '</td><td>' + ecmSetConfig.qryMsg[qryMsgKey] + '</td></tr>');
              }
            }else{
              alert('很抱歉，伺服器暫時無法連線，請稍候再試' + '(' + buy9999_rtnData.returnMsg + ')');
              ecmSetConfig.qryKey = false;
              return;
            }
            if(DailyBuy_rtnData.returnMsg == 'OK'){
              for(var i = 0, arrLength = DailyBuy_rtnData.dt_promo_no.length; i < arrLength; i++){
                var qryMsgKey = ecmSetConfig.DailyBuy_mpn + '_' + DailyBuy_rtnData.dt_promo_no[i] + '_' + DailyBuy_rtnData.gift_code[i];
                momoj(ecmSetConfig.ref_history_selector + ' tr:last').after('<tr class="table3" style=""><td>' + DailyBuy_rtnData.insert_date[i].substring(0,10) + '</td><td>' + ecmSetConfig.qryMsg[qryMsgKey] + '</td></tr>');
              }
            }else{
              alert('很抱歉，伺服器暫時無法連線，請稍候再試' + '(' + DailyBuy_rtnData.returnMsg + ')');
              ecmSetConfig.qryKey = false;
              return;
            }
          }
        }

        var rtnDtpnArr = rtnData.dt_promo_no;
        var regDateArr = rtnData.insert_date;
        if(rtnData.returnMsg == 'OK'){
          var DailyReg_Date_Arr = [];
          ecmSetConfig.DailyRegCount = 0;
          for(var i = 0, arrLength = rtnDtpnArr.length; i < arrLength; i++){
            var dtpn = rtnDtpnArr[i];
            var selector = ecmSetConfig.DailyReg_mpn + '_' + dtpn;
            var regDate = regDateArr[i].substring(0,10);
            var atToday = Date.parse(ecmSetConfig.today).valueOf() <= Date.parse(regDate).valueOf();
    
            //兌換紀錄
            momoj(ecmSetConfig.ref_history_selector + ' tr:last').after('<tr class="table3" style=""><td>' + regDate + '</td><td>' + ecmSetConfig.qryMsg[selector + '_' + rtnData.gift_code[i]] + '</td></tr>');
    
            //有今日紀錄
            if(atToday){
              if(selector in ecmSetConfig.A_msg){
                $('#' + selector).attr('onClick', "alert('" + ecmSetConfig.A_msg[selector] + "');");
              }
              if(dtpn == ecmSetConfig.game){//今日已領取加碼
                ecmSetConfig.game_A_today = true;
              }
              if(dtpn == ecmSetConfig.DailyReg){//今日已簽到
                ecmSetConfig.DailyReg_A_today = true;
              }
            }
    
            if(dtpn == ecmSetConfig.DailyReg){
              //簽到戳章
              var stampId = regDate.substring(5,7) + regDate.substring(8,10);
              momoj('#' + stampId).attr('class', 'opendate');
              //簽到次數
              ecmSetConfig.DailyRegCount ++;
              DailyReg_Date_Arr.push(regDate);
            }
          }
    
          //以簽到次數 判斷加碼
          barQualify(ecmSetConfig.DailyRegCount, DailyReg_Date_Arr);

          ecmSetConfig.qryKey = false;
          if(flag == 'open'){
            openDiv('ref_history');
          }else if(flag == 'game'){
            if(ecmSetConfig.game_A_today){
              alert(ecmSetConfig.A_msg[ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.game]);
            }else{
              reg(ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.game);
            }
          }else if(flag == 'DailyReg'){
            if(ecmSetConfig.DailyReg_A_today){
              alert(ecmSetConfig.A_msg[ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.DailyReg]);
            }else{
              reg(ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.DailyReg);
            }
          }
        }else{
          alert('很抱歉，伺服器暫時無法連線，請稍候再試' + '(' + returnMsg + ')');
          ecmSetConfig.qryKey = false;
          return;
        }
      }
    }});
    //ecmSetConfig.qryKey = false;
  }
}
ecmSetConfig.game_isOpen = false;
ecmSetConfig.fiveDaysTaken = false;
function barQualify(cnt, dateArr){
  var dayOfSix = cnt % 6;
  if(cnt == 0){
    return;

  }else if(dayOfSix == 0){
    //第6天
    if(ecmSetConfig.DailyReg_A_today){
      ecmSetConfig.game_isOpen = true;
      momoj('#' + ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.DailyReg).find('i').attr('class', 'off');
      if(ecmSetConfig.game_A_today){
        momoj('#' + ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.game).attr('onClick', "alert('" + ecmSetConfig.A_msg[ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.game] + "');");
      }

    }else{//新的一輪開始，重新計算簽到天數
      dateArr.sort(function(a,b){
        return new Date(a) - new Date(b);
      });
      ecmSetConfig.INS_msg[ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.DailyReg] = '簽到成功。\n\n提醒您：' + lastTime(dateArr) + '解鎖之加碼拉霸時間已結束！自今日起將重新計算，累積簽到6天可再解鎖加碼拉霸。';
    }

  }else{
    for(var i = 0; i < dayOfSix; i++){
      momoj('#' + ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.DailyReg).find('i').eq(i).attr('class', 'off');
    }
    //第5天
    if(dayOfSix == 5){
      ecmSetConfig.fiveDaysTaken = !ecmSetConfig.DailyReg_A_today;
      ecmSetConfig.INS_msg[ecmSetConfig.DailyReg_mpn + '_' + ecmSetConfig.DailyReg] = '完成累積簽到達六天門檻！\n\n已解鎖今日限定拉霸，隔日將重新累計簽到！\n遊戲時間至' + ecmSetConfig.today + ' 23:59:59止，額滿即止，逾期未玩視同放棄。';
    }
  }
}

function lastTime(dateArr) {//回傳上一輪最後一天
  if(dateArr[29]){return dateArr[29];}
  else if(dateArr[23]){return dateArr[23];}
  else if(dateArr[17]){return dateArr[17];}
  else if(dateArr[11]){return dateArr[11];}
  else if(dateArr[5]) {return dateArr[5];}
}

// 顯示Div視窗
function openDiv(refId) {
  var resultSet = top.momoj('#' + refId).html();
  top.momoj().LayerMask({contentWidth:'100%', contentHeight:'auto'}).open();
  top.momoj('#MoMoLMContent').empty();
  top.momoj('#MoMoLMContent').html(resultSet).css({position:"absolute", background:"transparent"});
}
// 關閉浮層 couponRain.js已有
//function closeDiv() {   
//  top.momoj().LayerMask().close();
//}

ecmSetConfig.regKey = false;
function reg(mpn_dtpn){
  var pnTemp = mpn_dtpn.split('_');
  var mpn  = pnTemp[0];
  var dtpn = pnTemp[1];
  if(!ecmSetConfig.regKey){
    ecmSetConfig.regKey = true;
    momoj().MomoLogin({flag:false, LoginSuccess:function(){

      if(mpn == ecmSetConfig.DailyReg_mpn){
        if(dtpn == ecmSetConfig.game){
          if(!ecmSetConfig.game_isOpen){
            if(mpn_dtpn in ecmSetConfig.EA_msg){
              alert(ecmSetConfig.EA_msg[mpn_dtpn]);
            }else{
              alert('您不符合登記資格');
            }
            ecmSetConfig.regKey = false;
            return;
          }
        }else if(dtpn == ecmSetConfig.DailyReg){
          if(ecmSetConfig.fiveDaysTaken && ecmSetConfig.gameFULL){
            if(!confirm('今日加碼拉霸已額滿！是否明天再回來簽到？\n\n提醒您：達成加碼拉霸門檻後，將立即解鎖加碼拉霸！若今日完成簽到，遊戲時間將於' + ecmSetConfig.today + ' 23:59:59結束，額滿即止，逾期未玩視同放棄。')){
              ecmSetConfig.regKey = false;
              return;
            }
          }
        }
      }

      var data = {
        doAction   : 'reg',
        m_promo_no : mpn,
        dt_promo_no: dtpn,
        gift_code  : ecmSetConfig.giftCodeMap[mpn_dtpn]
      };
      var rtnData = promoAjax(data);
      if(rtnData != '-1'){
        var returnMsg = rtnData.returnMsg;
        if(returnMsg == 'A'){
          if(mpn_dtpn in ecmSetConfig.A_msg){
            alert(ecmSetConfig.A_msg[mpn_dtpn]);
          }else{
            alert(ecmSetConfig.returnMsg[returnMsg]);
          }
        }else if(returnMsg == 'EA'){
          if(mpn_dtpn in ecmSetConfig.EA_msg){
            alert(ecmSetConfig.EA_msg[mpn_dtpn]);
          }else{
            alert(ecmSetConfig.returnMsg[returnMsg]);
          }
        }else if(returnMsg == 'INS'){
          var prize = rtnData.prize;
          if(mpn == ecmSetConfig.DailyReg_mpn && dtpn == ecmSetConfig.DailyReg){
            momoj('#ref_dailyReg').find('img').attr('src', getImgPath(ecmSetConfig.imgSrc[mpn_dtpn + '_' + prize]));
            openDiv('ref_dailyReg');
            var alertMsg = '恭喜獲得' + ecmSetConfig.qryMsg[mpn_dtpn + '_' + prize] + '，今日已' + ecmSetConfig.INS_msg[mpn_dtpn];
            setTimeout(function(){
              alert(alertMsg);
            }, 1000);
          }else if(mpn == ecmSetConfig.DailyReg_mpn && dtpn == ecmSetConfig.game){
            momoj('#result').attr('src', getImgPath(ecmSetConfig.imgSrc[mpn_dtpn + '_' + prize]));
            var alertMsg = '恭喜獲得' + ecmSetConfig.qryMsg[mpn_dtpn + '_' + prize];
            setTimeout(function(){
              alert(alertMsg);
            }, 1000);
          }else if(mpn_dtpn in ecmSetConfig.INS_msg){
            alert(ecmSetConfig.INS_msg[mpn_dtpn]);
          }else{
            alert(ecmSetConfig.returnMsg[returnMsg]);
          }
        }else if(returnMsg == 'A_EX'){
          if(mpn_dtpn in ecmSetConfig.A_EX_msg){
            alert(ecmSetConfig.A_EX_msg[mpn_dtpn]);
          }else{
            alert(ecmSetConfig.returnMsg[returnMsg]);
          }
        }else if(returnMsg in ecmSetConfig.returnMsg){
          alert(ecmSetConfig.returnMsg[returnMsg]);
        }else{
          alert('很抱歉，伺服器暫時無法連線，請稍候再試' + '(' + returnMsg + ')');
        }
        ecmSetConfig.regKey = false;
        getRegNumAll();
        getTakenStatus('info');
      }
    }});
  }
}

//ecmSetConfig.after0319 = false;
function getRegNumAll(){
  DailyReg_dtpnForCntArr = ['D20180301004'];
//  if(ecmSetConfig.after0319){
      buy9999_dtpnForCntArr  = ['D20180301005','D20180301006','D20180301007','D20180301004'];
//  }else{
//      buy9999_dtpnForCntArr  = ['D20180301001','D20180301002','D20180301003','D20180301004'];
//  }
  getRegNum(ecmSetConfig.DailyReg_mpn, DailyReg_dtpnForCntArr);
  getRegNum(ecmSetConfig.buy9999_mpn, buy9999_dtpnForCntArr);
}

function pageInfo(){
//  if(ecmSetConfig.after0319){
//    momoj('.stage2').show();
//    momoj('.stage1').hide();
//  }else{
//    momoj('.stage1').show();
//    momoj('.stage2').hide();
//  }
  getRegNumAll();
  if(momoj().cookie('loginUser') != null){
    getTakenStatus('info');
  }
}

$(document).ready(function(){
//  ecmSetConfig.after0319 = Date.parse(ecmSetConfig.now).valueOf() >= Date.parse('2018/03/19').valueOf();
  pageInfo();
});