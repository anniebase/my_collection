$(function () {

	/*銀行*/
	var sw_box = new Swiper('.sw_box .PDS', {

        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.PDS .swiper-pagination',  
        paginationClickable: true, //觸擊切換
        
		//排版
		slidesPerView: '3', //顯示幾個
        loop: true, //無限循環
        //自動撥放
        autoplay: 3000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

		
		breakpoints: {

            768: {
                slidesPerView: 1,
            },

        },
		
		
		
		
		
	});


})