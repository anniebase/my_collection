/**********************************
 2018 11月入會首購_hhyang
 *********************************/
var ecmSetConfig = {};
ecmSetConfig.now = new Date();
ecmSetConfig.todayDate = ecmSetConfig.now.getDate();
ecmSetConfig.month = ecmSetConfig.now.getMonth()+1;
ecmSetConfig.Year = ecmSetConfig.now.getFullYear();
ecmSetConfig.today = ecmSetConfig.Year + "/" + ((ecmSetConfig.month.toString().length < 2)?"0":"") + ecmSetConfig.month + "/" + ((ecmSetConfig.todayDate.toString().length < 2)?"0":"") + ecmSetConfig.todayDate;
// 取得圖片位置
ecmSetConfig.itjsSrc = momoj('#itjs').attr('src');
ecmSetConfig.imgEcm = ecmSetConfig.itjsSrc.substring(0,ecmSetConfig.itjsSrc.indexOf("images\/"));
ecmSetConfig.timestamp = ecmSetConfig.itjsSrc.split('?')[1];
// 圖檔路徑
function getImgPath(imgName){
  return ecmSetConfig.imgEcm + 'images/' + imgName + '?' + ecmSetConfig.timestamp;
}
ecmSetConfig.imgSrc = {
  //。10月新入會抽
  'M20181101015_D20181101001_NT11111'      : 'NT11111.gif',
  'M20181101015_D20181101001_NONE'         : 'NTNO.gif',
  'M20181101015_D20181101001_EFAULT_GIFT'  : 'NTNO.gif',
  //。10月首購折價券
  //'M20181001015_D20181001002_coupon'     : '',

  'M20181101014_D20181101001_NT555'        : 'NT555.gif',
  'M20181101014_D20181101001_NT15'         : 'NT15.gif',
  'M20181101014_D20181101001_NT20'         : 'NT20.gif',
  'M20181101014_D20181101001_NT10'         : 'NT10.gif',
  'M20181101014_D20181101001_NT5'          : 'NT5.gif',
  'M20181101014_D20181101001_DEFAULT_GIFT' : 'NT5.gif',
  'M20181101014_D20181101002_NT666'        : 'NT666.gif',
  'M20181101014_D20181101002_NT18'         : 'NT18.gif',
  'M20181101014_D20181101002_NT24'         : 'NT24.gif',
  'M20181101014_D20181101002_NT12'         : 'NT12.gif',
  'M20181101014_D20181101002_NT6'          : 'NT6.gif',
  'M20181101014_D20181101002_DEFAULT_GIFT' : 'NT6.gif',
  'M20181101014_D20181101003_NT18'         : 'NT18.gif',
  'M20181101014_D20181101003_NT24'         : 'NT24.gif',
  'M20181101014_D20181101003_NT666'        : 'NT666.gif',
  'M20181101014_D20181101003_NT12'         : 'NT12.gif',
  'M20181101014_D20181101003_NT6'          : 'NT6.gif',
  'M20181101014_D20181101003_DEFAULT_GIFT' : 'NT6.gif',
  'M20181101014_D20181101004_NT777'        : 'NT777.gif',
  'M20181101014_D20181101004_NT77'         : 'NT77.gif',
  'M20181101014_D20181101004_NT7'          : 'NT7.gif',
  'M20181101014_D20181101004_DEFAULT_GIFT' : 'NT7.gif',
  'M20181101014_D20181101005_NT1111'       : 'NT1111.gif',
  'M20181101014_D20181101005_NT111'        : 'NT111.gif',
  'M20181101014_D20181101005_NT11'         : 'NT11.gif',
  'M20181101014_D20181101005_DEFAULT_GIFT' : 'NT11.gif',
  'M20181101014_D20181101006_NT888'        : 'NT888.gif',
  'M20181101014_D20181101006_NT88'         : 'NT88.gif',
  'M20181101014_D20181101006_NT8'          : 'NT8.gif',
  'M20181101014_D20181101006_EFAULT_GIFT'  : 'NT8.gif',
};
ecmSetConfig.INS_msg = {
  //。新入會抽
  'M20181101015_D20181101001_NT11111'      : '恭喜獲得＄11111元紅利金',
  'M20181101015_D20181101001_NONE'         : '銘謝惠顧',
  'M20181101015_D20181101001_EFAULT_GIFT'  : '銘謝惠顧',
  //。首購折價券                         
  'M20181101015_D20181101002_coupon'       : '恭喜獲得＄1000折價券，折價券已歸戶。',

  'M20181101014_D20181101001_NT555'        : '恭喜獲得＄555元紅利金',
  'M20181101014_D20181101001_NT15'         : '恭喜獲得＄15元紅利金',
  'M20181101014_D20181101001_NT20'         : '恭喜獲得＄20元紅利金',
  'M20181101014_D20181101001_NT10'         : '恭喜獲得＄10元紅利金',
  'M20181101014_D20181101001_NT5'          : '恭喜獲得＄5元紅利金',
  'M20181101014_D20181101001_DEFAULT_GIFT' : '恭喜獲得＄5元紅利金',
  'M20181101014_D20181101002_NT666'        : '恭喜獲得＄666元紅利金',
  'M20181101014_D20181101002_NT18'         : '恭喜獲得＄18元紅利金',
  'M20181101014_D20181101002_NT24'         : '恭喜獲得＄24元紅利金',
  'M20181101014_D20181101002_NT12'         : '恭喜獲得＄12元紅利金',
  'M20181101014_D20181101002_NT6'          : '恭喜獲得＄6元紅利金',
  'M20181101014_D20181101002_DEFAULT_GIFT' : '恭喜獲得＄6元紅利金',
  'M20181101014_D20181101003_NT18'         : '恭喜獲得＄18元紅利金',
  'M20181101014_D20181101003_NT24'         : '恭喜獲得＄24元紅利金',
  'M20181101014_D20181101003_NT666'        : '恭喜獲得＄666元紅利金',
  'M20181101014_D20181101003_NT12'         : '恭喜獲得＄12元紅利金',
  'M20181101014_D20181101003_NT6'          : '恭喜獲得＄6元紅利金',
  'M20181101014_D20181101003_DEFAULT_GIFT' : '恭喜獲得＄6元紅利金',
  'M20181101014_D20181101004_NT777'        : '恭喜獲得＄777元紅利金',
  'M20181101014_D20181101004_NT77'         : '恭喜獲得＄77元紅利金',
  'M20181101014_D20181101004_NT7'          : '恭喜獲得＄7元紅利金',
  'M20181101014_D20181101004_DEFAULT_GIFT' : '恭喜獲得＄7元紅利金',
  'M20181101014_D20181101005_NT1111'       : '恭喜獲得＄1111元紅利金',
  'M20181101014_D20181101005_NT111'        : '恭喜獲得＄111元紅利金',
  'M20181101014_D20181101005_NT11'         : '恭喜獲得＄11元紅利金',
  'M20181101014_D20181101005_DEFAULT_GIFT' : '恭喜獲得＄11元紅利金',
  'M20181101014_D20181101006_NT888'        : '恭喜獲得＄888元紅利金',
  'M20181101014_D20181101006_NT88'         : '恭喜獲得＄88元紅利金',
  'M20181101014_D20181101006_NT8'          : '恭喜獲得＄8元紅利金',
  'M20181101014_D20181101006_EFAULT_GIFT'  : '恭喜獲得＄8元紅利金',
};
ecmSetConfig.returnMsg = {
  'D'        : '請於活動時間內參加活動',
  'W'        : '請於指定星期參加活動',
  'WP'       : '競標金額錯誤',
  'L'        : '請先登入會員',
  'A'        : '您已參加過此活動',//'您已登記過此活動',
  'A_EX'     : '您已參加過其他首購活動',//'您已登記過其他活動',
  'EA'       : '您不符合登記資格',
  'FULL'     : '名額已經額滿!!',//'登記已額滿',
  'NOT_USED' : '很抱歉，活動暫不開放',
  'NOT_APP'  : '請在momo APP參加活動',
  'NOT_WEB'  : '請在momo網頁版參加活動',
  'NOT_NC'   : '您非活動期間新客',
  'NOT_WFB'  : '您非活動期間首購',
  'NO_PT'    : '點數不足',
  'INS'      : '登記成功，感謝您對本活動的支持',
};
/* ▼已登記人數 */
function cnt1001_1stBuyGame(dtpn, selector){
  var data = {
    doAction    : 'cnt',
    m_promo_no  : ecmSetConfig.mpn_chooseOne,
    dt_promo_no : dtpn,
    cnt_type    : '1001',
  };

  var rtnData = promoMechAjax(data);
  if(rtnData != '-1'){
    var returnMsg = rtnData.returnMsg;
    if(returnMsg == 'OK'){
      var regNum = rtnData[dtpn];
      momoj('#' + selector + ' .gameRegCnt').html(regNum);
    }else{
      alert('很抱歉，伺服器暫時無法連線，請稍候再試' + '(' + returnMsg + ')');
    }
  }
}
ecmSetConfig.regKey = false;
function reg(mpn, dtpn, giftcod){
  if(!ecmSetConfig.regKey){
    ecmSetConfig.regKey = true;
    momoj().MomoLogin({flag:false, LoginSuccess:function(){
      var data = {
        doAction   : 'reg',
        m_promo_no : mpn,
        dt_promo_no: dtpn,
        gift_code  : giftcod
      };
      var rtnData = promoMechAjax(data);
      if(rtnData != '-1'){
        var returnMsg = rtnData.returnMsg;
        if(returnMsg == 'INS'){
          var mpn_dtpn_prize = mpn + '_' + dtpn + '_' + (rtnData.prize==''?giftcod:rtnData.prize);
          if(mpn_dtpn_prize in ecmSetConfig.imgSrc){//抽獎
            //浮層
            showAns(getImgPath(ecmSetConfig.imgSrc[mpn_dtpn_prize]));
            setTimeout(function(){
              alert(ecmSetConfig.INS_msg[mpn_dtpn_prize]);
              ecmSetConfig.regKey = false;
            }, 1000);
            return;

          }else if(mpn_dtpn_prize in ecmSetConfig.INS_msg){//CP
            alert(ecmSetConfig.INS_msg[mpn_dtpn_prize]);
          }else{
            alert(ecmSetConfig.returnMsg[returnMsg]);
          }
          ecmSetConfig.regKey = false;

        }else if(returnMsg in ecmSetConfig.returnMsg){
          alert(ecmSetConfig.returnMsg[returnMsg]);
          ecmSetConfig.regKey = false;
        }else{
          alert('很抱歉，伺服器暫時無法連線，請稍候再試' + '(' + returnMsg + ')');
          ecmSetConfig.regKey = false;
        }
      }
    },LoginCancel:function(){
      ecmSetConfig.regKey = false;
    }});
  }
}
function qry1003_game() {//查詢
  momoj().MomoLogin({GoCart:false, LoginSuccess:function() {
    var data = {
      doAction    : 'qry',
      m_promo_no  : ecmSetConfig.mpn_chooseOne,
      dt_promo_no : '',
      qry_type    : '1003',//#1001: 指定, #1002: 指定多個, #1003: 所有, #1004: 剩餘點數
    };
    var rtnData = promoMechAjax(data);
    if(rtnData != '-1') {
      var returnMsg = rtnData.returnMsg;
      if(returnMsg == 'OK') {
        var _tempVal = '';
        var gift_code = rtnData.gift_code;
        var dt_promo_no = rtnData.dt_promo_no;
        var insert_date = rtnData.insert_date;
        if(insert_date){
          for(var i=0;i<insert_date.length;i++){
            var giftStr = ecmSetConfig.INS_msg[ecmSetConfig.mpn_chooseOne+'_'+dt_promo_no[i]+'_'+gift_code[i]]
            alert('感謝您於' +insert_date[i].substr(0,10)+'參與此活動\n'+ giftStr+'\n紅利金將於指定時間歸戶\n相關辦法請至<活動詳情>查看');
          }
          if(insert_date.length == 0){
            alert('無獎項紀錄');
          }
        }
        //if(ArrSize > 0){
        //  momoj('#hasSQ').show();
        //  momoj('#noSQ').hide();
        //  momoj('#SQ').html(redeem_gift[0]);
        //}else{
        //  momoj('#noSQ').show();
        //  momoj('#hasSQ').hide();
        //}

        //openDiv(_tempVal);
      }else {
        alert('很抱歉，伺服器暫時無法連線，請稍候再試' + '(' + returnMsg + ')');
      }
    }
  }});
}
ecmSetConfig.promoMechAjaxKey = false;
function promoMechAjax(data){
  if(!ecmSetConfig.promoMechAjaxKey){
    ecmSetConfig.promoMechAjaxKey = true;
    var result = '-1';
    momoj.ajax({
      url         : '/ajax/promoMech.jsp',
      async       : false,
      cache       : false,
      type        : 'POST',
      dataType    : 'json',
      contentType : 'application/x-www-form-urlencoded; charset=big5',
      data        : data,
      timeout     : 30000,
      success     : function(rtnData){
        ecmSetConfig.promoMechAjaxKey = false;
        result = rtnData;
      },
      error       : function(err, msg1, msg2){
        ecmSetConfig.promoMechAjaxKey = false;
        alert('ERROR\n很抱歉！伺服器暫時無法連線，請稍候再試');
      }
    });
    return result;
  }
}
// 顯示Div視窗
function showAns(ansSrc){
  top.momoj().LayerMask({contentWidth:'100%', contentHeight:'auto'}).open();
  top.momoj('#MoMoLMContent').empty();

  var resultSet = top.momoj('#gift_ref').html();
  top.momoj('#MoMoLMContent').html(resultSet).css({position:"absolute", background:"transparent"});
  top.momoj('#MoMoLMContent #gift_img').attr('src', ansSrc);
  top.momoj('#MoMoLMContent .ref').css("display","block");
}
// 關閉浮層視窗
function closeDiv() { 
  top.momoj().LayerMask().close();
}
//----------------------------------------------------------

ecmSetConfig.mpn_chooseOne  = 'M20181101014';//擇一
ecmSetConfig.mpn        = 'M20181101015';
ecmSetConfig.dpn_Game   = 'D20181101001';
ecmSetConfig.dpn_CP     = 'D20181101002';
ecmSetConfig.gft_CP     = 'coupon';

function gameForNewCust() {
  reg(ecmSetConfig.mpn, ecmSetConfig.dpn_Game, '');
}
function getCP() {
  reg(ecmSetConfig.mpn, ecmSetConfig.dpn_CP, ecmSetConfig.gft_CP);
}

function pageInfo() { 
  var stageStartDate = [     '2018/11/01',      '2018/11/10',      '2018/11/12',      '2018/11/20',      '2018/11/28',      '2018/10/31'];
  var stageEndDate   = [     '2018/11/09',      '2018/11/11',      '2018/11/13',      '2018/11/22',      '2018/11/30',      '2018/11/30'];
  var stage_dtpn     = [   'D20181101004',    'D20181101005',    'D20181101006',    'D20181101002',    'D20181101003',    'D20181101001'];
  var showArea_id    = [          'Area2',           'Area3',           'Area4',           'Area6',           'Area6',           'Area5'];
  for(var i = 0, arrLength = stageStartDate.length; i < arrLength; i++) {
    var afterStart = Date.parse(ecmSetConfig.today).valueOf() >= Date.parse(stageStartDate[i]+' 00:00:00').valueOf();
    var beforeEnd  = Date.parse(ecmSetConfig.today).valueOf() <= Date.parse(stageEndDate[i]+' 23:59:59').valueOf();
    if(afterStart && beforeEnd){
      //文案顯示時間區間不同
      if(i != stage_dtpn.length-1){//當月首購預設在array最後一個
        ecmSetConfig.returnMsg['NOT_WFB'] = '此活動限定' + stageStartDate[i] + ' - ' + stageEndDate[i] + '期間完成首購會員參加，您不符合資格喔！';
      }
      //區塊顯示不同
      momoj('#Area2, #Area3, #Area4, #Area5, #Area6').hide();
      momoj('#'+showArea_id[i]).show();
      //已登記人數
      cnt1001_1stBuyGame(stage_dtpn[i], showArea_id[i]);
      //呼叫方法不同
      momoj('#' + showArea_id[i] + ' .game_click_btn').click(function(){
        reg(ecmSetConfig.mpn_chooseOne, stage_dtpn[i], '');
        cnt1001_1stBuyGame(stage_dtpn[i], showArea_id[i]);
      });
      break;
    }
  }

}
//---------------------------------------------------------------------------------------
momoj(document).ready(function(){
  pageInfo();
});