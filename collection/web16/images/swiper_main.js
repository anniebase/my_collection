
$(function () { 
		//目前頁籤名稱
        //$('.NavArea .Nav_toptext').html( $('.NavArea .Nav').find('.on a').html() );
        //頁籤輪輪播class
		//$(".m_fixarea > .fixbox > ul").addClass('swiper-wrapper');
        //$(".m_fixarea > .fixbox > ul > li").addClass('swiper-slide');
});


$(window).load(function(){
	
	/*Phone置頂滑動選單*/
	var NavArea_swiper = new Swiper('.NavArea .Nav', {
			//基本
			initialSlide: $(".NavArea .Nav li.on").index() +1 , //初始險是第幾個
			speed: 1000, //滑動速度(300)
			freeMode: true, //取消只滑動1格,但不會貼齊(要貼齊要再加Sticky)
			freeModeSticky: true, //取消只滑動1格時也可貼齊
					
			//排版
			slidesPerView: 'auto', //顯示幾個
			spaceBetween: 0, //間距
	
			//自動撥放
			autoplay: 1200, //自動輪播間隔時間
			autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
	});
	NavArea_swiper.onResize();
	
	//移除第open個
	//NavArea_swiper.removeSlide( $(".NavArea .Nav li.on").index() );
	
	//展開選單切換滑動
	$('.NavArea .Btn').addClass('Btn_off')
	$('.NavArea').delegate(".Btn_off","click",function(){
		NavArea_swiper.stopAutoplay();
		NavArea_swiper.slideTo(0,0);
		$('.NavArea .Nav li').addClass('swiper-no-swiping');
		$('.NavArea .Btn').removeClass('Btn_off').addClass('Btn_open');
	});
	$('.NavArea').delegate(".Btn_open","click",function(){
		NavArea_swiper.startAutoplay();
		$('.NavArea .Nav li').removeClass('swiper-no-swiping');
		$('.NavArea .Btn').removeClass('Btn_open').addClass('Btn_off');
	});
	

})
