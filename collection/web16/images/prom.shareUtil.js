﻿var fb_key = ".icon-fb";
var line_key = ".icon-line";
var shareUtil = {
	/**
	* 分享主活動頁 fb tulee 2015年8月5日
	*/
	edmShareToFb : function(shareUrl, title, subtitle) {
    if(!edmShare.isApp()) { 
      fbUtil_hhyang.setFbData(shareUrl, '#'+title, subtitle);
    }else {
      edmShare.app(title, subtitle, shareUrl);
    }
	},
  
	/**
	* 分享主活動頁 line tulee 2015年8月5日
	*/
	edmShareToLine : function(shareUrl, title, subtitle) {
    if(!edmShare.isApp()) {      
      window.open('//line.naver.jp/R/msg/text/?'.concat(encodeURIComponent(title+subtitle)).concat(encodeURIComponent(' ')).concat(encodeURIComponent(shareUrl)));
    }else {
      edmShare.app(title, subtitle, shareUrl);
    }
	}

};