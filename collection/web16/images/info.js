var compeitiorInfo ={
    A_1:{
        group : '個唱組',
        no : '1',
        team : '',
        name : '王楊博翔',
        school : '成功高中',
        video : 'https://youtu.be/uRaPNj9MMoM',
        content : '我是王楊博翔，來自成功高中，目前二年級寫，興趣是唱歌，雖然知道要以讀書為重，但我還是不會放棄自己喜歡的事情。'
    },
    A_3:{
        group : '個唱組',
        no : '3',
        team : '',
        name : '王與傑',
        school : '建國中學',
        video : 'https://youtu.be/EiG3ieTC8S4',
        content : '大家好 我是參賽者王與傑 參加比賽是因為想要讓更多人聽到我的音樂'
    },
    A_4:{
        group : '個唱組',
        no : '4',
        team : '',
        name : '何東瑋',
        school : '建國中學',
        video : 'https://youtu.be/v16Sp_c5Lao',
        content : '大家好我是何東瑋 十七歲 臺北人 未婚 喜歡唱歌彈琴 Music is my life！'
    },
    A_5:{
        group : '個唱組',
        no : '5',
        team : '',
        name : '吳大元',
        school : '成功高中',
        video : 'https://youtu.be/7AuexSd_SFE',
        content : '我來自成功高中，身為一個高三生，音樂是唯一的舒壓方式，盧廣仲是我的偶像，對我來說，音樂也是生活的態度，這個比賽是給自己一次挑戰的機會，如果你喜歡我的聲音，謝謝你的鼓勵!!'
    },
    A_6:{
        group : '個唱組',
        no : '6',
        team : '',
        name : '吳欣柔',
        school : '莊敬高職',
        video : 'https://youtu.be/KgJ9iUCWmMw',
        content : '大家好 我叫吳欣柔今年高三19歲 喜歡唱歌跳舞作詞 專長是唱歌希望能站上大舞台給更多人聽到我的歌聲 更希望有一天能成為很厲害的歌手!'
    },
    A_7:{
        group : '個唱組',
        no : '7',
        team : '',
        name : '呂彥霆',
        school : '南強工商',
        video : 'https://youtu.be/lFPFoDkPXyI',
        content : '大家好  我目前就讀表演藝術科三年級戲劇組  從小就熱愛唱歌  在高中最後一年終於鼓起勇氣來參加比賽  希望能讓更多人聽見我的聲音！'
    },
    A_8:{
        group : '個唱組',
        no : '8',
        team : '',
        name : '李羿諒',
        school : '中和高中',
        video : 'https://youtu.be/YYcS8IKhun0',
        content : '大家好我是個唱組的李羿諒，來自新北市中和高中，熱愛唱歌，希望能透過這次的比賽，增進我的實力!'
    },
    A_9:{
        group : '個唱組',
        no : '9',
        team : '',
        name : '李衍易',
        school : '新北高中',
        video : 'https://youtu.be/K8Ppm0XMw3s',
        content : '我是新北熱音跳槽來比流音比賽的廢主唱（還請各位手下留情啊），這場比賽我就把它當作一場冒險一場夢吧（因為我也沒想過我會進複賽），雖說是一場夢但我還是會全力以赴的！希望我和複賽的大家都加油！'
    },
    A_10:{
        group : '個唱組',
        no : '10',
        team : '',
        name : '李遠哲',
        school : '花蓮高農',
        video : 'https://youtu.be/2hozTWg2Cxg',
        content : '大家好！我是來自花蓮的太魯閣族，我叫李遠哲，很榮幸也很感恩能參與這次比賽，我期許自己能夠在比賽中進步在進步，願大家比賽順利，天主保佑！'
    },
    A_11:{
        group : '個唱組',
        no : '11',
        team : '',
        name : '周正',
        school : '成功高中',
        video : 'https://youtu.be/k0kMpkyFV9Q',
        content : '哈囉大家好 我是周正 就讀成功高中，現在是成功流行音樂社的教學，對唱歌有極大的熱忱，希望在這次比賽能順利晉級到決賽！'
    },
    A_12:{
        group : '個唱組',
        no : '12',
        team : '',
        name : '周凱薇',
        school : '景美女中',
        video : 'https://youtu.be/UMbeFPiip8k',
        content : '嗨大家好！ 雖然看起來很不平易近人但其實我人很好啦！ 然後希望大家可以投我 謝謝大家！'
    },
    A_13:{
        group : '個唱組',
        no : '13',
        team : '',
        name : '林子翔',
        school : '恆毅中學',
        video : 'https://www.youtube.com/watch?v=47phvdQnzuY',
        content : '嗨 各位好！'
    },
    A_14:{
        group : '個唱組',
        no : '14',
        team : '',
        name : '林妍煦',
        school : '南強工商',
        video : 'https://youtu.be/CjcIA0u42wI',
        content : ' 是個喜歡唱歌、彈琴的女孩'
    },
    A_15:{
        group : '個唱組',
        no : '15',
        team : '',
        name : '林亞蒨',
        school : '臺北市影視音實驗教育機構                                                                     ',
        video : 'https://youtu.be/Iv6UYCNnxkk',
        content : '大家好，我是林亞蒨。我期許自己能為大家帶來耳目一新的演出，希望你們會喜歡哦哦！'
    },
    A_16:{
        group : '個唱組',
        no : '16',
        team : '',
        name : '林怡均',
        school : '康橋高中',
        video : 'https://youtu.be/gZMDJib71-o',
        content : '我是林怡均 ，來自康橋高中，今年18歲，來參賽的原因是因為，前年報名流音，但在初賽海選就被刷下來，今年呢 初賽唱一樣的歌，來為自己雪恥一下，成功進入59強，最後一年的高中時光，最後一次參加流音，希望自己能好好發揮！'
    },
    A_17:{
        group : '個唱組',
        no : '17',
        team : '',
        name : '林星宇',
        school : '鶯歌工商',
        video : 'https://youtu.be/awjjXeu5lPQ',
        content : '我是林星宇，興趣使然的參賽者'
    },
    A_18:{
        group : '個唱組',
        no : '18',
        team : '',
        name : '林家宏',
        school : '宜蘭高商',
        video : 'https://youtu.be/w0pTzFLwOus',
        content : '宜蘭在地小孩 高高胖胖的 從小就對唱歌情有獨鍾 一直到高一才參加學校的歌唱比賽 也得了不錯的名次 現在高三了 看到有流音之星這個很好的活動 馬上就報名 希望有不錯的成績 也希望評審能給我一些意見 讓我進步 加油林家宏！！'
    },
    A_19:{
        group : '個唱組',
        no : '19',
        team : '',
        name : '林媛恩',
        school : '市大同高中',
        video : 'https://youtu.be/VrkOIbkm7PQ',
        content : '我是林媛恩，今年17歲。我一直記得小的時候，我總是拿著羽球拍假裝是吉他，站在沙發上自彈自唱;觀眾是我的娃娃們和叫我小心不要摔倒的媽媽。十年過去了，現在的我是吉他社的社長，站在真正的舞台上自彈自唱;台下是幾百個觀眾和以我為榮的媽媽。音樂好像沒有在我生命裡缺席，但卻每天都讓我離夢想更近一步！'
    },
    A_20:{
        group : '個唱組',
        no : '20',
        team : '',
        name : '林聖烽',
        school : '新北高工',
        video : 'https://youtu.be/ikJR6szuZOI',
        content : 'Hi'
    },
    A_21:{
        group : '個唱組',
        no : '21',
        team : '',
        name : '林聖謀',
        school : '建國中學',
        video : 'https://youtu.be/4vz4nsq7pqs',
        content : '我是林聖謀，我很喜歡唱歌'
    },
    A_22:{
        group : '個唱組',
        no : '22',
        team : '',
        name : '林詩馨',
        school : '崇光女中',
        video : '',
        content : '我是崇光女中的林詩馨，可以叫我阿ㄙ馨，我很喜歡唱歌也喜歡交朋友，音樂是陪伴我的朋友，希望未來可以將我的音樂發展出去讓世界聽到我的聲音!'
    },
    A_23:{
        group : '個唱組',
        no : '23',
        team : '',
        name : '林鼎皓',
        school : '國立政治大學附屬高級中學',
        video : '',
        content : '嗨! 大家好~ 我是政大附中101的林鼎皓，大家都叫我肯尼 因為長得有點像哈哈，平常喜歡聽的歌很多 中文英文韓文都有聽!我唱歌也不是非常特別 但希望大家會喜歡~'
    },
    A_24:{
        group : '個唱組',
        no : '24',
        team : '',
        name : '邱吉爾',
        school : '花蓮高中',
        video : 'https://youtu.be/vSEkXsqFNdQ',
        content : '邱吉爾，一個讓你一聽就不會忘記的名字，就像我的歌聲一樣，一聽就能讓你印象深刻。'
    },
    A_25:{
        group : '個唱組',
        no : '25',
        team : '',
        name : '邱品寧',
        school : '南強工商',
        video : '',
        content : '我叫邱品寧 今年17歲 就讀南強工商表演藝術科音樂組3年級 很喜歡唱歌'
    },
    A_26:{
        group : '個唱組',
        no : '26',
        team : '',
        name : '金玟均',
        school : '南強工商',
        video : '',
        content : '我是來自南強工商表演藝術科音樂組三年級的金玟均，我的興趣是唱歌，我覺得唱歌可以讓我覺得很放鬆所以我非常喜歡唱歌。'
    },
    A_27:{
        group : '個唱組',
        no : '27',
        team : '',
        name : '徐樂',
        school : '松山家商',
        video : 'https://youtu.be/LRIva_RmmzE',
        content : '從小其實沒有什麼太大的夢想，只記得自己特別愛聽歌然後跟著哼，在成長過程中，也沒有接受專業的訓練，但是，在任何壓力大的時刻，總是能想到歌唱帶給我的紓解，也參加過學校的比賽，也逐漸喜歡上在舞台上享受歌唱的自己，只有那時候，才是最真實的我'
    },
    A_28:{
        group : '個唱組',
        no : '28',
        team : '',
        name : '高天恩',
        school : '高雄市立六龜高級中學',
        video : 'https://youtu.be/k_KfdEDg-Ag',
        content : ''
    },
    A_29:{
        group : '個唱組',
        no : '29',
        team : '',
        name : '高宥婷',
        school : '大安高工',
        video : '',
        content : '哈囉我叫高宥婷 目前是大安吉他的社長 喜歡唱歌'
    },
    A_30:{
        group : '個唱組',
        no : '30',
        team : '',
        name : '張育棋',
        school : '衛理女中',
        video : 'https://youtu.be/HUwfw5DM6nU',
        content : '大家好我是張育棋，從小我就很喜歡音樂，喜歡邊彈吉他邊唱歌，很開心能被肯定進入複賽，我會好好表現的!'
    },
    A_31:{
        group : '個唱組',
        no : '31',
        team : '',
        name : '張凱\u5586',
        school : '南強工商',
        video : 'https://youtu.be/VOSHzjcvh9c',
        content : '哈囉大家好我叫張凱\u5586，171公分61公斤，我喜歡跳舞喜歡演戲，更喜歡唱歌，生命中許多大大小小的事都是音樂陪我度過的，歌唱就是我的生活，我雖然沒有很高也沒有很帥，但如果要說我這個人有什麼值得你關注的，我會用我的歌聲證明給你看'
    },
    A_32:{
        group : '個唱組',
        no : '32',
        team : '',
        name : '莊佩昕',
        school : '松山高中',
        video : 'https://youtu.be/bYU_tg7xXt0',
        content : '我是一個完全看不懂樂譜、沒有絕對音感、甚至連打拍子都有障礙的人。即時如此我還是很喜歡唱歌，希望未來能夠一直站在舞台上，把美好的旋律帶給大家。'
    },
    A_33:{
        group : '個唱組',
        no : '33',
        team : '',
        name : '許主兒',
        school : '桃園市立陽明高級中學',
        video : 'https://youtu.be/4HOyjSn6P1s',
        content : '大家好！我是來自桃園陽明高中的許主兒∼最熱愛的興趣是唱歌彈吉他！到哪裡都能不停的唱^^目前在學校的熱音社擔任主唱 希望能透過自己的歌聲讓身邊的人感到溫暖?很開心能參加這次的比賽 希望自己能在比賽中學到很多東西、能夠更自在的面對舞台 也能夠一直堅持自己的夢想！'
    },
    A_34:{
        group : '個唱組',
        no : '34',
        team : '',
        name : '許庭瑀',
        school : '中山女高',
        video : 'https://youtu.be/EXiW55GR07s',
        content : '大家好~我是中山女高的許庭瑀，熱愛歌唱、喜歡在大家面前表演，從國中就很想參加流音之星了，很開心能進入複賽~'
    },
    A_35:{
        group : '個唱組',
        no : '35',
        team : '',
        name : '陳少迪 ',
        school : '北一女中',
        video : 'https://youtu.be/mC-_22D_2Ms',
        content : '我是北一流音17屆的教學陳少迪！最能代表我也是我最喜歡的slogan 大概就是這句話了哈哈哈！我很喜歡唱歌！只要唱歌就可以讓我變得很快樂 no why 我想不到還能說什麼了就這樣吧 我覺得我自我介紹的很清楚了顆顆'
    },
    A_36:{
        group : '個唱組',
        no : '36',
        team : '',
        name : '陳宇驊',
        school : '內湖高中',
        video : 'https://youtu.be/y0pyX8JpoY8',
        content : '哈嘍大家好！我是內中小高一陳宇驊一直都很喜歡唱歌！這次是我第一次參加流音之星！我會努力不讓大家失望的！'
    },
    A_37:{
        group : '個唱組',
        no : '37',
        team : '',
        name : '陳冠任',
        school : '宜蘭高商',
        video : 'https://youtu.be/7A5JWWgqAuI',
        content : '大家好，我是陳冠任。熱愛音樂的高中生，音樂在我的人生中，佔有非常大的地位。'
    },
    A_38:{
        group : '個唱組',
        no : '38',
        team : '',
        name : '陳姿璇',
        school : '中山女高',
        video : 'https://youtu.be/Dq69s8qGbrw',
        content : '嗨大家 我是來自中山女高的陳姿璇 喜歡唱歌喜歡彈鋼琴喜歡咬人 請大家多多指教'
    },
    A_39:{
        group : '個唱組',
        no : '39',
        team : '',
        name : '陳傳佳',
        school : '建國中學',
        video : 'https://youtu.be/GUx23uBJQSs',
        content : '大家好！我是陳傳佳 來自建國中學！如果你喜歡我的聲音 請一定要支持我哦~'
    },
    A_40:{
        group : '個唱組',
        no : '40',
        team : '',
        name : '彭文軒',
        school : '建國中學',
        video : 'https://youtu.be/Vnou94HHCEo',
        content : '嗨∼ 我是彭文軒，一個泰國跟泰雅的結合，很愛唱歌，尤其愛唱R&B、Soul、Gospel...；我最愛的歌手是Beyonce，從小看她長大，很想跟她一樣在舞台上叱吒風雲!喔對了！我自己也有在創作，喜歡音樂的人，可以交個朋友。'
    },
    A_41:{
        group : '個唱組',
        no : '41',
        team : '',
        name : '曾羽萱',
        school : '北一女中 ',
        video : 'https://youtu.be/lG-cBV6BGdE',
        content : '我是北一一數曾羽萱，國小四年級在因緣際會之下參加了合唱團，因此對唱歌產生了興趣，所以高中參加了流音，而能夠有機會參加這次比賽，希望能通過這次切磋增加自己的實力。謝謝大家!'
    },
    A_42:{
        group : '個唱組',
        no : '42',
        team : '',
        name : '曾亭語',
        school : '板橋高中',
        video : 'https://youtu.be/kFsaW56gZL8',
        content : '這是我第三年參加流音之星，高一時只報了對唱組，晉級到複賽，然後就拜拜。高二報了個唱組，我的信心滿滿，沒想到上台後唱得很烙賽，在初賽就拜拜。今年終於進複賽，請大家給我一個讚!'
    },
    A_43:{
        group : '個唱組',
        no : '43',
        team : '',
        name : '游子儀',
        school : '三民高中',
        video : 'https://youtu.be/6lxDB1cDCcw',
        content : '哈嘍！！大家好我是來自三民高中的游子儀 一個喜歡唱歌的平凡人 嗯！對！希望能透過唱歌這件事傳達出我對生命的體會 還有帶給大家能量∼'
    },
    A_44:{
        group : '個唱組',
        no : '44',
        team : '',
        name : '游文瑄',
        school : '國立宜蘭高商',
        video : 'https://youtu.be/pqEXcSEKiQc',
        content : '大家好，我是來自宜蘭高商的游文瑄，從小就喜愛唱歌的我雖然沒有機會去學唱歌但家裡很支持我去學音樂，因此在國小六年級開始接觸國樂接觸了大提琴到現在，音樂對我來說是一種生命一種感動，希望我能藉由這個比賽讓我唱出對音樂的那份感情，也希望能讓更多人聽見我的聲音，謝謝！'
    },
    A_45:{
        group : '個唱組',
        no : '45',
        team : '',
        name : '程于恬',
        school : '台中科技大學',
        video : 'https://youtu.be/fzxA5Jq4f_s',
        content : '大家好我示程于恬，興趣廣泛到讓你出奇不意！畫畫唱歌跳舞手做都很喜歡，不幸的是，我的聲音能適合的歌曲不多，如果你喜歡我的歌聲的話請讓我知道，在此認真的謝謝你！很高興你會喜歡！'
    },
    A_46:{
        group : '個唱組',
        no : '46',
        team : '',
        name : '黃至麒',
        school : '建國中學',
        video : 'https://youtu.be/iWg7bU3cMUg',
        content : '我是黃至麒，天秤座，A型，左手手掌有顆痣，算命師說這是當歌手的象徵'
    },
    A_47:{
        group : '個唱組',
        no : '47',
        team : '',
        name : '黃筠媛',
        school : '泰北高中',
        video : 'https://youtu.be/3DEWgV4xsLA',
        content : '大家好我是泰北高中室內設計科，我從小學小提琴，音樂是我的夢想，為了生活只好選擇別條路，但是我依然持續著我的夢想'
    },
    A_48:{
        group : '個唱組',
        no : '48',
        team : '',
        name : '楊子萱',
        school : '內湖高中',
        video : '',
        content : '哈囉我是就讀內湖高中二年級的楊子萱，平時就很喜歡唱歌，現在的社團也是流行音樂社，希望能多多投票給我~'
    },
    A_51:{
        group : '個唱組',
        no : '51',
        team : '',
        name : '葉嘉璇',
        school : '南強工商',
        video : 'https://youtu.be/pfL364dvxBE',
        content : '大家好∼我是來自南強工商的葉嘉璇 今年17歲，平常喜歡唱唱歌彈彈琴吃吃東西∼希望我們可以一起吃東西 哈哈哈哈！'
    },
    A_53:{
        group : '個唱組',
        no : '53',
        team : '',
        name : '廖敏妏',
        school : '新店高中',
        video : 'https://youtu.be/kd0WKLN7O5Q',
        content : '早安、午安、晚安，嗨！你好嗎？我是就讀於新店高中的廖敏妏，我對於音樂有特別的執著，投我一票！帶你一起沈浸這奇妙的音樂世界吧！'
    },
    A_54:{
        group : '個唱組',
        no : '54',
        team : '',
        name : '蔡蕙宇',
        school : '國立北門高級中學',
        video : 'https://youtu.be/FuxtlrrmJX4',
        content : '我是蔡蕙宇，就讀台南市北門高中一年級，我從小就很喜歡唱歌、運動、打球，也經常參加各地區大大小小的歌唱比賽，得到了許多不錯的成績。閒來無事時，我最喜歡的休閒活動就是唱歌了！'
    },
    A_55:{
        group : '個唱組',
        no : '55',
        team : '',
        name : '蔣牧慈',
        school : '中山女高',
        video : 'https://youtu.be/G-tvictIjVs',
        content : '我今年17歲，就讀中山女高三年級，是中山炫音17屆的主唱。我熱愛唱歌，也很享受在舞台上的每一個瞬間，希望我的歌聲能被更多的人聽到、感動更多的人！'
    },
    A_56:{
        group : '個唱組',
        no : '56',
        team : '',
        name : '盧柔伊',
        school : '景美女中',
        video : 'https://youtu.be/tXJMZ6AP-Zs',
        content : '大家好我叫盧柔伊，今年16歲，就讀景美女中高一，喜歡唱歌跟彈琴，希望大家能支持我！'
    },
    A_57:{
        group : '個唱組',
        no : '57',
        team : '',
        name : '闕綵綺',
        school : '南港高中',
        video : '',
        content : '嗨，我是來自南港高中熱音社主唱組的綵綺!超開心這次可以進到複賽，幫我投票吧'
    },
    A_58:{
        group : '個唱組',
        no : '58',
        team : '',
        name : '羅盛民',
        school : '建臺中學',
        video : 'https://youtu.be/YUq-BZA0Dpk',
        content : '大家好，我是來自苗栗的羅盛民，很多人都叫我四維。我的興趣可說是五花八門，但其中最喜歡唱歌。同時也因如此，才會參加流音之星，希望能出來見見世面。'
    },
    B_60:{
        group : '對唱組',
        no : '60',
        team : 'Barema',
        name : '凌廉宇、林庭諭',
        school : '北士商、士林高商',
        video : 'https://youtu.be/nDeUZqI_yms',
        content : '我們是凌廉宇 、林庭諭。'
    },
    B_61:{
        group : '對唱組',
        no : '61',
        team : 'SixTeens',
        name : '鄧禮頡、白振佑 、顏寧',
        school : '建國中學、大同高中、北一女中',
        video : '',
        content : '大家好，我們來自建中、大同高中及北一女，高一時因為acapella認識了彼此，在過程中也培養出對於音樂的熱愛'
    },
    B_62:{
        group : '對唱組',
        no : '62',
        team : '凌晨兩點半',
        name : '邱品寧、林妍煦',
        school : '南強工商',
        video : '',
        content : '南強工商 演藝科三年級 邱品寧 林妍煦'
    },
    B_63:{
        group : '對唱組',
        no : '63',
        team : '星月團',
        name : '范詠淇、朱曼君',
        school : '麗山高中',
        video : '',
        content : '我叫范詠淇，是個喜歡唱歌的高中生，希望能用我的歌聲感動大家'
    },
    B_64:{
        group : '對唱組',
        no : '64',
        team : '胡睿瑜 蔡孟芫',
        name : '胡睿瑜、蔡孟芫',
        school : '北一女中',
        video : 'https://youtu.be/34GTCxobwHw',
        content : '個性活潑開朗 熱愛唱歌 希望藉由流音之星這個比賽學習到更多經驗和唱歌技巧 並訓練台風和臨場反應 豐富自己的高中生活'
    },
    B_65:{
        group : '對唱組',
        no : '65',
        team : '復仇者',
        name : '劉育柔、簡婕安',
        school : '新北高工',
        video : 'https://youtu.be/vq_DZw5s74s',
        content : '大家好我們是來自新北高工的歌研社，熱愛唱歌，在這個因緣際會下我們認識，熟悉彼此的聲音，所以我們決定合唱，為了培養默契我們總是不放過任何時間練習，在過程很累但我們很享受因為我們愛唱歌!'
    },
    B_66:{
        group : '對唱組',
        no : '66',
        team : '陳少迪x陳芝緗',
        name : '陳少迪x陳芝緗',
        school : '北一女中',
        video : 'https://youtu.be/XCNYD12Y1Wk',
        content : '大家好我們是來自北一女中的學生陳少迪和陳芝緗，我們是北一女中流行音樂社的教學好夥伴！我們平時就很喜歡唱歌，可以在唱歌的時候獲得滿滿的快樂！因為膚色差...yep，所以大家都會叫我們黑白郎君！希望大家能多多支持我們，謝謝大家，就這樣！'
    },
    B_67:{
        group : '對唱組',
        no : '67',
        team : '曾亭語X胡恩嘉',
        name : '曾亭語X胡恩嘉',
        school : '板橋高中',
        video : 'https://youtu.be/-d03LX3oqO0',
        content : '大家好，我們是來自板橋高中的朋友，請多多支持！'
    },
    B_68:{
        group : '對唱組',
        no : '68',
        team : '黑人手軟',
        name : '黃靖惟、梁士茹 ',
        school : '中和高中',
        video : 'https://youtu.be/hsiv80JGyKE',
        content : '大家好我叫黃靖惟！就讀中和高中二年級現在在流音社擔任教學的職位！我從小就很喜歡唱歌，是個跟著YoYo TV唱唱跳跳長大的小孩哈哈哈~'
    },
    B_69:{
        group : '對唱組',
        no : '69',
        team : '揚恩組合',
        name : '張家揚 、顧恩亞 ',
        school : '耕莘健康管理學校、華岡藝校',
        video : 'https://youtu.be/nXe8bHC0Hk8',
        content : '我們是揚恩組合，有了8年的友情，要說我們怎麼認識的，首先要感謝上帝次給我們那麼好的聲音再來就是感謝音樂了！音樂讓我們更增添了互相的默契也更了解了彼此的故事。很開心這次我們能進入流音之星的複賽，希望大家聽到我們的歌聲後，能感受到我們對音樂的熱情。'
    },
    C_70:{
        group : '創作組',
        no : '70',
        team : '64dB',
        name : '賴則勳、胡呈嫚、葉晨宇、程鈺欣、黃少桐',
        school : '建國中學、北一女中',
        video : 'https://youtu.be/0XC7OyH0LDk',
        content : '大家好我們是64dB 這次表演的這首歌 是走輕快路線的 在講追尋自己想要的事物而不要一直在乎趕不上他人 有時候慵懶的過生活也不錯 希望大家會喜歡'
    },
    C_72:{
        group : '創作組',
        no : '72',
        team : 'QR Code',
        name : '陳冠任、莊博翔',
        school : '宜蘭高商',
        video : 'https://youtu.be/7wBaYiEhTNY',
        content : '隊伍的名稱是由我們兩個人的名字組成,希望讓大家感覺很普通,但又能讓大家感受到驚奇,首次以自創歌曲參賽就是想突破自我的限制,我們對自己的比賽歌曲非常有信心,請大家拭目以待！！'
    },
    C_73:{
        group : '創作組',
        no : '73',
        team : 'Rise',
        name : '白敘廷、施語香、謝以柔、陳韋慈、陳子彤、朱心沛',
        school : '北一女中',
        video : 'https://youtu.be/QpoHI70oztA',
        content : '大家好，我們是北一女中音樂創作社。Let me rise，讓我們跟著音樂緩緩升起吧！'
    },
    C_77:{
        group : '創作組',
        no : '77',
        team : '可可咖啡',
        name : '游宗穎',
        school : '松山高中',
        video : 'https://youtu.be/WskoB8_6pcA',
        content : ''
    },
    C_78:{
        group : '創作組',
        no : '78',
        team : '杏仁脆片',
        name : '謝以勒',
        school : '文藻外語大學',
        video : 'https://youtu.be/b_UHo34y31E',
        content : '嗨，我是謝以勒，我愛吃杏仁脆片!'
    },
    C_79:{
        group : '創作組',
        no : '79',
        team : '彩虹棒棒糖',
        name : '蔡沛倫、蕭淇、譚雅勻、李家愃、詹舒涵、林永欣',
        school : '南湖高中',
        video : 'https://youtu.be/NR0KLSznu00',
        content : '大家好我們是彩虹棒棒糖!我們超支持彩虹!然後我們喜歡吃棒棒糖!'
    },
    C_82:{
        group : '創作組',
        no : '82',
        team : '慢慢想樂團',
        name : '林怡均、呂中、林俊聿、曾安典、黃彥中',
        school : '康橋高中',
        video : 'https://youtu.be/yD7wZKLE8Uk',
        content : '我是主唱林怡均，我們的團名叫慢慢想樂團，我們的步調很慢，不管是練習還是編詞曲，都是經過深思熟慮之後的結果，想呢 諧音也有響的意思，我們的歌不是那種一開始就很炸的，但是慢慢咀嚼到後頭，會跟我們慢慢地一起融入歌當中。'
    },
    C_83:{
        group : '創作組',
        no : '83',
        team : '潘名軒',
        name : '潘名軒',
        school : '明德中學',
        video : 'https://youtu.be/XUVkGKvEoo0',
        content : '你好我是潘名軒 我是創作組的第13組 來自台中的明德中學 平常就喜歡玩音樂，最近也開始嘗試創作，最大的夢想是能夠有一團一起玩音樂的朋友，組一個團，發專輯~~'
    }
}