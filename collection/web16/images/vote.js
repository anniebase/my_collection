﻿/************************
 * 2018 12月流音之星人氣王-票選活動頁機制
 * ychsiung 2018.12.05
*************************/
var urlSearch = window.location.search.substring(1);
var params = JSON.parse('{"' + decodeURI(urlSearch).replace(/"/g, '\\"').replace(/=/g, '":"').replace(/&/g, '","') + '"}');

//取得圖片位置
var itjsSrc = momoj('#itjs').attr('src');
var imgEcm = itjsSrc.substring(0,itjsSrc.indexOf("images\/"));
var timestamp = itjsSrc.split('?')[1];
// 圖檔路徑
function getImgPath(imgName){
  return imgEcm + 'images/' + imgName + '?' + timestamp;
}
// 顯示Div視窗
function openDiv(refId) {
  var resultSet = top.momoj('#' + refId).html();
  top.momoj().LayerMask({contentWidth:'100%', contentHeight:'auto'}).open();
  top.momoj('#MoMoLMContent').empty();
  top.momoj('#MoMoLMContent').html(resultSet).css({position:"absolute", background:"transparent"});
}
//補0
function padding(num, length){
  for(var len = (num+"").length; len < length; len = num.length){
    num = '0' + num;
  }
  return num;
}
var msg_map ={
  share_confirm : '登入會員再分享，即有機會獲得電影票！\n是否登入會員？',
  get_8_points  : '投票成功！恭喜您獲得500元折價券(單品1200享折)',
  get_12_points : '投票成功！恭喜您獲得600元折價券(單品1800享折)',
};
var nowStage = 0;
/*****************
 **** 投票頁 *****
 *****************/
//nowStage
//isLoginOK
//status         //OK
//totalCnt
//competitors    //{TOTALSCORE: "88", S2: "41", CUST_NO: "A_7", S1: "6"}
//competitorsTop //{TOTALSCORE: "218", S2: "65", CUST_NO: "A_4", S1: "88"}
//compTop_A      //[ "A_4",  "A_5",  "A_7",  "A_3", "A_6",  "A_1",  "A_2"]
//voteTop_A      //[ "218",  "112",   "88",   "84",  "77",   "42",   "13"]
//compTop_B      //["B_13", "B_12", "B_10", "B_14", "B_9", "B_11", "B_15", "B_8"]
//voteTop_B      //[  "58",   "53",   "37",   "24",  "23",   "22",   "14",  "14"]
var compListKey = false;
var competitors = new Array();//參賽者清單
var tmpVoteCnt = 0;
function pageIn_show_compList(code_toShowVoteArea){
  if(!compListKey){
    compListKey = true;
    var rtnData = promoAjax({doAction : 'list'});
    if(rtnData != '-1') {
      var status = rtnData.status;
      nowStage = parseInt(rtnData.nowStage)+ 1;
      if(status == 'OK'){
        competitors = rtnData.competitors;//與search共用
        //title
        if(nowStage == 1){
		momoj('#pc_img').attr('src', getImgPath('step_' + nowStage + '.png'));	
		momoj('#ph_img').attr('src', getImgPath('step_' + nowStage + '_m.png'));
		}else if(nowStage == 2){
	    momoj('#pc_img').attr('src', getImgPath('step_' + nowStage + '.png'));	
		momoj('#ph_img').attr('src', getImgPath('step_' + nowStage + '_m.png'));		    
		}
        //ABC組 TOP1
        showTop1('A', rtnData.competitorsTop);
        showTop1('B', rtnData.competitorsTop);
		showTop1('C', rtnData.competitorsTop);
        //參賽者列表
        search('ALL');
        //顯示指定參賽者投票浮層(名片頁)
        if(code_toShowVoteArea != ''){
          for(var id in competitors){
            var indexId = competitors[id];
            var code = indexId.CUST_NO;
            var voteCnt = indexId.TOTALSCORE;
            if(code == code_toShowVoteArea){
              tmpVoteCnt = voteCnt;
              voteBox_show(code);
            }
          }
        }
        
        compListKey = false;
      }
    }
  }
}
 //ABC組 TOP1
function showTop1(flag, Arr){
  for(var id in Arr){
	var indexId = Arr[id];
    var code = indexId.CUST_NO;
    if(flag != 'ALL' && code.indexOf(flag) == -1){
      continue;
    }

    if(code in compeitiorInfo){
      momoj('#top1_' + flag + '_no').html(compeitiorInfo[code].no);
      momoj('#top1_' + flag + '_img').attr('src', getImgPath('photo/' + code +'.jpg'));
      momoj('#top1_' + flag + '_voteCnt').html(indexId.TOTALSCORE);
	  if(flag =='B'|| flag =='C'){
	  momoj('#top1_' + flag + '_name').html(compeitiorInfo[code].team);	  
	  }else{
	  momoj('#top1_' + flag + '_name').html(compeitiorInfo[code].name);  
	  }
	  
      break;//only Top1
    }
  }
}
// 搜尋 bar Start
function searchBar() {
  var value = momoj('#searchbar').val().toUpperCase().trim();
  if(value == '') {
    alert('請輸入編號、團名、姓名');
    return;
  }
  momoj('#searchbar').val('');
  search(value);
}
/*使用此方法前須確認名單(doAction:list)已取出*/
function search(value) {
  var searchedCodes = new Array();//參賽者代碼 
  var searchedCnt   = new Array();//參賽者票數  
  var _searchTF = false; 

  var tempValue = momoj.str2Unicode(value);
  if(tempValue == '&#34911;&#33590;&#23032;'){
    value = '&#34911;茶;姸';
  }
  //搜尋
  for(var  id in competitors){
    var indexId = competitors[id];
    var code = indexId.CUST_NO;
    var voteCnt = indexId.TOTALSCORE;
    if(code in compeitiorInfo){
      var group = compeitiorInfo[code].group.toUpperCase();
      var no    = compeitiorInfo[code].no.toUpperCase();
      var name  = compeitiorInfo[code].name.toUpperCase();
	  var team  = compeitiorInfo[code].team.toUpperCase();
      if(group.indexOf(value)!=-1 || code.indexOf(value)!=-1 || name.indexOf(value)!=-1 || team.indexOf(value)!=-1 ||value == 'ALL'){
        searchedCodes.push(code);
        searchedCnt.push(voteCnt);
        _searchTF = true;
      }
    }
  }

  if(_searchTF){
    showSearchLt(searchedCodes, searchedCnt);
  }else{
    alert('找不到您輸入的名稱或參賽編號，請再次確認，謝謝');
  }  
}

function searchgroup(value) {
  var searchedCodes = new Array();//參賽者代碼 
  var searchedCnt   = new Array();//參賽者票數  
  var _searchTF = false; 

  //搜尋
  for(var  id in competitors){
    var indexId = competitors[id];
    var code = indexId.CUST_NO;
    var voteCnt = indexId.TOTALSCORE;
    if(code in compeitiorInfo){
      if(code.indexOf(value)!=-1){
        searchedCodes.push(code);
        searchedCnt.push(voteCnt);
        _searchTF = true;
      }
    }
  }

  if(_searchTF){
    showSearchLt(searchedCodes, searchedCnt);
  }else{
    alert('找不到您輸入的名稱或參賽編號，請再次確認，謝謝');
  }  
}

/*每頁顯示12人*/
var nowPage       = 0;  // 現在頁數
var totalPage     = 0;  // 看更多
var perPageDatas  = 12; // 每頁顯示12人
var compareCodes  = new Array();//參賽者代碼 
var compareStageCounts   = new Array();//參賽者該階段票數
function showSearchLt(infoLt, countLt){
  compareCodes  = infoLt;
  compareStageCounts = countLt;
  momoj('#voteArea').empty();

  nowPage=1;
  totalPage = Math.ceil(compareCodes.length/perPageDatas);
  //預設每頁12人，但若未滿一頁，則顯示所有參賽者
  var forcunt = perPageDatas;
  if(totalPage <= 1) {
    forcunt =  compareCodes.length;
  }

  for(var i=0 ; i<forcunt; i++) {
    var code  = compareCodes[i];
    var cnt   = compareStageCounts[i];
    if(typeof(code) != 'undefined') {
      voteList_append(code, cnt);
    }
  }
}
/*下一頁*/
function moreData(){
  if(nowPage >= totalPage){
    alert("已經全部顯示了喔!");
    return false;
  }
  nowPage++;
  var startIdx = (nowPage - 1) * perPageDatas;
  for(var i = 0 ; i < perPageDatas ; i++) {
    if(startIdx+i <= compareCodes.length - 1) {
      var code = compareCodes[startIdx+i];
      var cnt  = compareStageCounts[startIdx+i];  
      voteList_append(code, cnt);
    }
  }
}
/*參賽者列表*/
function voteList_append(code, cnt) {
  if(compeitiorInfo[code]){
    var group = compeitiorInfo[code].group;
    var no    = compeitiorInfo[code].no;
 // var name  = compeitiorInfo[code].name;
 //	var team  = compeitiorInfo[code].team;
	var school  = compeitiorInfo[code].school;
	var groupCard = 'type' + code.split("_")[0];
	var nameCard = code.split("_")[0];
	var showname ="";
	if (nameCard == 'B' || nameCard == 'C' ){
		showname = compeitiorInfo[code].team;
	}else{
	    showname = compeitiorInfo[code].name;
	}
    var tmp = 		
	          '<li class="' + groupCard + '">'+
              '  <div class="votecard_box"> '+
              '    <div class="photo"><img class="lazy" src="'+ getImgPath('photo/' + code + '.jpg'/*img*/) + '"></div>'+
              '    <div class="txt">'+
              '      <h3 class="heady">' + group + ' NO.<span>' + no + '</span></h3>'+
	          '      <h3><span>' + showname + '</span></h3> '+
	          '      <h3><span>' + school + '</span></h3>'+
	          '      <h3>目前點數:<span id="voteCnt_' + code + '">' + cnt + '</span></h3>'+
              '    </div>'+
              '  </div>'+
	          '  <div class="votecard_btn">'+
              '        <a class="go_bt vote_btn_l" href="javascript:void(0);" onClick="vote(\'' + code + '\');">投我一票</a> '+
              '        <a class="go_bt vote_btn_r" href="javascript:void(0);" onClick="voteBox_show(\'' + code/*showDiv*/ + '\');">更多介紹</a> '+
              '  </div>  '+
              '</li>';
			
    momoj('#voteArea').append(tmp);
	
  }
}
/*****************
 **** 排行榜 *****
 *****************/
//nowStage
//isLoginOK
//status         //OK
//totalCnt
//competitors    //{TOTALSCORE: "88", S2: "41", CUST_NO: "A_7", S1: "6"}
//competitorsTop //{TOTALSCORE: "218", S2: "65", CUST_NO: "A_4", S1: "88"}
//compTop_A      //[ "A_4",  "A_5",  "A_7",  "A_3", "A_6",  "A_1",  "A_2"]
//voteTop_A      //[ "218",  "112",   "88",   "84",  "77",   "42",   "13"]
//compTop_B      //["B_13", "B_12", "B_10", "B_14", "B_9", "B_11", "B_15", "B_8"]
//voteTop_B      //[  "58",   "53",   "37",   "24",  "23",   "22",   "14",  "14"]
var topListKey = false;
var competitors = new Array();//參賽者清單
function pageIn_show_topList(){
  if(!topListKey){
    topListKey = true;
    var rtnData = promoAjax({doAction : 'list'});
    if(rtnData != '-1') {
      var status = rtnData.status;
      if(status == 'OK'){
        nowStage = parseInt(rtnData.nowStage) + 1;
        momoj('.A,.B,.C,.ALL').remove();//清空 rank為append時的selector需保留作為定位
        momoj('.card').hide();//清空
        var competitorsTop = rtnData.competitorsTop;
        topList_append('A', competitorsTop);
        topList_append('B', competitorsTop);
		topList_append('C', competitorsTop);
        topList_append('ALL', competitorsTop);
        topList_show('ALL');//預設顯示全部
        topListKey = false;
      }
    }
  }
}
/*排行榜*/
function topList_append(flag, Arr){
  var number = 0;//名次
  for(var id in Arr){
	var indexId = Arr[id];
    var code = indexId.CUST_NO;
	var groupCard = 'type' + code.split("_")[0];
	
    if(flag != 'ALL' && !code.startsWith(flag)){
      continue;
    }
    if(code in compeitiorInfo){
      number++;
      	  
	  var tmp = 
	  '<div class="card ' + flag + '" style="display:none;">'+
      '      <li class="' + groupCard + '">'+
      '       <div class="votecard_box"> '+
	  '	  <div class="rank item"> '+ number +'</div> '+
      '         <div class="photo item"><img class="lazy" src="' + getImgPath('photo/' + code +'.jpg'/*img*/) + '"></div>'+
      '         <div class="txt item">'+
      '           <h3 class="heady">NO.<span>' + compeitiorInfo[code].no/*no_num*/ + '</span></h3>'+
	  '		  <h3><span>' + compeitiorInfo[code].name/*name*/ + '</span></h3> '+
	  '		  <h3>目前點數:<span id="voteCnt_' + code + '">' + indexId.TOTALSCORE/*voteCnt*/ + '</span></h3>  '+
      '         </div>'+
	  '	<div class="votecard_btn item">'+
      '             <a class="go_bt vote_btn_r" href="javascript:void(0);" onClick="voteBox_show(\'' + code/*showDiv*/ + '\');">更多<br class="for_phone">介紹</a> '+
      '       </div>'+
      '       </div>'+
      '     </li>'+
	  '</div>';
	  
      $('#rankList .card:last').after(tmp);
    }
  }
}
/*名片頁(浮層)*/
function voteBox_show(code){
  var voteCnt = momoj('#voteCnt_'+code).html();
  voteCnt = (typeof voteCnt == "undefined" || voteCnt == null)?tmpVoteCnt:voteCnt;
  //樣式
  var css_class_control = 'type' + code.split("_")[0];//變底色
  momoj('#ref_voteBox .float_box').removeClass('typeA').removeClass('typeB').removeClass('typeC').addClass(css_class_control);
  if(nowStage == 2){//第二階段 有影片示意圖
    momoj('#ref_voteBox .img_box a').addClass('ready').attr('href', compeitiorInfo[code].video);
  }else{
    momoj('#ref_voteBox .img_box a').removeClass('ready');
  }                                                     
  momoj('#voteImg').attr('src', getImgPath('photo/' + code +'.jpg'/*img*/));       //頭像
  momoj('#group').html(compeitiorInfo[code].group);    //組別
  momoj('#school').html(compeitiorInfo[code].school);  //學校
  momoj('#no_num').html(compeitiorInfo[code].no);       //編號
  momoj('#team').html(compeitiorInfo[code].team);         //團名
  momoj('#name').html(compeitiorInfo[code].name);       //姓名
  momoj('#content').html(compeitiorInfo[code].content); //介紹
  momoj('.voteCnt').html(voteCnt);            //票數
  //參賽者清單一起更新??或是closeDiv()後接續重整
  //momoj('#voteClose').attr('onClick', 'closeDiv();');//closeDiv
  momoj('#voteClick').attr('onClick', 'vote(\'' + code/*showDiv*/ + '\')');//投我一票
  momoj('#ref_voteBox .btn_FB').attr('onClick', 'share("facebook", "' + code + '");');//fb share
  momoj('#ref_voteBox .btn_LINE').attr('onClick', 'share("line", "' + code + '");');    //line share

  openDiv('ref_voteBox');
}
/*頁籤切換(若需更新票數需重整)*/
function topList_show(flag){
  momoj('.A,.B,.C,.ALL').hide();
  momoj('.' + flag).show();
}
/*****************
 ***** 投票 ******
 *****************/
//nowStage: "0", isLoginOK: "T", status: "INS", newCnt: "14" //INS OK
//nowStage: "0", isLoginOK: "T", status: "R"                 //投同組
//nowStage: "0", isLoginOK: "T", status: "INS", newCnt: "42" //INS OK
//nowStage: "0", isLoginOK: "T", status: "A", voteLimit: "2" //每日限兩票
var votetKey = false;
function vote(code){
  if(!votetKey){
    votetKey = true;
    momoj().MomoLogin({flag:false, LoginSuccess:function() {
      if(typeof code == 'undefined') {
        alert('參賽者代碼錯誤，請重新投票');
        return;
      }
      var rtnData = promoAjax({doAction : 'gotoVote', no : code});
      if(rtnData != '-1') {
        nowStage = parseInt(rtnData.nowStage) + 1;
        var status = rtnData.status;
        if(status == 'INS'){
          var pointCnt = rtnData.pointCnt;
          var pointsMsgKey = pointCnt?'get_' + pointCnt + '_points':'';
          if(pointsMsgKey in msg_map){
            alert(msg_map[pointsMsgKey]);
          }else{
            alert('投票成功!');
          }
          
        }else if(status == 'NG_NO') {  //無此編號
          alert('參賽者編號錯誤!'); 
        }else if(status == 'SAME_NO') {//有兩位以上相通編號
          alert('參賽者編號異常!'); 
        }else if(status == 'R') {
          alert('同日不能投同組的參賽者唷!'); 
        } else if(status == 'A') {
          alert('一人一天只有' + rtnData.voteLimit + '票喔。'); 
        } else if(status == 'S') {
        }else {
          alert('很抱歉!伺服器暫時無法連線，請稍候再試。');
        }
        if(rtnData.newCnt){ //如果有值會進來判斷
          momoj('#MoMoLMContent .voteCnt').html(rtnData.newCnt); //名片頁更新
          momoj('#voteCnt_' + code).html(rtnData.newCnt); //名片頁更新
        }
        votetKey = false;

//        pageIn_show_compList();
      }
    },LoginCancel:function(){
      votetKey = false;
    }});
  }
}
/*****************
 *** 集點查詢 ****
 *****************/
//nowStage: "0", isLoginOK: "T"
var qryPointKey = false;
function qryPoint() {
  if(!qryPointKey){
    qryPointKey = true;
    momoj('#ref_ptList table td img').attr('src', getImgPath('point_b.png'));
    momoj().MomoLogin({flag:false, LoginSuccess:function() {
      var rtnData = promoAjax({doAction : 'voteDays'});
      if(rtnData != '-1') {
        var status = rtnData.status;
        if(status == 'OK') {
          nowStage = parseInt(rtnData.nowStage) + 1;
          var dateArr = rtnData.voteDays
          for(var i = 0, arrLength = dateArr.length; i < arrLength; i++){
            momoj('#'+ dateArr[i] +' img').attr('src', getImgPath('point_a.png'));
          }
          qryPointKey = false;
          openDiv('ref_ptList');
        }
      }
    },LoginCancel:function(){
      qryPointKey = false;
    }});
  }
}
/*****************
 ***** 分享 ******
 *****************/
//ins: "OK", nowStage: "0", status: "OK", isLoginOK: "T"
function share(ShareToWhere, code) {
  if(ShareToWhere != 'facebook' && ShareToWhere != 'line') {
    return;
  }
  if(momoj().cookie('loginUser') == null || momoj().cookie("ccmedia") == null){
    if(confirm(msg_map.share_confirm)){
      momoj().MomoLogin({flag:false, LoginSuccess:function() {
        promoAjax({doAction : 'share', shareBy : ShareToWhere});
        shareSet(ShareToWhere, code);
      }});
    }
  }else{
    promoAjax({doAction : 'share', shareBy : ShareToWhere});
    shareSet(ShareToWhere, code);
  }
}

function shareSet(ShareToWhere, code) {
  var isMainPage = (typeof code == "undefined" || code.length <= 0);
  code = isMainPage?"":code;
  //網址
  var urlStr = edmShare.getUrl();//分享網址
  var fb_share_url   = urlStr + (isMainPage?'':('&code=' + code)) + '&cid=vote1218&oid=fbshare';
  var line_share_url = urlStr + (isMainPage?'':('&code=' + code)) + '&cid=vote1218&oid=lineshare';
  //文案
  var title = '流音之星人氣獎';
  var subtitle_main = '投票/分享抽GoPro及電影票！';
  var subtitle_code = '投票/分享抽GoPro及電影票！';
  if(code in compeitiorInfo){
    subtitle_code += '快來幫 ' + compeitiorInfo[code].no + '號' + ' ' + compeitiorInfo[code].name + ' 投票分享！';
  }
  var subtitle = isMainPage?subtitle_main:subtitle_code;

  if(ShareToWhere == 'facebook') {
    shareUtil.edmShareToFb(fb_share_url, title, subtitle);
  } else if (ShareToWhere == 'line') {
    shareUtil.edmShareToLine(line_share_url, '「'+title+'」', subtitle);
  }
}
//promoAjax
var promoAjaxKey = false;
function promoAjax(data) {
  if(promoAjaxKey == false) {
    promoAjaxKey = true;
    var result = '-1';
    momoj.ajax({
      url : '/ajax/promotionEvent_P020181218.jsp',
      async : false,
      cache : false,
      type : 'POST',
      dataType : 'json',
      contentType : 'application/x-www-form-urlencoded; charset=big5',
      data : data,
      timeout : 30000,
      success : function(rtnData) {
        var status = rtnData.status;
        if(status == 'D') {
          alert('請於活動時間內登入參加');
        }else if(status == 'L') {
          alert('請先登入會員');
        }else if(typeof status == 'undefined' || status == '' || status == 'F' || status == 'ERR' || !status) {
          alert('很抱歉!伺服器暫時無法連線，請稍候再試');
        }else {
          result = rtnData;
        }
        promoAjaxKey = false;
      },
      error : function(err, msg1, msg2) {
        promoAjaxKey = false
        alert("ERROR\n很抱歉!伺服器暫時無法連線，請稍候再試");
      }
    });
    return result;
  }
}
momoj(document).ready(function(){
  var params_code = (params.code)?(params.code):'';
  pageIn_show_compList(params_code);
  momoj('#search_A').click(function(){	
  searchgroup('A');
  });
  momoj('#search_B').click(function(){ 	  
  searchgroup('B');
  });
  momoj('#search_C').click(function(){	
  searchgroup('C');
  });
  momoj('#search_ALL').click(function(){
  search('ALL');
  });
//  pageIn_show_topList(); //排行
  
});
//-------SSO-------
momoj(document).ready(function(){
  var rtnResult = momoj.ajaxTool({data:{flag:6001}});
  var timestamp = "";
  if(rtnResult.rtnCode=='200'){
    timestamp = rtnResult.rtnData.jsessionid;
  }
  var loginUser = momoj().cookie('loginUser');
  
  var loginFlag = momoj().cookie('ms');
  if(loginFlag == "Y"){
    momoj('#dvImg').html("");
    momoj('#dvImg').html("<img width='1' height='1' src='https://www.momoshop.com.tw/ms/login.png?timestamp="+ timestamp +"'/>");
    momoj().cookie("ms", null, {path: '/'});
  }
  
  if(loginUser != null && loginUser != "null" && loginUser != ""){
    momoj('#dvImg').html("");
    momoj('#dvImg').html("<img width='1' height='1' src='https://www.momoshop.com.tw/ms/update.png?timestamp="+ timestamp +"'/>");
  }
  else{
    var cksOut = momoj().cookie('sOut');
    if(cksOut == "Y"){
      momoj().cookie("sOut", null, {path: '/'});
      momoj('#dvImg').html("");
      var logoutstr = "<img width='1' height='1' src='https://www.momoshop.com.tw/ms/logout.png?timestamp="+ timestamp +"'/>";
      logoutstr+="<iframe src='https://m.momoshop.com.tw/ajax/ajaxTool.jsp?data={flag:%27memLogout%27}&MSFlag=1' width='1' height='1' style='visibility: hidden; display: none'></iframe>";
      logoutstr+="<iframe src='https://www.momomall.com.tw/login/logout.jsp?MSFlag=1' width='1' height='1' style='visibility: hidden; display: none'></iframe>";
      momoj('#dvImg').html(logoutstr);
    }else{
      momoj('#dvImg').html("");
      momoj('#dvImg').html("<img width='1' height='1' src='https://www.momoshop.com.tw/ms/check.png?timestamp="+ timestamp +"'/>");
     setTimeout(function(){
        var MS_RTN = momoj.ajaxTool({data:{flag:6002}});
        if(MS_RTN.rtnCode=='200'){
          if(MS_RTN.rtnData.result == "Y"){
            
          }
        }
      }, 1000);
    }
  }
});
