﻿//取資料
window.fbAsyncInit = function() {
	FB.init({
		appId : '494946170579097',
		status : true,
		xfbml : true,
		version : 'v2.12'
	});
	//FB.AppEvents.logPageView();
};

(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {
		return;
	}
		js = d.createElement(s);
		js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

var fbUtil_hhyang = {
	/**
	* 
	* @param pLink
	* @param pPicture
	* @param pName //fb 分享後 第一行
	* @param pCaption //fb 分享後 第二行
	* @param pDescription //fb 分享後 第三行
	*            tulee 2015年8月5日
	*/
	setFbData : function(pLink, pHashtag, pQuote) {
		console.log(pLink,pHashtag);
		FB.ui({
			method : 'share',
			display : 'popup',
			href : pLink,
			hashtag : pHashtag,
			quote : pQuote,
      mobile_iframe : true
		}, function(response){
			console.log(response);
		});
	}
};