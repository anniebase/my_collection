/*
 * 全站搶紅包 
 * ecchao 2017.10.24 雙十一紅包雨
 * ecchao 2017.12.06 雙十二元寶雨
 * hhyang 2018.01.22 全站搶福袋
 * yzhsieh 2018.04.10-13 全站下頭
 * ecchao 2018.05.03 全站下禮物
 * ecchao 2018.06.08 年中大促足球
 * hhyang 2018.07.11 全球購物節
 * ychsiung 2018.08.03 放閃購物節
 * ychsiung 2018.08.31 99購物節
 * hhyang 2018.10.02 終極加碼日
 * ychsiung 2018.10.19 雙十一紅包雨
 * ecchao 2018.11.08 雙十一紅包雨(call 雲端版)
 * hhyang 2018.12.05 雙十二紅包雨(call 雲端版)
 * zihsu 2019.01.02 年貨節紅包雨(call 雲端版)
 */

/*-----變數區域-----*/

//圖片路徑
var imgEcmUrl  = 'https://image.momoshop.com.tw/ecm/img/online/couponRain/images/';
//var imgEcmUrl  = '/ecm/img/online/couponRain/images/';



//紅包的html
var rainAreahtml = [
'<!doctype html>',
'<html>',
'<head>',
'<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">',
'<meta charset="big5">',
'<title>紅包雨</title>',
'</head>',
'<body>',
'<!--紅包雨-->',
'<div id="snowArea"> ',
'<!--開始-->  <div id="snow_go"></div>',
'<!--結果-->  <div id="snowEND_Area"><div class="snowEND"><a href="javascript:void(0);"><img src=""></a><div class="closeBN" onclick="closeDiv();"><a>×</a></div></div></div>',
'</div>',
'<style type="text/css">',
'/*紅包雨*/',
'/*#snowArea { position:fixed; top:0; left:0; width:100%; height:100%;  }*/',
'/*紅包*/',
'.snowbox_play { cursor:pointer;}',
'.snowbox { z-index: 99; ',
'-webkit-transform-origin: 50% 100%;',
'transform-origin: 50% 100%;',
'-webkit-animation: snow-play 3s ease-in-out infinite alternate;',
'animation: snow-play 3s ease-in-out infinite alternate;',
'}',
'@media screen and (max-width:767px) {',
'.snowbox {}',
'}',
' @-webkit-keyframes snow-play {',
' 0% {',
' -webkit-transform:translateX(0px) rotate(0deg);',
' transform:translateX(0px) rotate(0deg);',
'}',
' 50% {',
' -webkit-transform:translateX(100px) rotate(20deg);',
' transform:translateX(100px) rotate(20deg);',
'}',
' 100% {',
' -webkit-transform:translateX(0px) rotate(0deg);',
' transform:translateX(0px) rotate(0deg);',
'}',
'}',
' @keyframes snow-play {',
' 0% {',
' -webkit-transform:translateX(0px) rotate(0deg);',
' transform:translateX(0px) rotate(0deg);',
'}',
' 50% {',
' -webkit-transform:translateX(100px) rotate(20deg);',
' transform:translateX(100px) rotate(20deg);',
'}',
' 100% {',
' -webkit-transform:translateX(0px) rotate(0deg);',
' transform:translateX(0px) rotate(0deg);',
'}',
'}',
'/*結果*/',
'#snowEND_Area { display:none; position:absolute; top:50%; left:50%;',
'-webkit-transform-origin:0% 0%;',
'   -moz-transform-origin:0% 0%;',
'    -ms-transform-origin:0% 0%;',
'     -o-transform-origin:0% 0%;',
'        transform-origin:0% 0%;',
'-webkit-transform: scale(1) translate(-50%,-50%);',
'   -moz-transform: scale(1) translate(-50%,-50%);',
'    -ms-transform: scale(1) translate(-50%,-50%);',
'     -o-transform: scale(1) translate(-50%,-50%);',
'        transform: scale(1) translate(-50%,-50%);',
'-webkit-animation:HEARTBEAT-play 2.5s infinite;',
' -moz-animation:HEARTBEAT-play 2.5s infinite;',
'  -ms-animation:HEARTBEAT-play 2.5s infinite;',
'   -o-animation:HEARTBEAT-play 2.5s infinite;',
'      animation:HEARTBEAT-play 2.5s infinite;}',
'#snowEND_Area .snowEND { position:relative; ',
'  -webkit-animation:snowEND-play 1s 1 ease-in-out;',
' -moz-animation:snowEND-play 1s 1 ease-in-out;',
'  -ms-animation:snowEND-play 1s 1 ease-in-out;',
'   -o-animation:snowEND-play 1s 1 ease-in-out;',
'  animation:snowEND-play 1s 1 ease-in-out;}',
'#snowEND_Area .snowEND img { width:100%; height:auto;}',
'#snowEND_Area .closeBN a { z-index:11; position:absolute; top:-10px; right:-10px; display:block; width:40px; height:40px; border-radius:50%; border:solid 2px #999999; font:bold 40px/1em "Century Gothic"; color:#FFF; text-align:center; background-color:rgba(0%,0%,0%,0.5);text-decoration: none; cursor:pointer;}',
'#snowEND_Area .closeBN a:hover { background-color:#666; text-decoration:none}',
'@media screen and (max-width:767px) {',
'#snowEND_Area { width:100%;}',
'#snowEND_Area .snowEND_Area { width:100%;}',
'#snowEND_Area .snowEND { margin:0 auto; width:90%; height:auto;}',
'#snowEND_Area .closeBN a { top:-10px; right:-10px;}',
'}',
'@-webkit-keyframes snowEND-play {',
'  from { -webkit-transform: scale(0) rotate(-1080deg); transform: scale(0) rotate(-1080deg); top:-600px;}',
'  to { -webkit-transform: scale(1) rotate(0deg); transform: scale(1) rotate(0deg); top:0;}',
'}',
'@keyframes snowEND-play {',
'  from { -webkit-transform: scale(0) rotate(-1080deg); transform: scale(0) rotate(-1080deg); top:-600px;}',
'  to { -webkit-transform: scale(1) rotate(0deg); transform: scale(1) rotate(0deg); top:0;}',
'}',
'</style>',
'</body>',
'</html>'
].join('');
  
//禮物對應名稱
var giftMapDefault = {
  'D300_990'   : '抽紅包300元折價券(限單品990以上使用)',
	'D333_990'   : '抽紅包333元折價券(限單品990以上使用)',
	'D400_1600'  : '抽紅包400元折價券(限單品1600以上使用)',
	'D500_1900'  : '抽紅包500元折價券(限單品1900以上使用)',
	'D500_990'   : '抽紅包500元折價券(限單品990以上使用)',
  'D520_2000'  : '抽紅包520元折價券(限單品2000以上使用)',
  'D666_3000'  : '抽紅包666元折價券(限單品3000以上使用)',
  'NT_1'       : '1元紅利金',
  'NT_3'       : '3元紅利金',
  'NT_5'       : '5元紅利金',
  'NT_10'      : '10元紅利金',
  'NT_999'     : '999元紅利金'
};
var giftMap = (typeof(rainning_giftMap) != 'undefined') ? rainning_giftMap : giftMapDefault;

//時間戳
var couponRain_timestamp = '?rn=' + ((typeof(rainning_timestamp) != 'undefined') ? rainning_timestamp : Math.random());

/*-----主程式-----*/

//Ready

momoj(document).ready(function(){
  //下雨時間設定
  var cuttentTime = new Date();
  var year = cuttentTime.getFullYear();
  var month = (cuttentTime.getMonth()+1).toString();
  month = (month.length<2) ? '0' + month : month;
  var day = (cuttentTime.getDate()).toString();
  day = (day.length<2) ? '0' + day : day;
  var today = year + month + day;
  var todayDate = year + '/' + month + '/' + day;
  var baseDateArray  = '';
  
	if(today == '20190114'|| today == '20190115' || today == '20190116' || today == '20190117'){
	  baseDateArray = new Array(todayDate + ' 09:10:00', todayDate  + ' 11:10:00', todayDate + ' 13:10:00', todayDate + ' 15:10:00', todayDate + ' 17:10:00', todayDate + ' 19:10:00', todayDate + ' 21:10:00');
	}else {
    return;
  }
 
  
  var baseDate = '';
  var basePreviousDate = '';
  for(var i=0 ; i<baseDateArray.length ; i++){
    if(cuttentTime < new Date(baseDateArray[i])){
      baseDate = baseDateArray[i];
      if(i > 0){
        basePreviousDate = baseDateArray[i-1];
      }else {
        basePreviousDate = baseDateArray[i];
      }
      break;
    }
  }
  
  //判斷該時間是否下雨
  var rainPreviousTime = new Date(basePreviousDate);
  var rainPreviousPlusTenTime = new Date(basePreviousDate + 10);
  var rainStartTime = new Date(baseDate);
  var rainningTime = false;
  var locationUrl = window.location.href;
  var isRainPage = (!locationUrl.includes('login.momo') && !locationUrl.includes('shoppingCart.momo'));
  
  if(cuttentTime < rainStartTime){
    var timeoutMs = rainStartTime - cuttentTime;
    if(isRainPage){
      setTimeout(function(){
        rainning(); 
      },timeoutMs);
    }
  }
  if(cuttentTime >= rainPreviousTime && cuttentTime < rainPreviousPlusTenTime){
    rainningTime = true;
  }
  
  //時間符合&&是可下雨的頁面 就啟動下雨動畫
  if(rainningTime && isRainPage){
     rainning(); 
  }
  
});

//Ajax
var promoAjaxKey = false;
function promoRainningAjax(data) {
  if(promoAjaxKey == false) {
    promoAjaxKey = true;
    var result = '-1';
    momoj.ajax({
      url : 'https://event.momoshop.com.tw/ajax/promotionEvent_P020190114.jsp',
      async : false,
      cache : false,
      type : 'POST',
      dataType : 'json',
      contentType : 'application/x-www-form-urlencoded; charset=big5',
      data : data,
      timeout : 30000,
      success : function(rtnData) {
        var status = rtnData.status;
        if(status == 'EXPIRED' || status == 'REG_EXPIRED') {
          time = -3;
          alert('請於活動時間內登入參加');
        }else if(status == 'NOT_LOGIN') {
          alert('請先登入會員');
        }else if(typeof status == 'undefined' || status == '' || status == 'ERR') {
          alert('很抱歉!伺服器暫時無法連線，請稍候再試');
        }else {
          result = rtnData;
        }
        promoAjaxKey = false;
      },
      error : function(err, msg1, msg2) {
        promoAjaxKey = false
        alert("ERROR\n很抱歉!伺服器暫時無法連線，請稍候再試");
      }
    });
    return result;
  }
}

/*紅包雨計時*/
var time = (typeof(rainning_second) != 'undefined') ? rainning_second : 10; //結束秒數
function clockSimpleBonusRainPC(){
  time = --time;
  if ( time < 0){
    momoj('#snowArea').fadeOut(1000);
  } 
  if ( time < -3){
    momoj('#snowArea').remove();
    top.momoj('#MoMoLMContent').empty();
    top.momoj().LayerMask({contentWidth:"100%", contentHeight:"680px"}).close();
    return;
  }
  setTimeout("clockSimpleBonusRainPC()",1000)
};

//開下雨的區域浮層
function rainArea(){

  top.momoj().LayerMask({contentWidth:"100%", contentHeight:"680px"}).open();
  top.momoj('#MoMoLMContent').empty();
  top.momoj('#MoMoLMContent').append(rainAreahtml).css({position:"absolute", background:"transparent"});
  top.momoj('#MoMoLM').css({'background-color':'transparent', 'opacity':'1'});
  
}

function getEnCustNo(){
  if(document.cookie){
    var enCustNo = momoj().cookie('ck_encust');
    if(enCustNo){
      return enCustNo;
    }
  }
}

//點擊抽紅包
var regRainningKey = false;
function regRainning(){
  if(!regRainningKey){
    regRainningKey = true;
    time = 1000;//登入限1000s，沒登就跳出
    
    momoj().MomoLogin({flag:false, LoginSuccess:function() {//判斷是否登入
      var enCustNo = getEnCustNo();
      rainArea();
      momoj('#snow_go').hide();
      var data = {
          actFlag : 'regA',
          enCustNo : enCustNo
      };
      var rtnData = promoRainningAjax(data);
      if(rtnData != '-1') {
        var status = rtnData.status;
        var gift = rtnData.prize;
        var cntA = parseInt(rtnData.cntA);
        var maxA = parseInt(rtnData.maxA);
        if(status == 'FULL') {
          momoj('#snowEND_Area img').attr('src', imgEcmUrl + 'full.jpg' + couponRain_timestamp);
          setTimeout(function(){
            alert('本時段名額已經額滿!!');
            regRainningKey = false;
          },2000);
        }else if(status == 'TAKEN') {
          momoj('#snowEND_Area img').attr('src', imgEcmUrl + 'taken.jpg' + couponRain_timestamp);
          setTimeout(function(){
            alert('本時段您已經領過了喔!!');
            regRainningKey = false;
          },2000);
        }else if(status == 'SUCCESS') {
          momoj('#snowEND_Area img').attr('src', imgEcmUrl + gift + '.jpg' + couponRain_timestamp);         
		  regRainningKey = false;
        }
        momoj('#snowEND_Area').show();
        time = 15;
      }
      
    }});
  }
}

// 關閉浮層
function closeDiv() {   
	top.momoj().LayerMask().close();
}

//下雨動作
function rainning(){
	rainArea();
	clockSimpleBonusRainPC();

	/*下紅包*/
	momoj(function(options){
		var con          = '#snow_go';
		var flake	    	= momoj('<div class="snowbox snowbox1 snowbox_play" />').css({'position': 'fixed', 'top': '-50px'}).html('<img style="  width:100%;  height:auto;" src="'+ imgEcmUrl + '1.gif' + couponRain_timestamp + '"/>');
		var flake2			= momoj('<div class="snowbox snowbox2 snowbox_play" />').css({'position': 'fixed', 'top': '-50px'}).html('<img style="  width:100%;  height:auto;" src="'+ imgEcmUrl + '1.gif' + couponRain_timestamp + '"/>');
    var flake3			= momoj('<div class="snowbox snowbox3 snowbox_play" />').css({'position': 'fixed', 'top': '-50px'}).html('<img style="  width:100%;  height:auto;" src="'+ imgEcmUrl + '1.gif' + couponRain_timestamp + '"/>');
	  var flake4			= momoj('<div class="snowbox snowbox4 snowbox_play" />').css({'position': 'fixed', 'top': '-50px'}).html('<img style="  width:100%;  height:auto;" src="'+ imgEcmUrl + '1.gif' + couponRain_timestamp + '"/>');
	  var flake5			= momoj('<div class="snowbox snowbox5 snowbox_play" />').css({'position': 'fixed', 'top': '-50px'}).html('<img style="  width:100%;  height:auto;" src="'+ imgEcmUrl + '1.gif' + couponRain_timestamp + '"/>');
		documentHeight = momoj(window).height(),
		documentWidth  = momoj(document).width();

		if ( momoj(window).width() > 767 ){
			var defaults    = {
				minSize     : 50,    //紅包的最小尺寸
				maxSize     : 70,    //紅包的最大尺寸
				newOn       : 1500,  //紅包出現的機率，值越小紅包越多
				flakeColor  : "#FFFFFF"
			};
		}else {
			var defaults    = {
				minSize     : 35,    //紅包的最小尺寸
				maxSize     : 70,    //紅包的最大尺寸
				newOn       : 2000,  //紅包出現的機率，值越小紅包越多
				flakeColor  : "#FFFFFF"
			};
		}
		options      = momoj.extend({}, defaults, options);
		
	  //第1個
	    var interval        = setInterval( function(){
		var startPositionLeft  = Math.random() * documentWidth - 0,
			startOpacity    = 0.5 + Math.random(),
			sizeFlake       = options.minSize + Math.random() * options.maxSize,
			endPositionTop  = documentHeight + 100,
			endPositionLeft = startPositionLeft  + Math.random() * 50,
			durationFall    = documentHeight * 1.5 + Math.random() * 5000 *1.8,
			starttransform  = Math.random()*500;
		var i = 0;
		flake.clone().appendTo( con ).css({
					left: startPositionLeft + 0,
					'width': sizeFlake +"px" ,
				}).animate({
					top: endPositionTop * 1,
					left: endPositionLeft,
				},durationFall,'linear',function(){
					momoj(this).remove()
				}
			).parent('div#snow_go').find('.snowbox').undelegate('img', 'click').delegate('img', 'click', function(e) {
				momoj(this).attr('src','' + imgEcmUrl + 'pc_element_end1.gif' + couponRain_timestamp).delay(700).fadeOut();
				momoj('#snow_go').fadeOut(500);
				regRainning();
				e.preventDefault();
			}).find('img').css({
				'-webkit-transform': 'translateX( -'+ starttransform +'px)',
				'transform': 'translateX( -'+ starttransform +'px)',
				'-webkit-transform': 'rotate('+ starttransform +'deg)',
				'transform': 'rotate('+ starttransform +'deg)',
		})
	},options.newOn);

    //第2個
	    var interval        = setInterval( function(){
		var startPositionLeft  = Math.random() * documentWidth - 0,
			startOpacity    = 0.5 + Math.random(),
			sizeFlake       = options.minSize + Math.random() * options.maxSize,
			endPositionTop  = documentHeight + 100,
			endPositionLeft = startPositionLeft  + Math.random() * 50,
			durationFall    = documentHeight * 1.5 + Math.random() * 5000 *1.8,
			starttransform  = Math.random()*500;
		var i = 0;
		flake2.clone().appendTo( con ).css({
					left: startPositionLeft + 0,
					'width': sizeFlake +"px" ,
				}).animate({
					top: endPositionTop * 1,
					left: endPositionLeft,
				},durationFall,'linear',function(){
					momoj(this).remove()
				}
			).parent('div#snow_go').find('.snowbox').undelegate('img', 'click').delegate('img', 'click', function(e) {
				momoj(this).attr('src','' + imgEcmUrl + 'pc_element_end1.gif' + couponRain_timestamp).delay(700).fadeOut();
				momoj('#snow_go').fadeOut(500);
				regRainning();
				e.preventDefault();
			}).find('img').css({
				'-webkit-transform': 'translateX( -'+ starttransform +'px)',
				'transform': 'translateX( -'+ starttransform +'px)',
				'-webkit-transform': 'rotate('+ starttransform +'deg)',
				'transform': 'rotate('+ starttransform +'deg)',
		})
	},options.newOn);

    //第3個
	    var interval        = setInterval( function(){
		var startPositionLeft  = Math.random() * documentWidth - 0,
			startOpacity    = 0.5 + Math.random(),
			sizeFlake       = options.minSize + Math.random() * options.maxSize,
			endPositionTop  = documentHeight + 100,
			endPositionLeft = startPositionLeft  + Math.random() * 50,
			durationFall    = documentHeight * 1 + Math.random() * 5000 *1.8,
			starttransform  = Math.random()*500;
		var i = 0;
		flake3.clone().appendTo( con ).css({
					left: startPositionLeft + 0,
					'width': sizeFlake +"px" ,
				}).animate({
					top: endPositionTop * 1,
					left: endPositionLeft,
				},durationFall,'linear',function(){
					momoj(this).remove()
				}
			).parent('div#snow_go').find('.snowbox').undelegate('img', 'click').delegate('img', 'click', function(e) {
				momoj(this).attr('src','' + imgEcmUrl + 'pc_element_end1.gif' + couponRain_timestamp).delay(700).fadeOut();
				momoj('#snow_go').fadeOut(500);
				regRainning();
				e.preventDefault();
			}).find('img').css({
				'-webkit-transform': 'translateX( -'+ starttransform +'px)',
				'transform': 'translateX( -'+ starttransform +'px)',
				'-webkit-transform': 'rotate('+ starttransform +'deg)',
				'transform': 'rotate('+ starttransform +'deg)',
		})
	},options.newOn);

    //第4個
	    var interval        = setInterval( function(){
		var startPositionLeft  = Math.random() * documentWidth - 0,
			startOpacity    = 0.5 + Math.random(),
			sizeFlake       = options.minSize + Math.random() * options.maxSize,
			endPositionTop  = documentHeight + 100,
			endPositionLeft = startPositionLeft  + Math.random() * 50,
			durationFall    = documentHeight * 4 + Math.random() * 5000 *1.8,
			starttransform  = Math.random()*500;
		var i = 0;
		flake4.clone().appendTo( con ).css({
					left: startPositionLeft + 0,
					'width': sizeFlake +"px" ,
				}).animate({
					top: endPositionTop * 1,
					left: endPositionLeft,
				},durationFall,'linear',function(){
					momoj(this).remove()
				}
			).parent('div#snow_go').find('.snowbox').undelegate('img', 'click').delegate('img', 'click', function(e) {
				momoj(this).attr('src','' + imgEcmUrl + 'pc_element_end1.gif' + couponRain_timestamp).delay(700).fadeOut();
				momoj('#snow_go').fadeOut(500);
				regRainning();
				e.preventDefault();
			}).find('img').css({
				'-webkit-transform': 'translateX( -'+ starttransform +'px)',
				'transform': 'translateX( -'+ starttransform +'px)',
				'-webkit-transform': 'rotate('+ starttransform +'deg)',
				'transform': 'rotate('+ starttransform +'deg)',
		})
	},options.newOn);

    //第5個
	    var interval        = setInterval( function(){
		var startPositionLeft  = Math.random() * documentWidth - 0,
			startOpacity    = 0.5 + Math.random(),
			sizeFlake       = options.minSize + Math.random() * options.maxSize,
			endPositionTop  = documentHeight + 100,
			endPositionLeft = startPositionLeft  + Math.random() * 50,
			durationFall    = documentHeight * 1.5 + Math.random() * 5000 *1.8,
			starttransform  = Math.random()*500;
		var i = 0;
		flake5.clone().appendTo( con ).css({
					left: startPositionLeft + 0,
					'width': sizeFlake +"px" ,
				}).animate({
					top: endPositionTop * 1,
					left: endPositionLeft,
				},durationFall,'linear',function(){
					momoj(this).remove()
				}
			).parent('div#snow_go').find('.snowbox').undelegate('img', 'click').delegate('img', 'click', function(e) {
				momoj(this).attr('src','' + imgEcmUrl + 'pc_element_end1.gif' + couponRain_timestamp).delay(700).fadeOut();
				momoj('#snow_go').fadeOut(500);
				regRainning();
				e.preventDefault();
			}).find('img').css({
				'-webkit-transform': 'translateX( -'+ starttransform +'px)',
				'transform': 'translateX( -'+ starttransform +'px)',
				'-webkit-transform': 'rotate('+ starttransform +'deg)',
				'transform': 'rotate('+ starttransform +'deg)',
		})
	},options.newOn);
    
	});
}