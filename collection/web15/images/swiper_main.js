
$(window).load(function(){


	/*直播輪播BN
	var Area_bn_swiper = new Swiper('.Area_bn_swiper .box', {
	
		//小圓點
		pagination: '.Area_bn_swiper .swiper-pagination',
		paginationClickable: true, //觸擊切換
		
		//基本
		//initialSlide: 4, //初始險是第幾個
		//freeMode: true, //取消只滑動1格,但不會貼齊(要貼齊要再加Sticky)
		//freeModeSticky: true, //取消只滑動1格時也可貼齊
		
		//排版
		centeredSlides: true, //當前區塊居中
		slidesPerView:1, //顯示幾個
		spaceBetween:0, //間距
		
		//自動撥放
		autoplay: 5000, //自動輪播間隔時間
		autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
	  
	});*/
  
	/*F固定時間更換圖片
	$(function (){
		var today = new Date(); 
		var year = today.getFullYear(); //西元年
		var month = today.getMonth()+1; //月
		var day = today.getDate(); //日
		var hour = today.getHours();  //時
		var minute = today.getMinutes();  //分
		var second = today.getSeconds();  //秒
		var week = today.getDay(); //星期0~6
		var todaytxt = today;

		//過期消失
		var Mydate  = new Date("2017/12/12 17:30:00"); 	if ( Mydate <= today ) { Area_bn_swiper.removeSlide([4]) };
		var Mydate  = new Date("2017/12/12 13:30:00"); 	if ( Mydate <= today ) { Area_bn_swiper.removeSlide([3]) };
		
		var Mydate  = new Date("2017/12/11 22:00:00"); 	if ( Mydate <= today ) { Area_bn_swiper.removeSlide([2]) };
		var Mydate  = new Date("2017/12/11 15:30:00"); 	if ( Mydate <= today ) { Area_bn_swiper.removeSlide([1]) };
		var Mydate  = new Date("2017/12/11 13:30:00"); 	if ( Mydate <= today ) { Area_bn_swiper.removeSlide([0]) };

		//setTimeout("clock()",1000)
	});*/

	/*
	$('#Area_bn_swiper .swiper-slide').eq(0).addClass('live');*/


/*銀行*/
	var Area_cantantBaseBN_swiper = new Swiper('.Area_cantantBaseBN .box', {

				
		//排版
		slidesPerView:5, //顯示幾個
		slidesPerGroup: 5, //一次切換幾個
		spaceBetween:0, //間距
		
		//自動撥放
		autoplay: 3000, //自動輪播間隔時間
		autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
		
		//RWD
        breakpoints: {
            767: {
				
				//基本
				direction: 'horizontal', //滑動方向-垂直(預設水平horizontal)
				freeMode: true, //取消只滑動1格,但不會貼齊(要貼齊要再加Sticky)
				freeModeSticky: true, //取消只滑動1格時也可貼齊
			
				//排版
				slidesPerView: 2, //顯示幾個
				slidesPerGroup: 2, //一次切換幾個
				spaceBetween: 0, //間距
				
				slidesOffsetBefore: 0, //左邊偏移量
				slidesOffsetAfter: 0, //右邊偏移量
				
				
            },
        },

    });
	

	
	/*促銷BN輪播*/
	var Area_acty_Swiper = new Swiper('.acty2 .container', {
        //基本
        loop: true, //無限循環
        //自動撥放
        autoplay: 4000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
		
		//小圓點
		pagination: '.acty2 .swiper-pagination',
		paginationClickable: true, //觸擊切換
	
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.acty2 .swiper-button-next', 
        prevButton: '.acty2 .swiper-button-prev',
  
        				//排版
				slidesPerView: 2, //顯示幾個
				slidesPerGroup: 2, //一次切換幾個
				spaceBetween: 0, //間距
				
		//RWD
        breakpoints: {
            767: {
			
				//排版
				slidesPerView: 1, //顯示幾個
				slidesPerGroup: 1, //一次切換幾個
				spaceBetween: 0, //間距
				
         },
        },

	
	});
	
  
  
})