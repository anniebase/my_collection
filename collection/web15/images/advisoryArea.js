$(function(){
	/** 頁面分頁籤 **/
	var advtabTitle = ".advisoryArea dd";
	var advContent = ".advisoryArea .tabcontent";
	$(advtabTitle + ":first").addClass("selected");
	$(advContent).hide().eq(0).show();
	$(advtabTitle).unbind("click").bind("click", function(){
		$(this).siblings("dd").removeClass("selected").end().addClass("selected");
		var index = $(advtabTitle).index( $(this) );
		$(advContent).eq(index).siblings(advContent).hide().end().fadeIn("slow");
	});
	
});


$(function(){
	/** 頁面分頁籤 **/
	var advtabTitle = ".advisoryArea_mm dd";
	var advContent = ".advisoryArea_mm .tabcontent";
	$(advtabTitle + ":first").addClass("selected");
	$(advContent).hide().eq(0).show();
	$(advtabTitle).unbind("click").bind("click", function(){
		$(this).siblings("dd").removeClass("selected").end().addClass("selected");
		var index = $(advtabTitle).index( $(this) );
		$(advContent).eq(index).siblings(advContent).hide().end().fadeIn("slow");
	});
	
});
