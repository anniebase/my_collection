// JavaScript Document*/ 
/* 回版頭*/
jQuery(function(){
	jQuery("#gotop").click(function(){
		jQuery("html,body").stop(true,false).animate({scrollTop:0},700); //設定回頁面頂端
		return false;	
	});
    jQuery(window).scroll(function() {
        if ( jQuery(this).scrollTop() > 300){ //設定大於300px才顯示浮層
            jQuery('#gotop').fadeIn("fast");
        } else {
            jQuery('#gotop').stop().fadeOut("fast");
        }
    });
});

/* 滑動的GOTO */
function goTop(val) {
	var gotop_i = 45
	jQuery('html,body').animate({scrollTop: jQuery(val).offset().top - gotop_i });
}


/* 浮層區*/
function agree(val) {
	$(val).toggleClass('showtime')
	var winST =  jQuery(window).scrollTop(); //目前位置
	var winH =  jQuery(window).height(); //裝置高度	
	//浮層高度
	$(val).find('.agreeArea .txtArea').css('height', winH * 0.68 - 34);
	//var this_agreeH = $(val).find('.agreeArea').height();
	//浮層top定位
	//$('.agreeArea').css('top', winST + winH/2 - this_agreeH/2 );
};


$(function(){
	var blackBox = $(".blackBox");
	var blackBox_close = $(".blackBox .close , .blackBox .but-close");
	var blackBox_BOXclose = ".Boxclose , .fixedfooterArea_B ";
	console.log('here');
	//點按鈕關閉
	blackBox_close.delegate( "a" ,"touchstart click",function(e){
		$(blackBox).removeClass('showtime');
		e.preventDefault();
	});
	//點黑區關閉
	blackBox.delegate( blackBox_BOXclose ,"touchstart click",function(e){
		$(blackBox).removeClass('showtime');
		e.preventDefault();
	});
});
/*高亮目前頁面*/
$(function(){
	var $con = $("#js-this"); 
	var title = $con.attr("data-title");   	//抓名稱
	var title2 = $con.attr("data-title2"); //抓名稱
	var title3 = $con.attr("data-title3"); //抓名稱
	$('.cantantBase, .fixarea, .Fixedfooter').find("li").each(function() {
		if ( $(this).html().indexOf(title) >= 0 || $(this).html().indexOf(title2) >= 0 || $(this).html().indexOf(title3) >= 0) {
			$(this).addClass("cate-hover index");
		}
	});
});

/*Phone置底選單*/
$(function(){
	var $ff = $('.Fixedfooter');
	var li = '.Fixedfooter_box li';
	var s = '.Fixedfooter-slide';
	var b = '.Fixedfooter_bg';
	var a = '.Fixedfooter_agree';
	var cate_open = 'cate-open';
	var cate_hover = 'cate-hover';
	var cate_close = 'cate-close';
	var c ;
	
	//點按鈕高亮
	$ff.delegate(li ,'click',function(){
		$(this).addClass(cate_hover).siblings(li).removeClass(cate_hover);
		var i = $(this).index();
		ffbox_bg(i);
		icon_animated(i)
	});
	
	//點按鈕打開浮層
	$ff.delegate( s ,'click',function(){
		var i = $(this).index(s);
		console.log('改變前c:'+ c + ' i:' + i );

		
		if ( $ff.hasClass(cate_open) ){
			if ( i == c ){
				//console.log('同一個');
				close()
			} 
				
			else {
				//console.log('不同');
				$ff.addClass(cate_open);
				$ff.find(a).fadeOut(0);
				$ff.find(a).eq(i).fadeIn(0);
			};
		} else {
			$ff.addClass(cate_open);
			$ff.find(a).fadeOut(0);
			$ff.find(a).eq(i).fadeIn(0);
		};
		c = i;
		//console.log('改變後c:'+ c + ' i:' + i );
	});

	//點黑區
	$ff.delegate( b ,'touchstart click',function(e){
		close()
		e.preventDefault();
		$ff.find(li).find('i').removeClass(); //icon動動
	});
	
	//關閉
	function close(){
		$ff.removeClass(cate_open);
		$ff.find(li).removeClass(cate_hover).each(function(i) {
			if ( $(this).hasClass('index') ) {
				$(this).addClass(cate_hover)
				ffbox_bg(i);
			}
		});
		$ff.find(a).fadeOut(0); //浮層
		$ff.find(li).find('i').removeClass(); //icon動動

	}
	
	//選單底圖移動
	function ffbox_bg(i){
		var l = $ff.find(li).length;
		var move_a = 100/l*i
		$ff.find('.Fixedfooter_box .bg').css('transform', 'translateX(' + move_a + '%)');

		console.log( l + '/' + i );
		
	}
	
	//目前高亮選單底圖移動
	function ffbox_bgindex(){
		var l = $ff.find(li).length ;
		var i = $ff.find(li + '.index').index()
		var move_a = 100/l*i

	
		$ff.find('.Fixedfooter_box .bg').css('transform', 'translateX('+ move_a + '%)');
				
	};ffbox_bgindex();
	
	
	
	
	//icon動動
	function icon_animated(i){
		var c = 'animated tada';
		$ff.find(li).eq(i).find('i').addClass( c );
		$ff.find(li).eq(i).siblings(li).find('i').removeClass( c );
	}


				
	
	
}); 


