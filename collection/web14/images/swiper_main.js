

$(window).load(function(){
	
	var AreaPD_box_PD_img_swiper = new Swiper('.album', {
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.album .swiper-pagination',  
        paginationClickable: true, //觸擊切換

        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.album .swiper-button-next', 
        prevButton: '.album .swiper-button-prev',

		//基本
		loop: true, //無限循環
		
        //排版
        slidesPerView: 3, //顯示幾個
		spaceBetween:10, //間距
		
        //自動撥放
        autoplay: 3000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
             	
		//延遲加載	
		lazyLoading : true,			//延遲載入啟動
		lazyLoadingInPrevNext : true,		//提前載入前一個和後一個slide
		lazyLoadingInPrevNextAmount : 2,	//提前載入幾個前後slide
		lazyLoadingOnTransitionStart : true,//切換時就開始載入	
		
		//RWD
        breakpoints: {
            767: {
                slidesPerView: 2,
                spaceBetween: 5,
            },
        },

	});
})