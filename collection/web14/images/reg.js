﻿/**********************************
 2019 2月全站_集點加價購(2/1-2/28)_hhyang
 *********************************/
var ecmSetConfig = {};

// 顯示Div視窗
function openDiv(refId) {
  var resultSet = top.momoj('#' + refId).html();
  top.momoj().LayerMask({contentWidth:'100%', contentHeight:'auto'}).open();
  top.momoj('#MoMoLMContent').empty();
  top.momoj('#MoMoLMContent').html(resultSet).css({position:"absolute", background:"transparent"});
}

ecmSetConfig.qryMsg = {
  'goods_0' : '【宇宙人】面紙套2入',
  'goods_1' : '【宇宙人】大型公仔抱枕',
  'goods_2' : '【Alfi】真空保溫壺 (紅色)',
  'goods_3' : '【品家家品】蒸鍋蒸籠組28cm',
  'goods_4' : '【一匙靈】濃縮潔淨洗衣精1+2件組',
  'goods_5' : '【moony】日本有機棉褲型L38片/包',
  'goods_6' : '【OLAY】長效保濕凝露2入組',
  'goods_7' : '【愛之味】純濃燕麥利樂包24瓶',
};

ecmSetConfig.returnMsg = {
  'D'              : '請於活動時間內參加活動',
  'L'              : '請先登入會員',
  'FULL_POINT'     : '點數已累積達上限100點，請先加購以下商品兌換掉點數，再來領點喔!!',
  'NO_ADD'         : '目前無可增加點數，下單後請稍待3分鐘後，再來集點喔!!',
  'GOODS_MISMATCH' : '很抱歉，活動暫不開放',//前端傳入品項不存在
  'FULL'           : '兌換完畢!!',
  'NO_PT'          : '點數不足',
};

ecmSetConfig.promoAjaxKey = false;
function promoAjax(data){
  if(!ecmSetConfig.promoAjaxKey){
    ecmSetConfig.promoAjaxKey = true;
    var result = '-1';
    momoj.ajax({
      url         : '/ajax/promotionEvent_P020190201.jsp',
      async       : false,
      cache       : false,
      type        : 'POST',
      dataType    : 'json',
      contentType : 'application/x-www-form-urlencoded; charset=big5',
      data        : data,
      timeout     : 30000,
      success     : function(rtnData){
        ecmSetConfig.promoAjaxKey = false;
        result = rtnData;
      },
      error       : function(err, msg1, msg2){
        ecmSetConfig.promoAjaxKey = false;
        alert('ERROR\n很抱歉！伺服器暫時無法連線，請稍候再試');
      }
    });
    return result;
  }
}
//【初始頁面】持有點數、獎項剩餘數
ecmSetConfig.infoKey = false;
function info(){
  if(!ecmSetConfig.infoKey){
    ecmSetConfig.infoKey = true;
    var data = {
      doAction   : 'info'
    };
    var rtnData = promoAjax(data);
    if(rtnData != '-1'){
      var status = rtnData.status;
      if(status == 'OK'){
        show_goods_cnt_status(rtnData.goods_cnt_status);
        show_nowPoint(rtnData.nowPoint);
        ecmSetConfig.infoKey = false;
      }
    }
  }
}
//【消費領點】
ecmSetConfig.getPointKey = false;
function getPoint(){
  if(!ecmSetConfig.getPointKey){
    ecmSetConfig.getPointKey = true;
    var data = {
      doAction   : 'getPoint'
    };
    var rtnData = promoAjax(data);
    if(rtnData != '-1'){
      var status = rtnData.status;
      if(status == 'OK'){
        alert('恭喜您獲得' + rtnData.addPoints + '點!');
        ecmSetConfig.getPointKey = false;
        info();
      }else if(status == 'L'){
        momoj().MomoLogin({flag:false, LoginSuccess:function(){
          ecmSetConfig.getPointKey = false;
          getPoint();
          info();
        },LoginCancel:function(){
          ecmSetConfig.getPointKey = false;
        }});
      }else if(status in ecmSetConfig.returnMsg){//D(請於活動時間內參加活動)、FULL_POINT(點數已達上限)、NO_ADD(無可增加點數)
        alert(ecmSetConfig.returnMsg[status]);
        ecmSetConfig.getPointKey = false;
      }
    }
  }
}
//【扣點兌換】
ecmSetConfig.regGoodsKey = false;
function regGoods(goods_idx){
  if(!ecmSetConfig.regGoodsKey){
    ecmSetConfig.regGoodsKey = true;
    //---------------------------------------------------------------------------------------
    if(!('goods_' + goods_idx) in ecmSetConfig.qryMsg){
      alert(ecmSetConfig.returnMsg['GOODS_MISMATCH']);
      ecmSetConfig.regGoodsKey = false;
      return;
    }
    var goods_name = ecmSetConfig.qryMsg['goods_' + goods_idx];
    if(!confirm('請確認是否兌換' + goods_name + '？\n按下[確認]後，點數將立即扣除!')){
      ecmSetConfig.regGoodsKey = false;
      return;
    }
    //---------------------------------------------------------------------------------------
    
    var data = {
      doAction   : 'regGoods',
      goods      : goods_idx
    };
    var rtnData = promoAjax(data);
    if(rtnData != '-1'){
      var status = rtnData.status;
      if(status == 'OK'){
        alert('已成功登記購' + goods_name + '');
        ecmSetConfig.regGoodsKey = false;
        info();
      }else if(status == 'L'){
        momoj().MomoLogin({flag:false, LoginSuccess:function(){
          ecmSetConfig.regGoodsKey = false;
          regGoods(goods_idx);
          info();
        },LoginCancel:function(){
          ecmSetConfig.regGoodsKey = false;
        }});
      }else if(status in ecmSetConfig.returnMsg){//D(請於活動時間內參加活動)、GOODS_MISMATCH(前端傳入品項不存在)、FULL(兌換完畢)、NO_PT(點數不足)
        alert(ecmSetConfig.returnMsg[status]);
        ecmSetConfig.regGoodsKey = false;
      }
    }
  }
}
//【兌換紀錄】
ecmSetConfig.qryKey = false;
function qry(){
  if(!ecmSetConfig.qryKey){
    ecmSetConfig.qryKey = true;
    var data = {
      doAction   : 'qry'
    };
    var rtnData = promoAjax(data);
    if(rtnData != '-1'){
      var status = rtnData.status;
      if(status == 'OK'){
        show_qry_list(rtnData.qry_list);
        ecmSetConfig.qryKey = false;
      }else if(status == 'L'){
        momoj().MomoLogin({flag:false, LoginSuccess:function(){
          ecmSetConfig.qryKey = false;
          qry();
          info();
        },LoginCancel:function(){
          ecmSetConfig.qryKey = false;
        }});
      }
    }
  }
}
//樣式：獎項剩餘數
function show_goods_cnt_status(goods_cnt_status){
  if(goods_cnt_status){
    for(var i = 0; i < goods_cnt_status.length; i++){
      var lesscount = Math.max(0,goods_cnt_status[i].lesscount);
      momoj('#' + goods_cnt_status[i].goods).removeClass('off').addClass(lesscount<=0?'off':'').find('.lesscount').html(lesscount);
    }
  }
}
//樣式：集點卡
function show_nowPoint(nowPoint){
  var point = parseInt(nowPoint);
  var ten_num = parseInt(point / 10);
  var single_num = point % 10;
  var is10x = single_num == 0 && ten_num > 0;
  for(var i = 0; i < 10; i++){
    //數字#point_card li txt
    var numStr = (is10x ? ten_num-1 : ten_num)*10 + parseInt(i)+1
    momoj('#point_card').find('li').eq(i).removeClass('off').find('.txt').html(numStr);
    //蓋章li addClass('off')
    if(i < single_num || is10x){
      momoj('#point_card').find('li').eq(i).removeClass('off').addClass('off');
    }
  }
}//樣式：兌換紀錄
function show_qry_list(qry_list){
  momoj('#ref_history table .table3').remove();
  if(qry_list){
    for(var i = 0; i < qry_list.length; i++){
      var date = qry_list[i].date.substr(0,19);
      var goods = qry_list[i].goods;
      var goods_name = qry_list[i].goods_name;
      var point = qry_list[i].point;
      momoj('#ref_history table tbody').append('<tr class="table3"><td>' + date + '</td><td>' + point + '</td><td>' + (goods in ecmSetConfig.qryMsg ? ecmSetConfig.qryMsg[goods] : goods_name) + '</td></tr>');
    }
  }
  openDiv('ref_history');
}
momoj(document).ready(function(){
  info();
});