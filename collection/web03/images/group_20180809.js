﻿/***********************
 2018 短促機制頁-放閃購物節
 配對遊戲(0809-0817)
 hhyang 2018.08.01
 **********************/
var ecmSetConfig = {};
//取得圖片位置
ecmSetConfig.itjsSrc = momoj('#itjs').attr('src');
ecmSetConfig.imgEcm = ecmSetConfig.itjsSrc.substring(0,ecmSetConfig.itjsSrc.indexOf("images\/"));
ecmSetConfig.timestamp = ecmSetConfig.itjsSrc.split('?')[1];
// 圖檔路徑
function getImgPath(imgName){
  return ecmSetConfig.imgEcm + 'images/' + imgName + '?' + ecmSetConfig.timestamp;
}
function newPage(){
  window.open(edmShare.getUrl()+'&code='+shareCode);
}
var urlSearch = window.location.search.substring(1);
var params = JSON.parse('{"' + decodeURI(urlSearch).replace(/"/g, '\\"').replace(/=/g, '":"').replace(/&/g, '","') + '"}');
var url_code = (params.code)?params.code:'';
var returnMsg = {
  "NO_MORE"  :'無資格',
  "NO_GROUP" :'還沒有配對喔～',
  "CONTENT_NOT_MATCH":'格式錯誤，請重新輸入！<br/>輸入文字限輸入中英數50個字元，不能有標點符號&空格',//(邀請碼應為6碼由英數組成)
  "NOT_MATCH":'格式錯誤，請確認配對邀請連結是否正確？',//(邀請碼應為6碼由英數組成)
  "NOT_EXIST":'查無此配對邀請！請確認配對邀請連結是否正確？',
  "FULL"     :'此配對已完成，請嘗試其他配對邀請，或自行創建配對隊伍。',
  "A"        :'不可同時加入兩組配對喔~<br/>3秒後將導入您目前所在配對。',
  "creat_ok" :'提醒您～需邀請一名好友與您配對，始符合活動資格。<br/>點擊「邀請配對」分享你的專屬連結<br/>邀請戰友加入吧!!',
  "createMsg":'創建後就不能回覆其他配對邀請了，確定要創建?',
  "joinMsg"  :'加入配對後就不能更換對象了，確定要加入?',
  "input_ref_txt"        :'限輸入中英數50個字元，不能有標點符號&空格',
  "member_content_default" :'小夥伴還沒回覆...',
  "member_name_default"    :'小夥伴',
};
function clickInfo(){
  info('click', '');
}
function pageInfo(){
  info('', url_code);
  momoj('#creatBtn').click(function(){
    //點擊「創建」，將輸入框上「傳送」紐加上createMsg()後顯示
    //點擊「傳送」，將系統訊息上「確認」紐加上create()後顯示確認創建訊息
    //點擊「確認」，開始創建。若CONTENT_NOT_MATCH，重複點擊「創建」後步驟
    click_create(returnMsg['input_ref_txt']);
  });
  momoj('#members_enough_N').click(function(){
    click_join(returnMsg['input_ref_txt']);
  });
  momoj('#m_share').attr('onClick', (edmShare.isApp())?'share(\'APP\');':'openDiv(\'#fb_line_ref\');');//小網||APP 大網已經用RWD隱藏了
}
var input_content = '';
function click_create(txt) {
  momoj('#input_ref .btn_mode').attr('onClick', 'createMsg();');
  //openDiv('#input_ref');
  altMsg_popDiv('input_ref', txt);
}
function click_join(txt) {
  momoj('#input_ref .btn_mode').attr('onClick', 'joinMsg();');
  //openDiv('#input_ref');
  altMsg_popDiv('input_ref', txt);
}
function createMsg() {
  input_content = top.momoj('#MoMoLMContent #input').val();
  momoj('#sure_ok').attr('onClick', 'create();');
  altMsg_popDiv('check_cancel_ref', returnMsg['createMsg']);
}
function joinMsg() {
  input_content = top.momoj('#MoMoLMContent #input').val();
  momoj('#sure_ok').attr('onClick', 'join();');
  altMsg_popDiv('check_cancel_ref',  returnMsg['joinMsg']);
}
function info(flag, group_code) {
  group_code = (typeof group_code == "undefined" || group_code.length <= 0)?'':group_code;
  var data={
    doAction : 'info',
    group_no : group_code,
    content : '',
  };
  var rtnData = promoGroupAjax(data);
  //無資格浮層{"status":"NO_MORE",   "isLogin":"IS_LOGIN", "isInTime":"IN_TIME","isAPP":"NOT_APP"} 無資格      有CODE/錯CODE/無CODE
  //預設活動頁{ status: "NOT_LOGIN",  isLogin: "NOT_LOGIN", isInTime: "IN_TIME", isAPP: "NOT_APP"} 未登入 ---- 無CODE
  //預設活動頁{ status: "NO_GROUP",   isLogin: "IS_LOGIN",  isInTime: "IN_TIME", isAPP: "NOT_APP"} 已登入 無組 無CODE
  /*預設活動頁{ status: "NOT_MATCH",  isLogin: "NOT_LOGIN", isInTime: "IN_TIME", isAPP: "NOT_APP"} 未登入 ---- 不符格式
        加               NOT_EXIST             (IS_LOGIN)                                        (=已登入 無組)隊伍編號不存在
    提示訊息框           FULL                                                                                  已額滿 */
               
  /*配對對話框{"status":"OK",        "isLogin":"NOT_LOGIN","isInTime":"IN_TIME","isAPP":"NOT_APP", 未登入 ---- 有CODE   "groupCode":"7PW9KS","members_enough":"N","members_name":["參"],"members_content":["歡喜拿好禮3"]}*/
  //配對對話框{"status":"OK",        "isLogin":"IS_LOGIN",                                       (=已登入 無組)有CODE   "groupCode":"7PW9KS","members_enough":"N","members_name":["參"],"members_content":["歡喜拿好禮3"]}
  /*配對對話框{"status":"GROUP_INFO","isLogin":"IS_LOGIN", "isInTime":"IN_TIME","isAPP":"NOT_APP", 已登入 有組 有CODE   "groupCode":"TESTXD","members_enough":"N","members_name":["寺"],"members_content":["55555"],       有組  "index_of_self":"0","joinAgainQulify":"N"}
        加
    回覆留言框*/

  if(rtnData != '-1') {
    var status = rtnData.status;//"NOT_MATCH"隊伍編號不符格式; "NOT_EXIST"隊伍編號不存在; "FULL"已額滿
    if(rtnData.groupCode){
      groupInfo(rtnData);
      return;
    }else{
      showDiv('start');
      if(status == 'NOT_MATCH' || status == 'NOT_EXIST' || status == 'FULL'){
        altMsg_popDiv('check_ref', returnMsg[status]);
      }else if(flag == 'click' && status == 'NO_GROUP') {
        altMsg_popDiv('check_ref', returnMsg['NO_GROUP']);
      }else if(flag == 'click' && rtnData.isLogin == 'NOT_LOGIN') {
        momoj().MomoLogin({flag:false, LoginSuccess:function() {
          info(flag, group_code);
        }});
      }
    }
  }
}
var createKey = false;
function create() {
  if(!createKey){
    createKey = true;
    
      //chk regex?
      var data={
        doAction   : 'create', 
        group_no : '',
        content : input_content,
      };
      var rtnData = promoGroupAjax(data);
      //EXPIRED NOT_APP NO_MORE
      //未登入  { status: "NOT_LOGIN",  isLogin: "NOT_LOGIN", isInTime: "IN_TIME", isAPP: "IS_APP"}
      //不符格式{ status: "NOT_MATCH",  isLogin: "IS_LOGIN",  isInTime: "IN_TIME", isAPP: "IS_APP"}  已登入 無組  不符格式
      //成功創建{ status: "OK",         isLogin: "IS_LOGIN",  isInTime: "IN_TIME", isAPP: "IS_APP", groupCode: "MRMPJG"}
      //已有隊伍{"status":"GROUP_INFO","isLogin":"IS_LOGIN", "isInTime":"IN_TIME","isAPP":"IS_APP","groupCode":"MRMPJG","members_enough":"N","index_of_self":"0","members_name":["寺"],"joinAgainQulify":"N","members_content":["稀哩嘩啦"]}
      if(rtnData != '-1') {
        var status = rtnData.status;
        if( status == 'GROUP_INFO'){
          altMsg_popDiv('check_ref', returnMsg['A']);
          setTimeout(function(){
            closeDiv();
            groupInfo(rtnData);
          },3000);
        }else if(status == 'NOT_LOGIN') {
//          showDiv('start');
          momoj().MomoLogin({flag:false, LoginSuccess:function() {
            createKey = false;
            create();
          },LoginCancel:function(){
            createKey = false;
          }});
        }else if( status == 'OK'){
          altMsg_popDiv('check_ref', returnMsg['creat_ok']);
          info('');
        }else if(status == 'CONTENT_NOT_MATCH'){
          click_create(returnMsg[status]);
        }
        createKey = false;
      }
  }
}
var joineKey = false;
function join() {
  if(!joineKey){
    joineKey = true;
    setTimeout(function(){
      //chk regex?

      var data={
        doAction : 'join', 
        group_no : url_code.toUpperCase(),// LowerCase(),//轉大寫
        content : input_content,
      };
      var rtnData = promoGroupAjax(data);
      if(rtnData != '-1') {
        var status = rtnData.status;
        if( status == 'GROUP_INFO'){
          altMsg_popDiv('check_ref', returnMsg['A']);
          setTimeout(function(){
            closeDiv();
            groupInfo(rtnData);
          },3000);
        }else if(status == 'NOT_LOGIN') {
  //        showDiv('start');
            momoj().MomoLogin({flag:false, LoginSuccess:function() {
              joineKey = false;
              join();
            },LoginCancel:function(){
              joineKey = false;
            }});
        }else if( status == 'OK'){
          info('');
          closeDiv();
        }else if(status == 'CONTENT_NOT_MATCH'){
          click_join(returnMsg[status]);
        }else if( status == 'NOT_MATCH' || status == 'NOT_EXIST' || status == 'FULL'){
          altMsg_popDiv('check_ref', returnMsg[status]);
        }
        joineKey = true;
      }else{
        alert("很抱歉!伺服器暫時無法連線，請稍候再試"); 
        joineKey = true;
      }
    }, 1000);//同狀況科一延遲
  }
}
var promoGroupAjaxKey = false;
function promoGroupAjax(data){
  if(!promoGroupAjaxKey){
    promoGroupAjaxKey = true;
    var result = '-1';
    momoj.ajax({
      type       :'POST',
      url        :'/ajax/promotionEvent_P020180809.jsp',
      contentType:'application/x-www-form-urlencoded; charset=BIG5',
      async      :false,
      cache      :false,
      data       :data,
      dataType   :'json',
      timeout    :30000,
      success    :function(rtnData){
        var status= rtnData.status;
        if(status == 'F' || status == 'ERR') {
          alert("很抱歉!伺服器暫時無法連線，請稍候再試");
        }else if(status == 'EXPIRED'){
          showDiv('start');
          alert('請於活動時間內登入參加');
        }else if(status == 'NO_MORE') {
          showDiv('start');
          altMsg_popDiv('check_ref', returnMsg['NO_MORE']);
        }else if(status == 'NO_GROUP') {
          //showDiv('start');
          result = rtnData;
        }else if(status == 'NOT_APP') {
          //showDiv('start');
          if(confirm('提醒您~此活動限使用momo購物網app參加~\n是否開啟APP？')){
            location.href = urlStr;
            return;
          }else{
            openDiv('#QR_ref');
          }
        }else{
          result = rtnData;
        }
        promoGroupAjaxKey = false;
      },
      error      :function(){
        promoGroupAjaxKey = false;
        alert("ERROR\n很抱歉!伺服器暫時無法連線，請稍候再試");      
      }
    });
    return result;
  }
}
var shareCode = '';
/*有組  "index_of_self":"0","joinAgainQulify":"N"}*/
function groupInfo(rtnData){//self_index
  var groupCode        = rtnData.groupCode;        //隊伍編號  "groupCode":"TESTXD",
  var members_enough   = rtnData.members_enough;   //是否成團  "members_enough":"N",
  var members_name     = rtnData.members_name;     //隊友名字  "members_name":["寺"],
  var members_content  = rtnData.members_content;  //隊友留言  "members_content":["55555"],

  shareCode = groupCode//for share
  //顯示回覆傳情 || 完成配對 || 活動詳情
  if(members_enough == 'N'){
    if(rtnData.index_of_self){
      showStatusBtn('share');
    }else{
      showStatusBtn('members_enough_N');
    }
  }else{
    showStatusBtn('members_enough_Y');
    momoj('#members_enough_Y_title').show();
  }
  //稱謂+留言
  for(var i = 0; i < members_name.length; i++){
    if(i == rtnData.index_of_self){
      members_name[i] = '我';
      if(i == 0){//自己是邀請人，另一個成員就是小夥伴
        momoj('#name_of_member1').html(returnMsg['member_name_default']);
        momoj('#content_of_member1 p').html(returnMsg['member_content_default']);
      }
    }else{
      members_name[i] = '好友_' + members_name[i];
    }
    if(i > 0){//頭像
      momoj('#photo_of_member1').removeClass('nameB').find('img').attr('src', getImgPath('kiwi_p1.png'));
    }
    momoj('#name_of_member'+i).html(members_name[i]);
    momoj('#content_of_member'+i).removeClass('textB').addClass('textA').find('p').html(members_content[i]);
  }
  showDiv('game');
}
function openDiv(divSelector) {
  var resultSet = top.momoj(divSelector).html();
  top.momoj().LayerMask({contentWidth:'100%', contentHeight:'auto'}).open();
  top.momoj('#MoMoLMContent').empty();
  top.momoj('#MoMoLMContent').html(resultSet).css({position:"absolute", background:"transparent"});
}
function showStatusBtn(showId){
  momoj('.statusBtn').hide();//clear
  momoj('#'+showId).show();
}
function showDiv(id) {
  document.getElementById('start').style.display = "none";
  document.getElementById('game').style.display = "none";
  $('#' + id).show();
}
function altMsg_popDiv(refId, msg){
  momoj('#' + refId + ' .box .txt').show().html(msg);
  openDiv('#' + refId);
}
momoj(document).ready(function(){
  pageInfo();
});
/*****************
 ***** 分享 ******
 *****************/
//ins: "OK", nowStage: "0", status: "OK", isLoginOK: "T"
function share(ShareToWhere) {
  if(ShareToWhere != 'facebook' && ShareToWhere != 'line' && ShareToWhere != 'APP') {
    return;
  }
  if(momoj().cookie('loginUser') == null || momoj().cookie("ccmedia") == null){
    if(confirm('登入會員再分享，才可成功發送邀請碼！\n是否登入會員？')){
      momoj().MomoLogin({flag:false, LoginSuccess:function() {
        shareSet(ShareToWhere, shareCode);
      }});
    }else{
        shareSet(ShareToWhere, shareCode);
    }
  }else{
    shareSet(ShareToWhere, shareCode);
  }
}
function shareSet(ShareToWhere, code) {
  var isMainPage = (typeof code == "undefined" || code.length <= 0);
  code = isMainPage?"":code;
  //網址
  var urlStr = 'https://www.momoshop.com.tw/edm/cmmedm.jsp?lpn=O21csULyi6V&n=1';//edmShare.getUrl();//分享網址
  var fb_share_url   = urlStr + (isMainPage?'':'&code='+code) + '&utm_source=FB&utm_medium=Freepair';
  var line_share_url = urlStr + (isMainPage?'':'&code='+code) + '&utm_source=line&utm_medium=Freepair';
  var main_url       = urlStr + (isMainPage?'':'&code='+code) + '&utm_medium=Freepair';
  //文案
  var title = '雙雙對對訂單免費';
  var subtitle = '2人一起買，抽訂單免費，快來跟我配對！';

  if(ShareToWhere == 'facebook') {
    shareUtil.edmShareToFb(fb_share_url, title, subtitle);
  } else if (ShareToWhere == 'line') {
    shareUtil.edmShareToLine(line_share_url, '「'+title+'」', subtitle);
  } else if (ShareToWhere == 'APP') {
    edmShare.app('「'+title+'」', subtitle, main_url);
  }
}