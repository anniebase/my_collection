
$(function () { 
        //頁籤輪輪播class
		$(".Area_viedo .box_PD .content_Area > ul").addClass('swiper-wrapper');
        $(".Area_viedo .box_PD .content_Area > ul > li").addClass('swiper-slide');
});



$(window).load(function(){
	
	var box_PD_swiper = new Swiper('.Area_viedo .box_PD .content_Area', {
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area_viedo .box_PD .swiper-pagination',  
        paginationClickable: true, //觸擊切換

        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area_viedo .box_PD .swiper-button-next', 
        prevButton: '.Area_viedo .box_PD .swiper-button-prev',

		//基本
		loop: true, //無限循環
		
        //排版
        slidesPerView: 1, //顯示幾個
		spaceBetween:10, //間距
		
        //自動撥放
        autoplay: 3000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
        
        //切換特效(翻牌)
        effect: 'flip', //切換特效 cube(立方體) coverflow(3D) flip(翻牌)
        centeredSlides: true, //目前區塊居中
        slidesPerView: 'auto', //顯示改回自動
        flip: {
            slideShadows : true,	//slides的陰影。默認true。
            limitRotation : true,	//限制最大旋轉角度為180度，默認true。
        },

	});
	box_PD_swiper.onResize();
})
