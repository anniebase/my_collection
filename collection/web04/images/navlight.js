$.debounce = function(func, wait, immediate) {
	var timeout
	return function() {
		var context = this, args = arguments
		later = function() {
			timeout = null
			if (!immediate) func.apply(context, args)
		}
		var callNow = immediate && !timeout
		clearTimeout(timeout)
		timeout = setTimeout(later, wait)
		if (callNow) func.apply(context, args)
	}
}
$.throttle = function(func, wait) {
	var context, args, timeout, throttling, more, result
	var whenDone = $.debounce(function() {
		more = throttling = false
	}, wait)
	return function() {
		context = this, args = arguments
		var later = function() {
			timeout = null
			if (more) func.apply(context, args)
			whenDone()
		}
		if (!timeout) timeout = setTimeout(later, wait)
		
		if (throttling) {
			more = true
		} else {
			result = func.apply(context, args)
		}
		whenDone()
		throttling = true
		return result
	}
}
$.fn.topSuction = function(option) {
	option = option || {}
	var fixCls = option.fixCls || 'cate-fixed'
	var navarea = option.navarea || '.navarea'
	var navbox = option.navbox || '.navbox'
	var fixedFunc = option.fixedFunc
	var resetFunc = option.resetFunc

	var $self = this
	var $win  = $(window)
	if (!$self.length) return

	var offset = $self.offset()
	var fTop   = offset.top
	var fLeft  = offset.left

	//暫存
	$self.data('def', offset)
	$win.resize(function() {
		$self.data('def', $self.offset())
	})

	$win.scroll(function() {
		var dTop = $(document).scrollTop()
		if (fTop < dTop) {
			$self.addClass(fixCls)
			var navareawidth = $(navarea).width()
			$self.children(navbox).width( navareawidth )
			if (fixedFunc) {
				fixedFunc.call($self, fTop)
			}
		} else {
			$self.removeClass(fixCls)
			if (resetFunc) {
				resetFunc.call($self, fTop)
			}
		}
	})
	
	//進頁面就執行
	var dTop = $(document).scrollTop()
	if (fTop < dTop) {
		$self.addClass(fixCls)
		var navareawidth = $(navarea).width()
		$self.children(navbox).width( navareawidth )
		if (fixedFunc) {
			fixedFunc.call($self, fTop)
		}
	} else {
		$self.removeClass(fixCls)
		if (resetFunc) {
			resetFunc.call($self, fTop)
		}
	}
	
};

/*
 * 導覽/選單高亮組件
 * option
 *   navs 	導覽/選單區塊選取器
 *   nav 		導覽/選單內容選取器
 *   content 	內容區塊選取器
 *   diffTop 	距離頂部的誤差值
 *   diffBottom 距離底部的誤差值
 *   lightCls 	高亮的class
 *   navopen	導覽/選單展開的class
 * 
 */
$.fn.navLight = function(option, callback) {
	option = option || {}
	var navarea = option.navarea || '.navarea'
	var navs = option.navs || '.navs'
	var nav = option.nav || '.nav'
	var content = option.content || '.content'
	var diffTop = option.diffTop || $(window).height()/4
	var diffBottom = option.diffBottom || 0
	var lightCls = option.lightCls || 'cate-hover'
	var navopen = option.navopen || 'cate-open'
	var open = option.open
	var $self = $(this)
	var $nav = $self.find(nav)
	var $content = $self.find(content)
	// 記錄每個選單的位置
	var navPosi = $nav.map(function(idx, elem) {
		var $cont = $(elem)
		var left = $cont.offset().left
		var width = $cont.outerWidth(true)
		return {
			left: left,
			width: width,
			jq: $cont
		}
	})
	// 記錄每個內容區塊的位置
	var contentPosi = $content.map(function(idx, elem) {
		var $cont = $(elem)
		var top = $cont.offset().top
		var bottom = $cont.offset().top
		var height = $cont.height()
		return {
			top: top-diffTop,
			bottom: top+height+diffBottom,
			jq: $cont
		}
	})
	//console.log(contentPosi);
	var $win = $(window)
	var $doc = $(document)
	var handler = $.throttle(function(e) {
		var dTop = $doc.scrollTop()
		highLight(dTop)
		//console.log(dTop)
	}, 100)

	function highLight(docTop) {
		contentPosi.each(function(idx, posi) {
			if ( posi.top < docTop && posi.bottom > docTop ) {
				//選單移動
				var left = navPosi[idx].left
				var center = ( $win.width() - navPosi[idx].width )/2
				$(navs).stop().animate({
					scrollLeft: left - center
				},100)
				//高亮
				$nav.removeClass(lightCls)
				$nav.eq(idx).addClass(lightCls).siblings()				
				if (callback) {
					callback($nav, $content)
				}
			}
		})
	}
	
	
	if (open) {
		$self.delegate( nav , ' click', function(e) { //不用touchstart會誤觸
			var $na = $(this)
			var idx = $nav.index($na)
			var $cont = $content.eq(idx)
			var top = $cont.offset().top
			var height = $nav.outerHeight(true)
			$(navarea).removeClass('cate-open')

			//樣式為置底時,不算選單高度
			if( $(navarea).hasClass('NavArea-fixed-bottom') == true ){
				height = 0;
			};

			//錨點到指定區塊
			var top_i = 0;
			//var top_i = $('.content_Area').prev('.mo_img').height();  //公版標題是前面接圖片時
			
			$('html,body').animate({
				scrollTop: top - height - top_i + 'px'
			})
			//console.log(height ,top)
			e.preventDefault()
		})
	}
	$win.scroll(handler)
};



/** 選單套件 
  *  1.捲過選單置頂
  *  2.點選選單,錨到內容
  *  3.捲過內容,選單高亮,高亮置中
  *  4.點選btn,展開選單

**/
$(window).load(function(){
	var $WRAPPER = $('.WRAPPER'); //最大包
	$WRAPPER.find('.NavArea').topSuction({
		fixCls:   'cate-fixed',		//選單超過置頂的class
		navarea:  '.NavArea',		//導覽/選單套件選取器
		navbox:   '.Nav_box',		//導覽/選單區塊選取器
	});  
	$WRAPPER.navLight({
		navarea:  '.NavArea',		//導覽/選單套件選取器
		navs:     '.Nav',			//導覽/選單內容大包選取器
		nav: 	  '.Nav li',		//導覽/選單內容選取器
		content:  '.content_BOX',	//內容區塊選取器
		lightCls: 'cate-hover',		//高亮的class
		navopen:  'cate-open',		//導覽/選單展開的class
		open: true,					//導覽/選單內容點擊啟動
	});
	
	//展開選單
	$('.NavArea').delegate(".Btn","click",function(){
		$('.NavArea').toggleClass('cate-open');
		$('html,body').animate({
			scrollTop: $('.Nav_box').offset().top //點按展開置頂
		},100)
	});
	$('.NavArea').delegate(".Nav_bg","click",function(){
		$('.NavArea').removeClass('cate-open');
	});
	
	//樣式為置底時,隱藏系統地
	if( $('.NavArea').hasClass('NavArea-fixed-bottom') == true ){
		$('.footerArea').hide();
	};
	
});




