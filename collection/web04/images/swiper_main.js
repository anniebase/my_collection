
$(function () { 
		
		/*銀行輪播*/
		$(".box_acty_bank .acty_bank").addClass('swiper-wrapper');
        $(".box_acty_bank .acty_bank > a").addClass('swiper-slide');

		/*直播輪播*/
		$(".box_acty_live .acty_live").addClass('swiper-wrapper');
        $(".box_acty_live .acty_live > a").addClass('swiper-slide');

		/*分會場輪播*/
		$(".box_acty_cantantBaseBN .acty_cantantBaseBN").addClass('swiper-wrapper');
        $(".box_acty_cantantBaseBN .acty_cantantBaseBN > a").addClass('swiper-slide');
	
		/*剁手搶輪播*/
		$(".layout_1220_2xn_0308A .box").addClass('swiper-wrapper');
		$(".layout_1220_2xn_0308A .box > ul").addClass('swiper-slide');

});



$(window).load(function(){
	
/*分會場輪播*/
	var box_acty_bank_swiper = new Swiper('.box_acty_bank', {
		
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.box_acty_bank .swiper-pagination',  
        paginationClickable: true, //觸擊切換
		
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
       // nextButton: '.swiper-button-next', 
        //prevButton: '.swiper-button-prev',
        nextButton: '.box_acty_bank .button_next', 
        prevButton: '.box_acty_bank .button_prev',
				
		//排版
		slidesPerView: 1, //顯示幾個
        slidesPerGroup: 1, //一次切換幾個
		spaceBetween: 0, //間距

        //自動撥放
        autoplay: 2500, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播


		
	}); 

/*直播輪播*/
	var box_acty_bank_swiper = new Swiper('.box_acty_live', {
		
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.box_acty_live .swiper-pagination',  
        paginationClickable: true, //觸擊切換
		
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
       // nextButton: '.swiper-button-next', 
        //prevButton: '.swiper-button-prev',
        nextButton: '.box_acty_live .button_next', 
        prevButton: '.box_acty_live .button_prev',
				
		//排版
		slidesPerView: 1, //顯示幾個
        slidesPerGroup: 1, //一次切換幾個
		spaceBetween: 0, //間距

        //自動撥放
        autoplay: 2500, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

        //切換特效(淡化)
        effect: 'fade',     //切換特效 fade(淡化) cube(立方體) coverflow(3D) flip(翻牌)
        fade: {
            crossFade: true //打開自動淡出
        },		
	}); 
	//固定時間更換圖片
	$(function (){
		var today = new Date(); 
		var year = today.getFullYear(); //西元年
		var month = today.getMonth()+1; //月
		var day = today.getDate(); //日
		var hour = today.getHours();  //時
		var minute = today.getMinutes();  //分
		var second = today.getSeconds();  //秒
		var week = today.getDay(); //星期0~6
		var todaytxt = today;
	
	/*過期消失*/
	var Mydate  = new Date("2018/7/20  18:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(19).remove() };
	var Mydate  = new Date("2018/7/20  16:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(18).remove() };
	var Mydate  = new Date("2018/7/20  14:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(17).remove() };
	var Mydate  = new Date("2018/7/20  12:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(16).remove() };
	var Mydate  = new Date("2018/7/20  10:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(15).remove() };

	var Mydate  = new Date("2018/7/19  18:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(14).remove() };
	var Mydate  = new Date("2018/7/19  16:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(13).remove() };
	var Mydate  = new Date("2018/7/19  14:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(12).remove() };
	var Mydate  = new Date("2018/7/19  12:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(11).remove() };
	var Mydate  = new Date("2018/7/19  10:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(10).remove() };
	
	var Mydate  = new Date("2018/7/18  18:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(9).remove() };
	var Mydate  = new Date("2018/7/18  16:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(8).remove() };	
	var Mydate  = new Date("2018/7/18  14:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(7).remove() };
	var Mydate  = new Date("2018/7/18  12:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(6).remove() };
	var Mydate  = new Date("2018/7/18  10:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(5).remove() };
	
	var Mydate  = new Date("2018/8/17  18:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(4).remove() };	
	var Mydate  = new Date("2018/8/17  16:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(3).remove() };
	var Mydate  = new Date("2018/8/17  14:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(2).remove() };
	var Mydate  = new Date("2018/8/17  12:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(1).remove() };
	var Mydate  = new Date("2018/8/17  10:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(0).remove() };
	
	});	
	
	
	/*剁手搶*/
	var layout_1220_2xn_0308A_swiper = new Swiper('.layout_1220_2xn_0308A', {
		
		//小圓點
		pagination: '.layout_1220_2xn_0308A .swiper-pagination',
		paginationClickable: true, //觸擊切換
		
		//左右切換
		nextButton: '.layout_1220_2xn_0308A .button-next',
		prevButton: '.layout_1220_2xn_0308A .button-prev',
        //自動撥放
     //  autoplay: 2500, //自動輪播間隔時間
    //   autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
		//排版
		loop: true,
        centeredSlides: true, //當前區塊居中
		slidesPerView: '1.5', //顯示幾個
		spaceBetween: 5, //間距

	}); 
	



	/*XXX*/
	var box_acty_bank_swiper = new Swiper('.box_acty_cantantBaseBN', {
		
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.box_acty_cantantBaseBN .swiper-pagination',  
        paginationClickable: true, //觸擊切換
		
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
       // nextButton: '.swiper-button-next', 
        //prevButton: '.swiper-button-prev',
        nextButton: '.box_acty_cantantBaseBN .button_next', 
        prevButton: '.box_acty_cantantBaseBN .button_prev',
				
		//排版
		slidesPerView: 1, //顯示幾個
        slidesPerGroup: 1, //一次切換幾個
		spaceBetween: 0, //間距

        //自動撥放
        autoplay: 2500, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

        //切換特效(翻牌)
        effect: 'flip', //切換特效 cube(立方體) coverflow(3D) flip(翻牌)
        centeredSlides: true, //目前區塊居中
        slidesPerView: 'auto', //顯示改回自動
        flip: {
            slideShadows : false,	//slides的陰影。默認true。
            //limitRotation : false,	//限制最大旋轉角度為180度，默認true。
        },

	}); 


})
