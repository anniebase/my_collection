/*搶紅包倒數v2-20180401*/
function clockSimple(){
		var day = 1 ;
		var nowTime = new Date; 
		var nowHours = nowTime.getHours();
		var nowMinutes = nowTime.getMinutes();
		var nowSecond = nowTime.getSeconds();
		var now_all_Minutes = nowHours*60 + nowMinutes; //目前分鐘數
		//console.log('目前時間: '+nowHours +'時'+ nowMinutes +'分'+ nowSecond +'秒,目前分鐘數:'+now_all_Minutes);
		
		var reHours = 24*day-nowHours-1;
		var reMinutes = 60-nowMinutes-1;
		var reSecond = 60-nowSecond;
		var re_all_Minutes = reHours*60 + reMinutes; //剩餘分鐘數
		//console.log('剩餘時間: '+reHours +'時'+ reMinutes +'分'+ reSecond +'秒,剩餘分鐘數:'+re_all_Minutes);

		//搶紅包時間波段設定
		var Time = [
			//波段+1(00)
			[00,00],	//0
			[09,10],	//1
			[11,10],	//2
			[13,10],	//3
			[15,10],	//4
			[17,10],	//5
			[19,10],	//6
			[21,10],	//7
		];
		var Minutes_Time = [
			//波段分鐘數+1(00)
			Time[0][0]*60 + Time[0][1],
			Time[1][0]*60 + Time[1][1],
			Time[2][0]*60 + Time[2][1],
			Time[3][0]*60 + Time[3][1],
			Time[4][0]*60 + Time[4][1],
			Time[5][0]*60 + Time[5][1],
			Time[6][0]*60 + Time[6][1],
			Time[7][0]*60 + Time[7][1],
		]
		//console.log('Minutes_Time: '+ Minutes_Time);

		//動作
		if( now_all_Minutes >=  Minutes_Time[Time.length-1]){
			//最後1波	
			//console.log('最後1波, 下一檔時段'+ Time[1][0] + ':' +  Time[1][1] , ', 倒數'+ (1440 - now_all_Minutes +Minutes_Time[1]) +'分');
			$('.giftReMinutes').html( 1440 - now_all_Minutes +Minutes_Time[1]); //1440-目前分鐘+第一波分鐘
			if ( Time[1][0] < 10 ){ Time[1][0] = '0'+Time[1][0]}
			if ( Time[1][1] < 10 ){ Time[1][1] = '0'+Time[1][1]}
			$('.nextTime').html( Time[1][0] + '<span class="timeout-play">:</span>' +  Time[1][1]);
		} else {
			//第1波~倒數第2波
			for (var i = 0; i < Time.length-1; i++){
				if ( Minutes_Time[i] <= now_all_Minutes && now_all_Minutes < Minutes_Time[i+1] ){
					//console.log('快到第'+ (i+1) +'波, 下一檔時段'+ Time[i+1][0] + ':' +  Time[i+1][1] , ', 倒數'+ (Minutes_Time[i+1] - now_all_Minutes)+'分');
					$('.giftReMinutes').html( Minutes_Time[i+1] - now_all_Minutes );
					if ( Time[i+1][0] < 10 ){ Time[i+1][0] = '0'+Time[i+1][0]}
					if ( Time[i+1][1] < 10 ){ Time[i+1][1] = '0'+Time[i+1][1]}
					$('.nextTime').html( Time[i+1][0] + '<span class="timeout-play">:</span>' +  Time[i+1][1]);
				}
			}	
		}
		setTimeout("clockSimple()",1000)
	}
	clockSimple(); 
