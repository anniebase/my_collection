﻿/* 回版頭*/
jQuery(function(){
	jQuery("#gotop").click(function(){
		jQuery("html,body").stop(true,false).animate({scrollTop:0}); //設定回頁面頂端
		return false;	
	});
    jQuery(window).scroll(function() {
        if ( jQuery(this).scrollTop() > 300){ //設定大於300px才顯示浮層
            jQuery('#gotop').fadeIn("fast");
        } else {
            jQuery('#gotop').stop().fadeOut("fast");
        }
    });
}); 

/* 滑動的GOTO */
function goTop(val) {
	var gotop_i = 0
	jQuery('html,body').animate({scrollTop: jQuery(val).offset().top - gotop_i });
}


/*滑鼠移置特定高度時，黏人精顯現 */
jQuery(window).scroll(function() {
	if ( jQuery(this).scrollTop() > 300){ //設定大於300px才顯示浮層
		$(".fixarea").show('fast');
	} else {
		$(".fixarea").hide();
	}
});

/*進入後集氣條動畫*/
$(function(){
	var bar_line_w = $("#bar_line").attr('data-width')
	$("#bar_line").animate({ width: bar_line_w })
});

/** 選單一直copy **/
$(function () {
	var $menutableArea =  $(".menutableArea");
	var $menutableAreacopy = $(".menutableAreacopy");
	$menutableAreacopy.html( $menutableArea );
	$('.iframeArea').each(function() {
		for ( i = 0; i <= $menutableAreacopy.length; i++ ) {
			$(this).find('.menutableAreacopy').eq(i).children().children().children("li").eq(i).addClass("cate_hover");
		}
    });
});

/*天變色
jQuery(function(){
    jQuery(window).scroll(function() {
		if ($(window).width() > 767 ){
			var ST = 100;
		} else {
			var ST = 50;
		}
        if ( jQuery(this).scrollTop() > ST){ //設定大於300px才顯示浮層
            jQuery('.cantantBase').addClass('fixactive');
        } else {
            jQuery('.cantantBase').removeClass('fixactive');
        }
    });
});*/

/* 浮層區*/
function agree(val) {
	$(val).fadeIn();
	var winST =  jQuery(window).scrollTop(); //目前位置
	var winH =  jQuery(window).height(); //裝置高度
	//浮層高度
	$(val).find('.agreeArea .txtArea').css('height', winH * 60 / 100 );
	var this_agreeH = $(val).find('.agreeArea').height();
	//浮層top定位
	$('.agreeArea').css('top', winST + winH/2 - this_agreeH/2 );
}
$(function(){
	var blackBox = $(".blackBox");
	var blackBox_close = $(".blackBox .close , .blackBox .but-close");
	var blackBox_BOXclose = ".Boxclose , .fixedfooterArea_B ";
	//點按鈕關閉
	blackBox_close.delegate( "a" ,"touchstart click",function(e){
		$(blackBox).fadeOut();
		e.preventDefault();
	});
	//點黑區關閉
	blackBox.delegate( blackBox_BOXclose ,"touchstart click",function(e){
		$(blackBox).fadeOut();
		e.preventDefault();
	});
});
 


/*TimeSwitch指定時間開關物件
  -----------------------------------------------
  啟動器: data-TimeSwitch_start="2018/2/12 00:00:00" data-TimeSwitch_end="2018/2/20 23:59:59" data-TimeSwitch_Myswitch="0"
  說明:
  data-TimeSwitch_start		開始時間
  data-TimeSwitch_end		結束時間
  data-TimeSwitch_Myswitch	動作 0刪除、1打開
  -----------------------------------------------*/
$(function() {
	$("[data-TimeSwitch_start]").each(function() {
		var TimeSwitch = new Date();
		var TimeSwitchmonth  = TimeSwitch.getMonth()+1; //月
		var TimeSwitchday    = TimeSwitch.getDate(); //日
		var TimeSwitchhour   = TimeSwitch.getHours();  //時
		var TimeSwitchminute = TimeSwitch.getMinutes();  //分
		var TimeSwitchsecond = TimeSwitch.getSeconds();  //秒
		var TimeSwitchweek   = TimeSwitch.getDay(); //星期0~6 
		if( TimeSwitchmonth < 10 ){TimeSwitchmonth = '0' + TimeSwitchmonth;}  
		if( TimeSwitchday   < 10 ){TimeSwitchday   = '0' + TimeSwitchday;}  
		//範圍時間
		var Mydate_start = new Date( $(this).attr('data-TimeSwitch_start') );
		var Mydate_end   = new Date( $(this).attr('data-TimeSwitch_end') );
		var Myswitch     = $(this).attr('data-TimeSwitch_Myswitch') ;
		//Myswitch = 0 隱藏
		if ( Myswitch == 0){
				if ( Mydate_start <= TimeSwitch && TimeSwitch <= Mydate_end ) {
						$(this).remove();  //Myswitch:0, 時間內,刪除
				} else {
						$(this).show();  //Myswitch:0, 時間外,打開
				}
		}
		//Myswitch = 1 打開
		if ( Myswitch == 1){
				if ( Mydate_start <= TimeSwitch && TimeSwitch <= Mydate_end ) {
						$(this).show();   //Myswitch:1, 時間內,打開
				} else {
						$(this).remove();   //Myswitch:1, 時間外,刪除
				}
		}
	});
});


//固定時間更換圖片
/*$(function (){
	var today = new Date(); 
	var year = today.getFullYear(); //西元年
	var month = today.getMonth(); //月
	var day = today.getDate(); //日
	var hour = today.getHours();  //時
	var minute = today.getMinutes();  //分
	var second = today.getSeconds();  //秒
	var week = today.getDay(); //星期0~6
	var todaytxt = today;

	*//*過期消失*//*
	var Mydate  = new Date("2018/7/20  18:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(19).remove() };
	var Mydate  = new Date("2018/7/20  16:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(18).remove() };
	var Mydate  = new Date("2018/7/20  14:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(17).remove() };
	var Mydate  = new Date("2018/7/20  12:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(16).remove() };
	var Mydate  = new Date("2018/7/20  10:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(15).remove() };

	var Mydate  = new Date("2018/7/19  18:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(14).remove() };
	var Mydate  = new Date("2018/7/19  16:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(13).remove() };
	var Mydate  = new Date("2018/7/19  14:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(12).remove() };
	var Mydate  = new Date("2018/7/19  12:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(11).remove() };
	var Mydate  = new Date("2018/7/19  10:30:00"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(10).remove() };
	
	var Mydate  = new Date("2018/7/18  18:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(9).remove() };
	var Mydate  = new Date("2018/7/18  16:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(8).remove() };	
	var Mydate  = new Date("2018/7/18  14:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(7).remove() };
	var Mydate  = new Date("2018/7/18  12:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(6).remove() };
	var Mydate  = new Date("2018/7/18  10:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(5).remove() };
	
	var Mydate  = new Date("2018/7/17  18:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(4).remove() };	
	var Mydate  = new Date("2018/7/17  16:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(3).remove() };
	var Mydate  = new Date("2018/7/17  14:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(2).remove() };
	var Mydate  = new Date("2018/7/17  12:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(1).remove() };
	var Mydate  = new Date("2018/7/17  10:29:59"); 	if ( Mydate <= today ) { $('.layout_1220_2xn_0308A .box ul').eq(0).remove() };
	
});*/
