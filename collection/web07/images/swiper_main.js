$(function () {

	/*銀行*/
	var Area2_1 = new Swiper('.top_PD_02 .change', {

        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.top_PD_02 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
		
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.top_PD_02 .swiper-button-next', 
        prevButton: '.top_PD_02 .swiper-button-prev',
		
		//排版
		slidesPerView: '1', //顯示幾個

        //自動撥放
        autoplay: 2500, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

	});
	
	/*鋪底活動*/
	var Area2_2 = new Swiper('.top_PD_01 .change', {

        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.top_PD_01 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
		
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.top_PD_01 .swiper-button-next', 
        prevButton: '.top_PD_01 .swiper-button-prev',

						   
        //基本
        grabCursor: true, //手掌游標
        loop: true, //無限循環
		
		//排版
		slidesPerView: '2', //顯示幾個

        //自動撥放
        autoplay: 2500, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

	});
	
	
	/*滿額加價購*/
	var Area1 = new Swiper('.Area1 .box', {

        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area1 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
		
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area1 .swiper-button-next', 
        prevButton: '.Area1 .swiper-button-prev',

						   
        //基本
        grabCursor: true, //手掌游標
        loop: true, //無限循環
		
		//排版
		slidesPerView: '2', //顯示幾個

        //自動撥放
        autoplay: 2500, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

	});
	
	//swiper.onResize();

});

//左右箭頭MouseOver出現
  $(function(){
	   	     $('.Area1 .button').addClass('btn_hide');
             $('.Area1').hover(function(){
				 $('.Area1').children('.button').removeClass('btn_hide');
			 },function(){
				 $('.Area1').children('.button').addClass('btn_hide');
			 });
              });

   $(function(){
	   	     $('.top_PD_02 .button').addClass('btn_hide');
             $('.top_PD_02').hover(function(){
				 $('.top_PD_02').children('.button').removeClass('btn_hide');
			 },function(){
				 $('.top_PD_02').children('.button').addClass('btn_hide');
			 });
              });     
