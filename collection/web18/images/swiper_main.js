


$(window).load(function(){
	

	var swiper = new Swiper('.swiper-container', {

        //小圓點
        pagination: '.swiper-pagination',
        paginationClickable: true, //觸擊切換
        
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.rightgogo', 
        prevButton: '.leftgogo',
  
        //基本
        initialSlide: Math.floor( Math.random() * ($('.swiper-slide').size()) ) , //初始險是第幾個(亂數)
        direction: 'vertical', //滑動方向-垂直(預設水平horizontal)
		//speed: 1000, //滑動速度(300)        
		//freeMode: true, //取消只滑動1格,但不會貼齊(要貼齊要再加Sticky)
        //freeModeSticky: true, //取消只滑動1格時也可貼齊
        //loop: true, //無限循環
          
        //排版
        slidesPerView: 3, //顯示幾個
        slidesPerGroup: 3, //一次切換幾個
        spaceBetween: 0, //間距

        //自動撥放
        autoplay: 6000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

		//RWD
        breakpoints: {
            767: {
				
				//基本
				direction: 'horizontal', //滑動方向-垂直(預設水平horizontal)
				freeMode: true, //取消只滑動1格,但不會貼齊(要貼齊要再加Sticky)
				freeModeSticky: true, //取消只滑動1格時也可貼齊
			
				//排版
				slidesPerView: 'auto', //顯示幾個
				slidesPerGroup: 1, //一次切換幾個
				spaceBetween: 10, //間距
				
				slidesOffsetBefore: 10, //左邊偏移量
				slidesOffsetAfter: 10, //右邊偏移量
				
				
            },
        },

    });
	

	
	/*活動按鈕banner*/
	var Area_topPD = new Swiper('.Area_acty2 .box_2', {

        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area_acty2 .swiper-pagination',  
        paginationClickable: true, //觸擊切換

        //基本
        grabCursor: true, //手掌游標
        loop: true, //無限循環
		
		//排版
		slidesPerView: '1', //顯示幾個
		spaceBetween: 0, //間距

        //自動撥放
        autoplay: 2500, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

	});
	swiper.onResize();
	

	


	
	
	
})
