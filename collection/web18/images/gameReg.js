var imgEcm = '';
var promoTs = '';
momoj(document).ready(function(){
	var src = momoj('#IT_js').attr('src');
	imgEcm = src.substring(0, src.indexOf('images\/'));
  promoTs = src.split('?')[1];
});

function picPath(fileName){
  return imgEcm + 'images/' + fileName + '.png?t=' + promoTs;
}

function showPic(fileName){
  momoj('#roplay img').attr('src', picPath(fileName));
}


var promoAjaxKey = false;
function promoAjax(data) {
  if(promoAjaxKey == false) {
    promoAjaxKey = true;
    var result = '-1';

    momoj.ajax({
      url : '/ajax/promotionEvent_P120190220.jsp',
      async : false,
      cache : false,
      type : 'POST',
      dataType : 'json',
      contentType : 'application/x-www-form-urlencoded; charset=big5',
      data : data,
      timeout : 30000,
      success : function(rtnData) {
        var status = rtnData.status;
        if(status == 'EXPIRED') {
          alert('請於活動時間內登入參加');
        }else if(status == 'NOT_LOGIN') {
          alert('請先登入會員');
        }else if(status == 'FULL') {
          alert('活動名額已滿');
        }else if(typeof status == 'undefined' || status == '' || status == 'ERR' || status == 'F_0' || status == 'F_1' || status == 'FAIL') {
          alert('很抱歉!伺服器暫時無法連線，請稍候再試');
        }else {
          result = rtnData;
        }
        promoAjaxKey = false;
      },
      error : function(err, msg1, msg2) {
        promoAjaxKey = false
        alert("ERROR\n很抱歉!伺服器暫時無法連線，請稍候再試");
      }
    });
    return result;
  }
}

var nameMap =   {"SQ_A"       :'包週禮包',
                   "SQ_B"       :'50萬銀幣',
                   "SQ_C"       :'20萬銀幣',
                   "D300_990"   :'300折價券',
                   "D800_1600"  :'800折價券'};


function gameButton() {
  
  momoj().MomoLogin({flag:false, LoginSuccess:function() {
  
    var data = {
      doAction : 'reg'
    };
    var rtnData = promoAjax(data);
    if(rtnData != '-1') {
      var status = rtnData.status;
      var redeem = rtnData.redeem;
      var prize = rtnData.prize;
      if(prize && prize.startsWith('SQ')){
        prize = prize.split('-')[0];
      }

      if(status == 'TAKEN') {
        alert("您已經參加過了喔~");
		momoj('#roResult').show().find('img').attr('src',picPath('taken'));		
      }else if(status == 'SUCCESS' || status == 'ONE_MORE'){
		momoj('.roMain').addClass('over');
        setTimeout(function(){//show獎品圖
           momoj('#roResult').show().find('img').attr('src',picPath(prize));
		   momoj('.roMain').removeClass('over');
        }, 1500);
        setTimeout(function(){//show alert訊息
          if(prize.startsWith('SQ') && status == 'ONE_MORE'){
            alert("恭喜你獲得" + nameMap[prize] + '\n你的序號是：' + redeem + '\n你是本月新客可以再玩一次!!');
		        init();
          }else if(prize.startsWith('SQ') && status == 'SUCCESS'){
            alert("恭喜你獲得" + nameMap[prize] + '\n你的序號是：' + redeem);
          }else if(status == 'ONE_MORE'){
            alert("恭喜你獲得" + nameMap[prize] + '\n你是本月新客可以再玩一次!!');
		        init();
          }else{
            alert("恭喜你獲得" + nameMap[prize]);
          }
        },4500);

      }
    }
  }});
}

function welcomeGift() {
  
  momoj().MomoLogin({flag:false, LoginSuccess:function() {
    var data = {
      doAction : 'gift'
    };
    
    var rtnData = promoAjax(data);
    if(rtnData != '-1') {
      var status = rtnData.status;
      var welcomeRedeem  = rtnData.redeem;
      if(status == 'TAKEN') {
        alert("您已經領取過了喔~\n您的序號是：" + welcomeRedeem);
      }else if(status == 'SUCCESS'){
        alert("恭喜您獲得333折價券一張和見面禮包序號 : " + welcomeRedeem);
      }
    }
  }});
}

function init(){
  momoj('#roResult').hide();
  momoj('.roMain').addClass('');
}

function list() {
  momoj().MomoLogin({flag:false, LoginSuccess:function() {
  
    var data = {
      doAction : 'list'
    };
  
    var rtnData = promoAjax(data);
    if(rtnData!=-1){

      var giftLt = rtnData.giftLt;
      var giftName = '';
      if(giftLt && giftLt.length > 0){
        var _tempVal = '';
        var msg = "";
        for(var i = 0 ; i<giftLt.length ; i++){
          if(giftLt[i].substring(0,1) == 'D'){
            msg = '折價券已經歸戶。';
            giftName = giftLt[i];
          }else{
            giftName = giftLt[i].split('-')[0];
            msg = giftLt[i].split('-')[1];
          }
          _tempVal += '<tr>'+
                        '<th>' + nameMap[giftName] + '</th>'+
                        '<th>' + msg + '</th>'+
                      '</tr>';
        }

        top.momoj().LayerMask({contentWidth:'100%', contentHeight:'580px'}).open();
        var showRecord = '<div class="ref">' +
                         '<div class="blackBox" style="display:block;">' +
                         '<div class="agreeArea">' +
                         '<div class="box">' +
                           '<h3>獎項查詢</h3>' +
                         '<div class="agree_table">' +
                          '<table cellspacing="0" cellpadding="0">' +
                            '<tbody>' +
                              '<tr>' +
                                '<th style="background-color:#D9D9D9">獎項</th>' +
                                '<th style="background-color:#D9D9D9">獎項說明</th>' +
                              '</tr>' +
                              _tempVal + 
                            '</tbody>' + 
                          '</table>' + 
                         '</div>' + 
                         '<div class="button but-close">' + 
                           '<a href="javascript:void(0);" onclick="closeDiv();">關閉</a>' + 
                         '</div>' + 
                         '</div>' + 
                         '</div>' + 
                         '</div>' + 
                         '</div>';
        top.momoj('#MoMoLMContent').html(showRecord).css({"position":"absolute","background-color":""});
      }else{
        alert("您無抽獎紀錄唷~");
      }
    }
  }});

}

function login(){ //登入會員
	momoj().MomoLogin({flag:false, LoginSuccess:function() {
		alert('您已成功登入!!');
	}});
}

function closeDiv() {   // 關閉浮層視窗
  top.momoj('#MoMoLMContent').empty();
  top.momoj().LayerMask().close();
}