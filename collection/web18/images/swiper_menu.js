﻿// JavaScript Document

$(function () { 
	
	var	wdth=$(window).width();
//	console.log('width:'+ wdth);
	


});


  //各slide大小需要一致
  barwidth = 36 //導航BAR
  tSpeed = 300 //切換速度300ms
  var navSwiper = new Swiper('#nav', {
  	slidesPerView:5,
  	freeMode: true,
  	on: {
  		init: function() {
  			navSlideWidth = this.slides.eq(0).css('width'); //選單寬度統一
  			bar = this.$el.find('.bar')
  			bar.css('width', navSlideWidth)
  			bar.transition(tSpeed)
  			navSum = this.slides[this.slides.length - 1].offsetLeft //最後slide的位置

  			clientWidth = parseInt(this.$wrapperEl.css('width')) 
  			navWidth = 0
  			for (i = 0; i < this.slides.length; i++) {
  				navWidth += parseInt(this.slides.eq(i).css('width'))
  			}

  			topBar = this.$el.parents('body').find('#menu_m') 

  		},

  	},
  });

  var pageSwiper = new Swiper('#page', {
  	watchSlidesProgress: true,
  	resistanceRatio: 0,
  	on: {
  		touchMove: function() {
  			progress = this.progress
  			bar.transition(0)
  			bar.transform('translateX(' + navSum * progress + 'px)')

  		},
  		transitionStart: function() {
  			activeIndex = this.activeIndex
  			activeSlidePosition = navSwiper.slides[activeIndex].offsetLeft
  			//?放??航粉色?移??渡
  			bar.transition(tSpeed)
  			bar.transform('translateX(' + activeSlidePosition + 'px)')
  			//?放?文字?色?渡
  			navSwiper.slides.eq(activeIndex).find('span').transition(tSpeed)
  			navSwiper.slides.eq(activeIndex).find('span').css('color', 'rgba(255,255,255,1)')
  			if (activeIndex > 0) {
  				navSwiper.slides.eq(activeIndex - 1).find('span').transition(tSpeed)
  				navSwiper.slides.eq(activeIndex - 1).find('span').css('color', 'rgba(255,255,255,1)')
  			}
  			if (activeIndex < this.slides.length) {
  				navSwiper.slides.eq(activeIndex + 1).find('span').transition(tSpeed)
  				navSwiper.slides.eq(activeIndex + 1).find('span').css('color', 'rgba(255,255,255,1)')
  			}
  			//?航居中
  			navActiveSlideLeft = navSwiper.slides[activeIndex].offsetLeft //activeSlide距左邊的距离

  			navSwiper.setTransition(tSpeed)
  			if (navActiveSlideLeft < (clientWidth - parseInt(navSlideWidth)) / 2) {
  				navSwiper.setTranslate(0)
  			} else if (navActiveSlideLeft > navWidth - (parseInt(navSlideWidth) + clientWidth) / 2) {
  				navSwiper.setTranslate(clientWidth - navWidth)
  			} else {
  				navSwiper.setTranslate((clientWidth - parseInt(navSlideWidth)) / 2 - navActiveSlideLeft)
  			}

  		},
  	}
  });

  navSwiper.$el.on('touchstart', function(e) {
  	e.preventDefault()
  })


   navSwiper.on('tap', function(e) {

  	clickIndex = this.clickedIndex
  	clickSlide = this.slides.eq(clickIndex)
  	pageSwiper.slideTo(clickIndex, 300);
  	this.slides.find('span').css('color', 'rgba(255,255,255,1)');
  	clickSlide.find('span').css('color', 'rgba(255,255,255,1)');
  })



		
  var scrollSwiper = new Swiper('.scroll', {

  	slidesOffsetBefore: 72,
  	direction: 'vertical',
  	freeMode: true,
  	slidesPerView: 'auto',
	  
  	mousewheel: {
  		releaseOnEdges: true,
  	},
  	on: {
  		touchMove: function() {

  			if (this.translate > 72 - 36 && this.translate < 72) {
  				topBar.transform('translateY(' + (this.translate - 72) + 'px)');
  			}

  		},
  		touchStart: function() {
  			startPosition = this.translate
  		},
  		touchEnd: function() {
  			topBar.transition(tSpeed)
  			if (this.translate > 36 && this.translate < 72 && this.translate < startPosition) {
  				topBar.transform('translateY(-36px)');
  				for (sc = 0; sc < scrollSwiper.length; sc++) {
  					if (scrollSwiper[sc].translate > 36) {
  						scrollSwiper[sc].setTransition(tSpeed);
  						scrollSwiper[sc].setTranslate(36)
  					}
  				}
  			}
  			if (this.translate > 36 && this.translate < 72 && this.translate > startPosition) {
  				topBar.transform('translateY(0px)');
  				for (sc = 0; sc < scrollSwiper.length; sc++) {
  					if (scrollSwiper[sc].translate < 72 && scrollSwiper[sc].translate > 0) {
  						scrollSwiper[sc].setTransition(tSpeed);
  						scrollSwiper[sc].setTranslate(72)
  					}
  				}

  			}






  		},

  		transitionStart: function() {

  			topBar.transition(tSpeed)
  			if (this.translate < 72 - 36) {
  				topBar.transform('translateY(-36px)');
  				if (scrollSwiper) {
  					for (sc = 0; sc < scrollSwiper.length; sc++) {
  						if (scrollSwiper[sc].translate > 36) {
  							scrollSwiper[sc].setTransition(tSpeed);
  							scrollSwiper[sc].setTranslate(36)
  						}
  					}
  				}

  			} else {
  				topBar.transform('translateY(0px)');

  				if (scrollSwiper) {
  					for (sc = 0; sc < scrollSwiper.length; sc++) {
  						if (scrollSwiper[sc].translate < 72 && scrollSwiper[sc].translate > 0) {
  							scrollSwiper[sc].setTransition(tSpeed);
  							scrollSwiper[sc].setTranslate(72)
  						}
  					}
  				}
  			}
  		},
  	}

  })


	
  var bannerSwiper = new Swiper('.banner', {
  	loop: true,
  	pagination: {
  		el: '.swiper-pagination',
  		type: 'fraction',
  		renderFraction: function(currentClass, totalClass) {
  			return '<span class="' + currentClass + '"></span>' + '/' + '<span class="' + totalClass + '"></span>';
  		},
  	},
  });



 /*導覽/選單--置頂組件*/
$.fn.topSuction = function(option) {
	option = option || {};
	var navbox = option.navbox || '.BN_Area2';		//置頂區塊Class
	var fixCls = option.fixCls || 'cate-fixed';		//置頂Class
	var fixend = option.fixend || 'body';			//結束置頂內容區塊ID或Class
	var fixedFunc = option.fixedFunc;
	var resetFunc = option.resetFunc;
	var $self = this;
	var $fixend = $(fixend);
	var $win  = $(window);
	if (!$self.length) return;
	var width = $self.width();
	var height = $self.height();
	var offset = $self.offset();
	var fTop   = offset.top;
	var fLeft  = offset.left;
	var feBottom =  $fixend.offset().top + $fixend.height()
	$self.css('z-index','200');
	if ( fixend == 'body' ){
		$self.attr('data-fix',true);
	}
	//$self.data('def', offset);
	//$win.resize(function() {
	//	$self.data('def', $self.offset());
	//});
	function fix(){
		dTop = $(document).scrollTop();	
		if ( fTop < dTop && feBottom > dTop) {
			$self.addClass(fixCls);
			$self.children(navbox).width( width );
			if ( feBottom - height < dTop ){
				var h =dTop - feBottom + height
				$self.find(navbox).css('top', h * -1 );
			} else {
				$self.find(navbox).css('top','' );
			}
			if (fixedFunc) {
				fixedFunc.call($self, fTop);
			};
		} else {
			$self.removeClass(fixCls);
			if (resetFunc) {
				resetFunc.call($self, fTop);
			};
		};
	};
	fix();
	$win.scroll(function() {
		fix();
	});
	$win.resize(function() {
		fix();
	});
	//【切換樣式】NavArea-fixed-bottom選單一開始就置底時，隱藏系統地
	if( $self.hasClass('NavArea-fixed-bottom') == true ){ $('.footerArea').hide() };
};



/*內頁頁籤開關動作*/
$(function(){
	
             $(".menu .menu_btn").click(function(){
				 $(".menu .menu_btn").removeClass('selected');
				 $(this).addClass('selected');
             var data_selector = $(".menu_btn.selected").attr('data-style');	
				 console.log(data_selector);
              //   alert("data_selector");

				 
		//swiper-slide-active		 

                                          });
	
});
