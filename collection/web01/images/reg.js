/**********************************
 2018 1212直播  ychsiung
 *********************************/
var ecmSetConfig = {};
ecmSetConfig.now = new Date();
ecmSetConfig.todayDate = ecmSetConfig.now.getDate();
ecmSetConfig.month = ecmSetConfig.now.getMonth()+1;
ecmSetConfig.Year = ecmSetConfig.now.getFullYear();
ecmSetConfig.today = ecmSetConfig.Year + "/" + ((ecmSetConfig.month.toString().length < 2)?"0":"") + ecmSetConfig.month + "/" + ((ecmSetConfig.todayDate.toString().length < 2)?"0":"") + ecmSetConfig.todayDate;
// 取得圖片位置
ecmSetConfig.itjsSrc = momoj('#itjs').attr('src');
ecmSetConfig.imgEcm = ecmSetConfig.itjsSrc.substring(0,ecmSetConfig.itjsSrc.indexOf("images\/"));
ecmSetConfig.timestamp = ecmSetConfig.itjsSrc.split('?')[1];
// 圖檔路徑
function getImgPath(imgName){
  return ecmSetConfig.imgEcm + 'images/' + imgName + '?' + ecmSetConfig.timestamp;
}

ecmSetConfig.returnMsg = {
  'D'        : '請於活動時間內參加活動',
  'W'        : '請於指定星期參加活動',
  'WP'       : '競標金額錯誤',
  'L'        : '請先登入會員',
  'A'        : '您已參加過此活動',//'您已登記過此活動',
  'A_EX'     : '您已登記過其他活動',//您已參加過其他首購活動
  'EA'       : '您不符合登記資格',
  'FULL'     : '名額已經額滿!!',//'登記已額滿',
  'NOT_USED' : '很抱歉，活動暫不開放',
  'NOT_APP'  : '請在momo APP參加活動',
  'NOT_WEB'  : '請在momo網頁版參加活動',
  'NOT_NC'   : '您非活動期間新客',
  'NOT_WFB'  : '您非活動期間首購',
  'NO_PT'    : '點數不足',
  'INS'      : '登記成功，感謝您對本活動的支持',
};

ecmSetConfig.regKey = false;
function reg(mpn, dtpn, giftcod){
  if(!ecmSetConfig.regKey){
    ecmSetConfig.regKey = true;
    momoj().MomoLogin({flag:false, LoginSuccess:function(){
      var data = {
        doAction   : 'reg',
        m_promo_no : mpn,
        dt_promo_no: dtpn,
        gift_code  : giftcod
      };
      var rtnData = promoMechAjax(data);
      if(rtnData != '-1'){
        var returnMsg = rtnData.returnMsg;
        if(returnMsg == 'INS'){                      
          alert(ecmSetConfig.returnMsg[returnMsg]);
          ecmSetConfig.regKey = false;
        }else if(returnMsg in ecmSetConfig.returnMsg){
          alert(ecmSetConfig.returnMsg[returnMsg]);
          ecmSetConfig.regKey = false;
        }else{
          alert('很抱歉，伺服器暫時無法連線，請稍候再試' + '(' + returnMsg + ')');
          ecmSetConfig.regKey = false;
        }
      }
    },LoginCancel:function(){
      ecmSetConfig.regKey = false;
    }});
  }
}
ecmSetConfig.promoMechAjaxKey = false;
function promoMechAjax(data){
  if(!ecmSetConfig.promoMechAjaxKey){
    ecmSetConfig.promoMechAjaxKey = true;
    var result = '-1';
    momoj.ajax({
      url         : '/ajax/promoMech.jsp',
      async       : false,
      cache       : false,
      type        : 'POST',
      dataType    : 'json',
      contentType : 'application/x-www-form-urlencoded; charset=big5',
      data        : data,
      timeout     : 30000,
      success     : function(rtnData){
        ecmSetConfig.promoMechAjaxKey = false;
        result = rtnData;
      },
      error       : function(err, msg1, msg2){
        ecmSetConfig.promoMechAjaxKey = false;
        alert('ERROR\n很抱歉！伺服器暫時無法連線，請稍候再試');
      }
    });
    return result;
  }
}

//----------------------------------------------------------
function live_1(){
		momoj('#showPage').show();
		var arr = ['startag_01','startag_02','startag_03','startag_04','startag_05'];
		for(var i =0;i<arr.length;i++){
		momoj("#"+arr[i]).appendTo(momoj("#showPage"));	
		}
	}

function live_2(){
		momoj('#showPage').show();
		var arr = ['startag_02','startag_03','startag_04','startag_05','startag_01'];
		for(var i =0;i<arr.length;i++){
		momoj("#"+arr[i]).appendTo(momoj("#showPage"));	
		}
	}

function live_3(){
		momoj('#showPage').show();
		var arr = ['startag_03','startag_04','startag_05','startag_01','startag_02'];
		for(var i =0;i<arr.length;i++){
		momoj("#"+arr[i]).appendTo(momoj("#showPage"));	
		}
	}

function live_4(){
		momoj('#showPage').show();
		var arr = ['startag_04','startag_05','startag_01','startag_02','startag_03'];
		for(var i =0;i<arr.length;i++){
		momoj("#"+arr[i]).appendTo(momoj("#showPage"));	
		}
	}

function live_5(){
		momoj('#showPage').show();
		var arr = ['startag_05','startag_01','startag_02','startag_03','startag_04'];   
		for(var i =0;i<arr.length;i++){
		momoj("#"+arr[i]).appendTo(momoj("#showPage"));	
		}
	}	
function pageInfo() { 
                       //老虎狗,                                    唐綺陽,                    洪百榕,                    購物專家,                     楊千霈
//  var stageStartDate =['2018/12/11 14:00:00',      '2018/12/11 21:00:00',      '2018/12/12 00:00:00',      '2018/12/12 21:00:00',      '2018/12/13 00:00:00'];
//  var stageEndDate   = ['2018/12/11 20:59:59',      '2018/12/11 23:59:59',      '2018/12/12 20:59:59',      '2018/12/12 23:59:59',      '2018/12/14 23:59:59'];
  
  var stageStartDate =['2018/12/07 15:30:00',      '2018/12/07 15:40:00',      '2018/12/07 15:50:00',      '2018/12/07 16:00:00',      '2018/12/07 16:10:00'];
  var stageEndDate    =['2018/12/07 15:39:59',      '2018/12/07 15:49:59',      '2018/12/07 15:59:59',      '2018/12/07 16:09:59',      '2018/12/07 16:19:59'];
  
  for(var i = 0, arrLength = stageStartDate.length; i < arrLength; i++) {
    var afterStart = Date.parse(ecmSetConfig.now).valueOf() >= Date.parse(stageStartDate[i]).valueOf();
    var beforeEnd  = Date.parse(ecmSetConfig.now).valueOf() <= Date.parse(stageEndDate[i]).valueOf();
    if(afterStart && beforeEnd){		
      //區塊排序
	  if( i == 0){
		live_1();  
	  }else if(i == 1){
		live_2();  
	  }else if(i == 2){
		live_3(); 
	  }else if(i == 3){
		live_4();  
	  }else if(i == 4){
		live_5();  
	  }	  		   
	}     
    }
  }

//---------------------------------------------------------------------------------------
momoj(document).ready(function(){
//  pageInfo();
});