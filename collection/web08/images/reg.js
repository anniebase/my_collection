/**********************************
 2018 11月便利達康_hhyang
 *********************************/
var promoAjaxKey = false;
function promoAjax(data) {
  if(promoAjaxKey == false) {
    promoAjaxKey = true;
    var result = '-1';

    momoj.ajax({
      url : '/ajax/promotionEvent_P220181101.jsp',
      async : false,
      cache : false,
      type : 'POST',
      dataType : 'json',
      contentType : 'application/x-www-form-urlencoded; charset=big5',
      data : data,
      timeout : 30000,
      success : function(rtnData) {
        var status = rtnData.status;
        if(status == 'EXPIRED') {
          alert('請於活動時間內登入參加');
        }else if(status == 'NOT_LOGIN') {
          alert('請先登入會員');
        }else if(typeof status == 'undefined' || status == '' || status == 'ERR' || status == 'F_0' || status == 'F_1' || status == 'FAIL') {
          alert('很抱歉!伺服器暫時無法連線，請稍候再試');
        }else if(status == 'NOT_APP') {
          alert('請使用momo購物網app參加');
        }else {
          result = rtnData;
        }
        promoAjaxKey = false;
      },
      error : function(err, msg1, msg2) {
        promoAjaxKey = false
        alert("ERROR\n很抱歉!伺服器暫時無法連線，請稍候再試");
      }
    });
    return result;
  }
}

function regA(){

  momoj().MomoLogin({flag:false, LoginSuccess:function() {
    var data = {
        doAction : 'regA'
    };
    
    var rtnData = promoAjax(data);
    if(rtnData!=-1){
      var status = rtnData.status;
      if(status=="NOT_VALID"){
        alert('抱歉，經查詢您活動區間尚未有全家/萊爾富/OK超商取貨訂單紀錄');
      }else if(status=="TAKEN"){
        alert('您已經登記過了喔!');
      }else if(status=="FULL"){
        alert('很抱歉，名額已經滿了!');
      }else if(status=="OK"){
        alert('您已登記成功！感謝對本活動的支持');
      }
      cntReg();
    }
  }});
}


function regB(){

  momoj().MomoLogin({flag:false, LoginSuccess:function() {
    var data = {
        doAction : 'regB'
    };
    
    var rtnData = promoAjax(data);
    if(rtnData!=-1){
      var status = rtnData.status;
      if(status=="NOT_VALID"){
        alert('抱歉，經查詢您活動區間尚未有全家/萊爾富/OK超商取貨訂單紀錄');
      }else if(status=="TAKEN"){
        alert('您已經登記過了喔!');
      }else if(status=="FULL"){
        alert('很抱歉，名額已經滿了!');
      }else if(status=="OK"){
        alert('您已登記成功！感謝對本活動的支持');
      }
      cntReg();
    }
  }});
}


function qry(){
  momoj('.clear').remove();
  momoj().MomoLogin({flag:false, LoginSuccess:function() {
    var data = {
        doAction : 'qry'
    };
    var rtnData = promoAjax(data);
    if(rtnData != '-1') {
      var status = rtnData.status;
      if(status == 'OK') {
        var dateArray = rtnData.date;
        var prizeArray = rtnData.prize;
        for(var i = 0; i < dateArray.length; i++){
          momoj('.ref table tbody').append('<tr class="clear"><th>' + dateArray[i] +'</th><th>' + ecmSetConfig.INS_msg[prizeArray[i]] + '</th></tr>');
        }
        momoj('.ref .blackBox').show();
      }else {
        alert('很抱歉，伺服器暫時無法連線，請稍候再試');
      }
    }
  }});
}


function cntReg(){
  var data = {
      doAction : 'chk'
  };
  var rtnData = promoAjax(data);
  if(rtnData!=-1){
    var giftCntA = rtnData.giftCntA;
    var giftCntB = rtnData.giftCntB;
    momoj('#title2 .num').html((parseInt(giftCntA)<=0)?'0':giftCntA);
    momoj('#title6 .num').html((parseInt(giftCntB)<=0)?'0':giftCntB);

//      momoj('#regimg').find('img').attr('src', getImgPath('btn_end.png'));

  }
}

//▼▼▼▼▼▼▼▼▼▼▼ without Ajax ▼▼▼▼▼▼▼▼▼▼▼
var ecmSetConfig = {};
ecmSetConfig.now = new Date();
// 取得圖片位置
ecmSetConfig.itjsSrc = momoj('#itjs').attr('src');
ecmSetConfig.imgEcm = ecmSetConfig.itjsSrc.substring(0,ecmSetConfig.itjsSrc.indexOf("images\/"));
ecmSetConfig.timestamp = ecmSetConfig.itjsSrc.split('?')[1];
// 圖檔路徑
function getImgPath(imgName){
  return ecmSetConfig.imgEcm + 'images/' + imgName + '?' + ecmSetConfig.timestamp;
}
ecmSetConfig.INS_msg = {
  'lottery'   : '登記送【威秀影城電影票】', 
  'gift_1129' : '登記送【LINE POINTS 100點】',//test
  'gift_1130' : '登記送【摩斯漢堡-冰紅茶(M)兌換券】',//test
  'gift_1204' : '登記送【摩斯漢堡-冰紅茶(M)兌換券】',
  'gift_1205' : '登記送【摩斯漢堡-冰紅茶(M)兌換券】',
  'gift_1206' : '登記送【摩斯漢堡-冰紅茶(M)兌換券】',
  'gift_1207' : '登記送【摩斯漢堡-冰紅茶(M)兌換券】',
  'gift_1208' : '登記送【摩斯漢堡-冰紅茶(M)兌換券】',
  'gift_1209' : '登記送【摩斯漢堡-冰紅茶(M)兌換券】',
  'gift_1210' : '登記送【摩斯漢堡-冰紅茶(M)兌換券】',
  'gift_1211' : '登記送【翰林茶棧 熊貓珍奶(特大杯)】',
  'gift_1212' : '登記送【LINE POINTS 100點】',
  'gift_1213' : '登記送【85度C-75元午茶組即享券】',
  'gift_1214' : '登記送【天仁喫茶趣TOGO70元飲品】'
};

var layerMask = null;
function closeDiv() { // 關閉Div視窗
  if(layerMask!=null){
    layerMask.close();
  }else{
    top.momoj().LayerMask().close(); 
  }
}
// 顯示Div視窗
function openDiv(refId) {
  var resultSet = top.momoj('#' + refId).html();
  top.momoj().LayerMask({contentWidth:'100%', contentHeight:'auto'}).open();
  top.momoj('#MoMoLMContent').empty();
  top.momoj('#MoMoLMContent').html(resultSet).css({position:"absolute", background:"transparent"});
}
function pageInfo() {
  cntReg();
  
  var stageStartDate = [     '2018/12/05',      '2018/12/12',      '2018/12/13',      '2018/12/14'];
  var stageEndDate   = [     '2018/12/11',      '2018/12/12',      '2018/12/13',      '2018/12/14'];
  var showTittleSrc  = [   'date1211.png',    'date1212.png',    'date1213.png',    'date1214.png'];
  for(var i = 0, arrLength = stageStartDate.length; i < arrLength; i++) {
    var afterStart = Date.parse(ecmSetConfig.now).valueOf() >= Date.parse(stageStartDate[i]+' 00:00:00').valueOf();
    var beforeEnd  = Date.parse(ecmSetConfig.now).valueOf() <= Date.parse(stageEndDate[i]+' 23:59:59').valueOf();
    if(afterStart && beforeEnd){
      //文案顯示時間區間不同
      momoj('#title2 .box_bg').attr('src', getImgPath(showTittleSrc[i]));
      break;
    }
  }
}
momoj(document).ready(function(){
  pageInfo();
});