
$(function () { 
});



$(window).load(function(){

	if( $(window).width() < 768 ){
	
	
		//滿版高度
		function fixPagesHeight() {
			$('.WRAPPER_swiper-wrapper .swiper-container , .WRAPPER_swiper-wrapper .swiper-slide').css({
				height: $(window).outerHeight(true) - $('.NavArea').outerHeight(true),
			})
		}
		
		//內容大於裝置高度
		function ifwH() {
			var wH = $(window).outerHeight(true) - 44 - 40;
			var con = $('.li_box').each(function(i, e) {
                var conH = $(this).outerHeight(true);
				if ( conH >= wH ){
					var i = wH / conH;
					$(this).css({
						'-webkit-transform':'scale(' + i  + ')',
						'-moz-transform':'scale(' + i  + ')',
						'-ms-transform':'scale(' + i  + ')',
						'-o-transform':'scale(' + i  + ')',
						'transform':'scale(' + i  + ')',
					});
				}
            });
		};

		
		$(window).on('resize', function() {
			fixPagesHeight();
			ifwH();
		})
		
		fixPagesHeight();
		ifwH();
		
		
		//全頁面切換
		var mySwiper = new Swiper('.WRAPPER_swiper-wrapper .swiper-container', {
	
			//小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
			pagination: '.WRAPPER_swiper-wrapper .swiper-pagination',  
			paginationClickable: true, //觸擊切換

			//左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
			nextButton: '.WRAPPER_swiper-wrapper .gobtn_next', 	

			
			//基本
        	//initialSlide: 0, //初始險是第幾個(亂數)
			//loop: true, //無限循環
			
		
			//基本
			direction: 'vertical', //滑動方向-垂直(預設水平horizontal)
			
			//特殊
			mousewheelControl: true,	//滑鼠滾輪功能
			watchSlidesProgress: true,	//啟動過程3 2 1 0 -1 -2 -3
			
	
			//初始化
			onInit: function(swiper){ //Swiper2.x的初始化是onFirstInit
				swiper.myactive = 0;
				swiperAnimateCache(swiper); //隱藏動畫元素
				swiperAnimate(swiper); //初始化完成開始動畫
				//console.log('onInit初始化');
			},
						
			//滑動時Progress回調函數
			onProgress: function(swiper) {
				for (var i = 0; i < swiper.slides.length; i++) {
					var slide = swiper.slides[i];
					var progress = slide.progress;
					var translate, boxShadow;
		
					translate = progress * swiper.height * 0.8;
					scale = 1 - Math.min(Math.abs(progress * 0.2), 1);
					opacity = progress ;
					boxShadowOpacity = 0;
					slide.style.boxShadow = '0px 0px 10px rgba(0,0,0,' + boxShadowOpacity + ')';
		
					if (i == swiper.myactive) {
						//下一頁
						es = slide.style;
						es.webkitTransform = es.MsTransform = es.msTransform = es.MozTransform = es.OTransform = es.transform = 'translate3d(0,' + (translate) + 'px,0) scale(' + scale + ')';
						es.zIndex=0;
						es.opacity = 1 - Math.min(Math.abs(opacity), 1);
					}else{
						//上一頁
						es = slide.style;
						es.webkitTransform = es.MsTransform = es.msTransform = es.MozTransform = es.OTransform = es.transform ='';
						es.zIndex=1;
						es.opacity = '';
					}
		
				}
				//console.log('onProgress滑動時Progress回調函');
			},
			
			//切換結束執行
			onTransitionEnd: function(swiper, speed) {
				for (var i = 0; i < swiper.slides.length; i++) {
					//es = swiper.slides[i].style;
					//es.webkitTransform = es.MsTransform = es.msTransform = es.MozTransform = es.OTransform = es.transform = '';
					//swiper.slides[i].style.zIndex = Math.abs(swiper.slides[i].progress);
				}
				swiper.myactive = swiper.activeIndex;
				swiperAnimate(swiper); //每個slide切換結束時也運行當前slide動畫
				//console.log('onTransitionEnd切換結束執行');
			},
			
			//切換開始執行
			onSetTransition: function(swiper, speed) {
				for (var i = 0; i < swiper.slides.length; i++) {
					//if (i == swiper.myactive) {
						es = swiper.slides[i].style;
						es.webkitTransitionDuration = es.MsTransitionDuration = es.msTransitionDuration = es.MozTransitionDuration = es.OTransitionDuration = es.transitionDuration = speed + 'ms';
					//}
				}
				//console.log('onSetTransition切換開始執行');
			},		
			
			
		});
		mySwiper.onResize();
		
		/*網址開銀行
		var urltxt = document.URL; //目前網址
		var idx = urltxt.indexOf("#") + 1; 
		urltxt = urltxt.substring(idx, idx + 6); //判斷#後1位數
		if (urltxt == "title4") {
			mySwiper.slideTo(4,300);
		}	*/
		
	} 

})
