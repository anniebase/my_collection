



/* 回版頭 */
jQuery(function(){
	jQuery("#gotop").click(function(){
		jQuery("html,body").stop(true,false).animate({scrollTop:0},700); //設定回頁面頂端
		return false;	
	});
    jQuery(window).scroll(function() {
        if ( jQuery(this).scrollTop() > 300){ //設定大於300px才顯示浮層
            jQuery('#gotop').fadeIn("fast");
        } else {
            jQuery('#gotop').stop().fadeOut("fast");
        }
    });
});




/* 滑動的GOTO */
function goTop(val) {
jQuery('html,body').animate({scrollTop:jQuery(val).offset().top},700);
}

/*滑鼠移置特定高度時，黏人精顯現*/
$(window).scroll(function() {
	//假如寬度大於1285
	if ( $(window).width() > 1285 ) {
		　　var top_position = $(this).scrollTop();
		　　if ( top_position > 530){
		　　　　// Let the item move with scrolling.
		　　　　$(".gogo").show('slow');
		　　} else {
		　　　　// Reset the position to default.
		　　　　$(".gogo").hide();
		　　}
	}
}); 

/** 延遲載圖 **/
var imglazy = ".articleList img.lazy";
$(function () {  
  $(imglazy).show().lazyload({
  effect : "fadeIn"
  });
});
/** 讀最新圖片 **/
var advtoday = new Date();
var advimg = ".articleList img";
$(function () {  
	$(advimg).each(function() {
		var advimgsrc = $(this).attr('data-original');
		$(this).attr({ 'data-original': advimgsrc +'?t=' + advtoday.getTime() });
	});
});



function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}