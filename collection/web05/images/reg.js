﻿

// 圖檔路徑
var src = momoj('#itjs').attr('src');
var imgEcm  = src.substring(0,src.indexOf("images\/"));
var promoTs = src.split('?')[1];

momoj(document).ready(function(){
  
  var todayDate = new Date();
  var semiFinalDate = new Date('2018/07/05');
  var finalDate = new Date('2018/07/09');
  if(todayDate >= semiFinalDate && todayDate < finalDate){
    momoj('.meneu').find('dd.selected').removeClass('selected');
    momoj('.meneu').find('dd').eq(1).addClass('selected');
    momoj('.advisoryArea').find('.tabcontent').hide();
    momoj('.advisoryArea').find('.tabcontent').eq(1).show();
  }else if(todayDate >= finalDate){
    momoj('.meneu').find('dd.selected').removeClass('selected');
    momoj('.meneu').find('dd').eq(2).addClass('selected');
    momoj('.advisoryArea').find('.tabcontent').hide();
    momoj('.advisoryArea').find('.tabcontent').eq(2).show();
  }else{
    momoj('.meneu').find('dd.selected').removeClass('selected');
    momoj('.meneu').find('dd').eq(0).addClass('selected');
    momoj('.advisoryArea').find('.tabcontent').hide();
    momoj('.advisoryArea').find('.tabcontent').eq(0).show();
  }
  momoj().MomoLogin({LoginSuccess:function() {
    cnt();
  }});
});

var leftTeam = {
  '0'  : 'a1A',
  '1'  : 'a2A',
  '2'  : 'a3A',
  '3'  : 'a4A',
  '4'  : 'a5A',
  '5'  : 'a6A',
  '6'  : 'a7A',
  '7'  : 'a8A',
  '8'  : 'b1A',
  '9'  : 'b2A',
  '10' : 'b3A',
  '11' : 'b4A',
  '12' : ['c1A','c1C']
}

var rightTeam = {
  '0'  : 'a1B',
  '1'  : 'a2B',
  '2'  : 'a3B',
  '3'  : 'a4B',
  '4'  : 'a5B',
  '5'  : 'a6B',
  '6'  : 'a7B',
  '7'  : 'a8B',
  '8'  : 'b1B',
  '9'  : 'b2B',
  '10' : 'b3B',
  '11' : 'b4B',
  '12' : ['c1B','c1D']
}

var teamName = {
  'a1A' : '法國',
  'a1B' : '阿根廷',
  'a2A' : '烏拉圭',
  'a2B' : '葡萄牙',
  'a3A' : '西班牙',
  'a3B' : '俄羅斯',
  'a4A' : '克羅埃西亞',
  'a4B' : '丹麥',
  'a5A' : '巴西',
  'a5B' : '墨西哥',
  'a6A' : '比利時',
  'a6B' : '日本',
  'a7A' : '瑞典',
  'a7B' : '瑞士',
  'a8A' : '哥倫比亞',
  'a8B' : '英格蘭',
  'b1A' : '法國',
  'b1B' : '烏拉圭',
  'b2A' : '巴西',
  'b2B' : '比利時',
  'b3A' : '瑞典',
  'b3B' : '英格蘭',
  'b4A' : '俄羅斯',
  'b4B' : '克羅埃西亞',
  'c1A' : '法國',
  'c1B' : '英格蘭',
  'c1C' : '比利時',
  'c1D' : '克羅埃西亞'
}


var serviceUrl = '/ajax/promotionEvent_P020180629.jsp';
var promoAjaxKey = false;
function promoAjax(data){
	if(promoAjaxKey == false) {
		promoAjaxKey = true;
		var result = '-1';
		momoj.ajax({
			url : serviceUrl,
			async : false,
			cache : false,
			type : 'POST',
			dataType : 'json',
			contentType : 'application/x-www-form-urlencoded; charset=big5',
			data : data,
			timeout : 30000,
			success : function(rtnData) {
				result = rtnData;
				promoAjaxKey = false;
			},
			error : function(err, msg1, msg2) {
				promoAjaxKey = false;
				alert("ERROR\n很抱歉!伺服器暫時無法連線，請稍候再試");
			}
		});
		return result;
	}
}

//init
function cnt(){
  var data = {
	  doAction : 'cnt'
  };
	var rtnData = promoAjax(data);
  if(rtnData != '-1') {
    
    var gameLt = rtnData.gameLt;
    var score = rtnData.score;
    momoj('.Area_top .vote_num').text(score);
    for(var i=0 ; i<gameLt.length ; i++){
      if(gameLt[i] == '0'){//尚未開始
        momoj('#game' + i + ' a .for_pc').attr('src', imgEcm + 'images/vote_btn_before.png?t=' + promoTs);
        momoj('#game' + i + ' a .for_phone').attr('src', imgEcm + 'images/vote_btn_before_m.png?t=' + promoTs);
        momoj('#game' + i + ' a').attr('href', 'javascript:void(0);');
      }else if(gameLt[i] == '1'){//可預測
        momoj('#game' + i + ' a .for_pc').attr('src', imgEcm + 'images/vote_btn.gif?t=' + promoTs);
        momoj('#game' + i + ' a .for_phone').attr('src', imgEcm + 'images/btn_go_m.png?t=' + promoTs);
        momoj('#game' + i + ' a').attr('href', 'javascript:goVote(' + i + ');');
      }else if(gameLt[i] == '2'){//預測結束
        momoj('#game' + i + ' a .for_pc').attr('src', imgEcm + 'images/vote_btn_after.png?t=' + promoTs);
        momoj('#game' + i + ' a .for_phone').attr('src', imgEcm + 'images/vote_btn_after_m.png?t=' + promoTs);
        momoj('#game' + i + ' a').attr('href', 'javascript:void(0);');
      }
    }
    var predectedGameIdx = rtnData.predectedGameIdx;
    if(typeof(predectedGameIdx) != 'undefined'){
      for(var k=0 ; k<predectedGameIdx.length ; k++){
        momoj('#game' + predectedGameIdx[k] + ' a .for_pc').attr('src', imgEcm + 'images/vote_btn_done.png?t=' + promoTs);
        momoj('#game' + predectedGameIdx[k] + ' a .for_phone').attr('src', imgEcm + 'images/btn_go.png?t=' + promoTs);
        momoj('#game' + predectedGameIdx[k] + ' a').attr('href', 'javascript:void(0);');
      }
    }
    
  }
}

function listVote(){
  momoj().MomoLogin({LoginSuccess:function() {
    var data = {
	    doAction : 'cnt'
    };
	  var rtnData = promoAjax(data);
    if(rtnData != '-1') {
    	top.momoj().LayerMask({ contentWidth:'100%', contentHeight:'auto'}).open();
	    top.momoj('#MoMoLMContent').empty();
      var predectionTeamLt = rtnData.predectionTeamLt;
      var predectedGameIdx = rtnData.predectedGameIdx;
      if(typeof(predectionTeamLt) != 'undefined' && typeof(predectedGameIdx) != 'undefined'){
        var _divTpl = '<div class="blackBox agree_more" style="display: block">                                       '+
                      '<div class="agreeArea">                                                                        '+
                      '<div class="box">                                                                              '+
                      '<h3>預測查詢<span class="close"><a href="javascript:closeDiv();">X 關閉</a></span></h3>        '+
                      '<div class="txtArea">                                                                          '+
                      '<table>                                                                                        '+
                      '<tbody>                                                                                        '+
                      '<tr><td>階段</td><td>組別</td><td>預測隊伍</td></tr>                                           '+
                      '<tr><td rowspan="8">八強<br class="for_phone">預測</td><td>第一組</td><td><p id="gameP0"></p></td></tr>'+
                      '<tr><td>第二組</td><td><p id="gameP1"></p></td></tr>                                           '+
                      '<tr><td>第三組</td><td><p id="gameP2"></p></td></tr>                                           '+
                      '<tr><td>第四組</td><td><p id="gameP3"></p></td></tr>                                           '+
                      '<tr><td>第五組</td><td><p id="gameP4"></p></td></tr>                                           '+
                      '<tr><td>第六組</td><td><p id="gameP5"></p></td></tr>                                           '+
                      '<tr><td>第七組</td><td><p id="gameP6"></p></td></tr>                                           '+
                      '<tr><td>第八組</td><td><p id="gameP7"></p></td></tr>                                           '+
                      '<tr><td rowspan="4">四強<br class="for_phone">預測</td><td>第一組</td><td><p id="gameP8"></p></td></tr>'+
                      '<tr><td>第二組</td><td><p id="gameP9"></p></td></tr>                                           '+
                      '<tr><td>第三組</td><td><p id="gameP10"></p></td></tr>                                          '+
                      '<tr><td>第四組</td><td><p id="gameP11"></p></td></tr>                                          '+
                      '<tr><td rowspan="1">冠軍<br class="for_phone">預測</td><td>-</td><td><p id="gameP12"></p></td></tr>'+
                      '</tbody>			                                                                                  '+
                      '</table>                                                                                       '+
                      '</div>                                                                                         '+
                      '<div class="button but-close"><a href="javascript:closeDiv();">關閉</a></div>                     '+
                      '</div>                                                                                         '+
                      '</div>                                                                                         '+
                      '</div>	                                                                                        ';
      }
    }
    top.momoj('#MoMoLMContent').html(_divTpl).css({position:"absolute", background:"transparent"});
    for(var i=0 ; i<13 ; i++){
      if(predectedGameIdx.indexOf(i.toString()) > -1){
        var pTeam = predectionTeamLt[predectedGameIdx.indexOf(i.toString())].replace('_done','');
        momoj('#gameP' + i).text(teamName[pTeam]);
      }else{
        momoj('#gameP' + i).text('-');
      }
    
    }
  }});


}

function goVote(gameIdx){
  momoj().MomoLogin({LoginSuccess:function() {
	  top.momoj().LayerMask({ contentWidth:'100%', contentHeight:'auto'}).open();
	  top.momoj('#MoMoLMContent').empty();
    if(gameIdx != 12){
      var _divTpl = '<div id="" style="">'+
                    '  <div class="hide_box">'+
                    '    <div class="contentArea">'+
                    '      <div class="closeBN"><a href="javascript:closeDiv();">×</a></div>'+
                    '      <div class="box">'+
                    '        <a class="flag_A"          href="javascript:reg(\'' + gameIdx + '\', \'' + leftTeam[gameIdx] + '\')"><img src="'+ imgEcm +'images/FLAG_' + leftTeam[gameIdx] + '.png?t=' + promoTs + '"></a>'+
		                '	       <a class="win_btn_A go_bt" href="javascript:reg(\'' + gameIdx + '\', \'' + leftTeam[gameIdx] + '\')"><img src="'+ imgEcm +'images/win_btn.png"></a>'+
                    '        <a class="flag_B"          href="javascript:reg(\'' + gameIdx + '\', \'' + rightTeam[gameIdx] + '\')"><img src="'+ imgEcm +'images/FLAG_' + rightTeam[gameIdx] + '.png?t=' + promoTs + '"></a>'+
		                '	       <a class="win_btn_B go_bt" href="javascript:reg(\'' + gameIdx + '\', \'' + rightTeam[gameIdx] + '\')"><img src="'+ imgEcm +'images/win_btn.png"></a>'+			  
                    '        <img class="vs for_phone" src="'+ imgEcm +'images/vs.png">'+
                    '      </div>'+
                    '    </div>'+
                    '  </div>'+
                    '</div>';
    }else if(gameIdx == 12){
      var _divTpl = '<div id="" style="">'+
                    '  <div class="hide_box2">'+
                    '    <div class="contentArea">'+
                    '      <div class="closeBN"><a href="javascript:closeDiv();">×</a></div>'+
                    '      <div class="box">'+
                    '        <a class="flag_A"          href="javascript:reg(\'' + gameIdx + '\', \'' + leftTeam[gameIdx][0] + '\')"><img src="'+ imgEcm +'images/FLAG_' + leftTeam[gameIdx][0] + '.png?t=' + promoTs + '"></a>'+
		                '	       <a class="win_btn_A go_bt" href="javascript:reg(\'' + gameIdx + '\', \'' + leftTeam[gameIdx][0] + '\')"><img src="'+ imgEcm +'images/win_btn.png"></a>'+
                    '        <a class="flag_B"          href="javascript:reg(\'' + gameIdx + '\', \'' + rightTeam[gameIdx][0] + '\')"><img src="'+ imgEcm +'images/FLAG_' + rightTeam[gameIdx][0] + '.png?t=' + promoTs + '"></a>'+
		                '	       <a class="win_btn_B go_bt" href="javascript:reg(\'' + gameIdx + '\', \'' + rightTeam[gameIdx][0] + '\')"><img src="'+ imgEcm +'images/win_btn.png"></a>'+			  
                    '        <a class="flag_C"          href="javascript:reg(\'' + gameIdx + '\', \'' + leftTeam[gameIdx][1] + '\')"><img src="'+ imgEcm +'images/FLAG_' + leftTeam[gameIdx][1] + '.png?t=' + promoTs + '"></a>'+
		                '	       <a class="win_btn_C go_bt" href="javascript:reg(\'' + gameIdx + '\', \'' + leftTeam[gameIdx][1] + '\')"><img src="'+ imgEcm +'images/win_btn.png"></a>'+		
                    '        <a class="flag_D"          href="javascript:reg(\'' + gameIdx + '\', \'' + rightTeam[gameIdx][1] + '\')"><img src="'+ imgEcm +'images/FLAG_' + rightTeam[gameIdx][1] + '.png?t=' + promoTs + '"></a>'+
		                '	       <a class="win_btn_D go_bt" href="javascript:reg(\'' + gameIdx + '\', \'' + rightTeam[gameIdx][1] + '\')"><img src="'+ imgEcm +'images/win_btn.png"></a>'+		
                    '        <img class="vs for_phone" src="'+ imgEcm +'images/vs.png">'+
                    '      </div>'+
                    '    </div>'+
                    '  </div>'+
                    '</div>';
    }
    top.momoj('#MoMoLMContent').html(_divTpl).css({position:"absolute", background:"transparent"});
  }});
}

function reg(gameIdx, voteTeam){	//投票

	momoj().MomoLogin({LoginSuccess:function() {
    if(confirm('您是否要預測此隊伍?')){
		  var data = {
		  	doAction : 'reg',
        gameIdx  : gameIdx,
        voteTeam : voteTeam
		  };
		  var rtnData = promoAjax(data);
      
		  if(rtnData != '-1') {
		  	var rtnMags = rtnData.status;
        var score = rtnData.score;
		  	if(rtnMags == 'NOT_LOGIN'){
		  		alert('請先登入會員/加入會員!!');
		  	}else if(rtnMags == 'EXPIRED'){
		  		alert('現在非此比賽預測時間!!');
		  	}else if(rtnMags == 'ERRTeam'){
		  		alert('投票隊伍錯誤!!');
		  	}else if(rtnMags == 'SUCCESS'){
          if(rtnData.pointCnt == '6'){
            alert('預測成功！恭喜您獲得500元折價券(單品1200享折)');
          }else if(rtnData.pointCnt == '12'){
            alert('預測成功！恭喜您獲得500元折價券(單品990享折)');
          }else {
            alert('預測成功！');
          }
          cnt();
          momoj('.Area_top .vote_num').text(score);
		  	}else if(rtnMags == 'TAKEN'){
		  		alert('此場比賽您已經預測過囉!!');
		  	}else if(rtnMags == 'ERR'){
		  		alert('很抱歉!伺服器暫時無法連線，請稍候再試。');
		  	}
		  }
    }
	}});
  
}

function list(){   //查詢
	momoj().MomoLogin({LoginSuccess:function() {
		var data = {
			doAction : 'voteDays'
		};
		var rtnData = promoAjax(data);
    
		if(rtnData != '-1') {
      var score = rtnData.score;
      momoj('.Area_top .vote_num').text(score);
      top.momoj().LayerMask({ contentWidth:'100%', contentHeight:'auto'}).open();
	    top.momoj('#MoMoLMContent').empty();
      var _divTpl = '<div class="floatBox">'+
                    '  <div><a class="closeButton" href="javascript:closeDiv();">×</a></div>'+
                    '  <div class="title">集點查詢</div>'+
                    '    <div class="txt">'+
                    '      <div class="title2">─集點說明─</div>'+
                    '        <ul style="list-style:decimal;width: 92%;margin: 0 auto;letter-spacing:">'+
                    '          <li>活動期間，每組比賽可預測一次，成功預測一組比賽可累積1點。</li>'+
                    '          <li>集滿6點，可獲得$500折價券1張(部分單品$1200以上可使用)。</li>'+
                    '          <li>集滿12點，可再獲得500元折價券1張(部分單品$990以上可使用)。</li>'+
                    '        </ul>'+
                    '    </div>'+
                    '    <div class="txt">'+
                    '      <div class="title3">─集點紀錄─</div>'+
                    '    </div>'+
                    '    <div class="dateArea clearfix">'+
                    '      <div><table><tr>'+
                    '        <td>1.</td>'+
                    '        <td>2.</td>'+
                    '        <td>3.</td>'+
                    '        <td>4.</td>'+
                    '        <td>5.</td>'+
                    '      </tr><tr>'+
                    '        <td><img id="game0" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game1" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game2" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game3" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game4" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '      </tr></table></div>'+
                    '      <div><table><tr>'+
                    '        <td>6.</td>'+
                    '        <td>7.</td>'+
                    '        <td>8.</td>'+
                    '        <td>9.</td>'+
                    '        <td>10.</td>'+
                    '      </tr><tr>'+
                    '        <td><img id="game5" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game6" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game7" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game8" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game9" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '      </tr></table></div>'+
                    '      <div><table><tr>'+
                    '        <td></td>'+
                    '        <td>11.</td>'+
                    '        <td>12.</td>'+
                    '        <td>13.</td>'+
                    '        <td></td>'+
                    '      </tr><tr>'+
                    '        <td></td>'+
                    '        <td><img id="game10" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game11" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td><img id="game12" src="'+ imgEcm +'images/ball_b.png"></td>'+
                    '        <td></td>'+
                    '      </tr></table></div>'+
                    '  </div>'+
                    '</div>';
      top.momoj('#MoMoLMContent').html(_divTpl).css({position:"absolute", background:"transparent"});
      var voteDays = rtnData.voteDays;
      for(var i = 0 ; i < voteDays.length; i++) { //集點印泥
        momoj('#game' + voteDays[i].REMARK).attr('src', imgEcm +'images/ball_a.png?t='+ Math.random());
      }          
                    
    }

	}});
}

// fbshare or lineshare
function share(ShareToWhere) {
	if(ShareToWhere != 'facebook' && ShareToWhere != 'line') {
		return;
	}
	if(ShareToWhere == 'facebook') {
		shareUtil.edmFbShare();
	}else if (ShareToWhere == 'line') {
		shareUtil.edmLineShare();
	}
}

function closeDiv(){	//查詢關閉紐
	top.momoj().LayerMask().close();
}