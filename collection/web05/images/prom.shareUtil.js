﻿var fb_key = ".icon-fb";
var line_key = ".icon-line";

var edmShareDescription = "「2018瘋世足」預測搶獎金拿好禮！";//後端只接字串
var edmShareCaption = "「2018瘋世足」預測搶獎金拿好禮！";//後端只接字串

var fbCidOid="&cid=vote0629&oid=fbshare";
var lineCidOid="&cid=vote0629&oid=lineshare";

var shareUtil = {
	/**
	* 分享主活動頁 fb tulee 2015年8月5日
	*/
	edmFbShare : function() {
		var urlStr = edmShare.getUrl();//分享網址
		var hashtag = '#2018瘋世足預測搶獎金拿好禮！';
		var quote = '「2018瘋世足」預測搶獎金拿好禮！';
		if(!edmShare.isApp()) {
			fbUtilWang.setFbData(urlStr + fbCidOid, hashtag, quote);
		}else {
			edmShare.app(edmShareDescription, edmShareCaption, urlStr + fbCidOid);
		}
	},
	/**
	* 分享主活動頁 line tulee 2015年8月5日
	*/
	edmLineShare : function() {
		var urlStr = edmShare.getUrl();//分享網址
		var lineShareCaption = '「2018瘋世足」預測搶獎金拿好禮！';
		if(!edmShare.isApp()) {      
			window.open('//line.naver.jp/R/msg/text/?'.concat(encodeURIComponent(edmShareDescription + " " + lineShareCaption)).concat(encodeURIComponent(' ')).concat(encodeURIComponent(urlStr+lineCidOid)));
		}else {
			edmShare.app(edmShareDescription, lineShareCaption, urlStr + lineCidOid);
		}
	}
};