
$(function () {
	
	/*版頭銀行*/
	var Area_acty_00_Swiper = new Swiper('.Area_acty_00 .Area_acty_00_box', {
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area_acty_00 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area_acty_00 .button-next', 
        prevButton: '.Area_acty_00 .button-prev',
         //切換特效(淡化)
        effect: 'fade',     //切換特效 fade(淡化) cube(立方體) coverflow(3D) flip(翻牌)
        fade: {
            crossFade: true //打開自動淡出
        },
        autoplay: 3000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
	});

	/*促銷BN輪播*/
	var acty2_swiper = new Swiper('.acty2 .swiper-container', {

        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
		pagination: '.acty2 .swiper-pagination',
		paginationClickable: true, //觸擊切換
			
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
		nextButton: '.acty2 .button-next',
		prevButton: '.acty2 .button-prev',
		
		//基本
		initialSlide: Math.floor( Math.random() * ($('.acty2 .swiper-slide').size()) ) , //初始險是第幾個(亂數)
		loop: true, //無限循環
		autoHeight: true, //自動高度

		//排版
		slidesPerView: 1, //顯示幾個
		spaceBetween: 8, //間距

        //自動撥放
        autoplay: 2500, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播


	});



})
