
$(function () { 

});



$(window).load(function(){
	
	/*版頭銀行*/
	var boxbank_swiper = new Swiper('.bank_box .box_bank_', {
		//基本
        direction: 'vertical', //滑動方向-垂直(預設水平horizontal),最大包的Class要搭配設定swiper-container-vertical
		loop: true, //無限循環
		
        //自動撥放
        autoplay: 5000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
	});	
	
	
	/*簽到活動*/
	var Area_acty_01_box_Swiper = new Swiper('.Area_acty_01_box', {
        //基本
        loop: true, //無限循環
        //自動撥放
        autoplay: 4000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
		
        //切換特效(翻牌)
        effect: 'flip', //切換特效 cube(立方體) coverflow(3D) flip(翻牌)
        centeredSlides: true, //目前區塊居中
        slidesPerView: 'auto', //顯示改回自動
        flip: {
            slideShadows : false,	//slides的陰影。默認true。
            //limitRotation : false,	//限制最大旋轉角度為180度，默認true。
        },		
	});
	
	
	/*加價購*/
	var Area_addbuy_Swiper = new Swiper('.Area1 .box .content ', {
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area1 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area1 .swiper-button-next', 
        prevButton: '.Area1 .swiper-button-prev',
		//基本
      //  initialSlide: Math.floor( Math.random() * ($('.Area1 .swiper-slide').size()) ) , //初始險是第幾個(亂數)
		//排版
        slidesPerView: 2, //顯示幾個
        slidesPerGroup: 2, //一次切換幾個
        spaceBetween: 0, //間距
        //自動撥放
        autoplay: 5000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
	});
	
	
	/*強檔回饋*/
	var Area_topPD2_Swiper = new Swiper('.Area_topPD2 .box', {
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area_topPD2 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area_topPD2 .swiper-button-next', 
        prevButton: '.Area_topPD2 .swiper-button-prev',
		//基本
        initialSlide: Math.floor( Math.random() * ($('.Area_topPD2 .swiper-slide').size()) ) , //初始險是第幾個(亂數)
		speed:1000, //滑動速度(300)
        direction: 'vertical', //滑動方向-垂直(預設水平horizontal)
        //自動撥放
        autoplay: 5000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
        //視差效果
        parallax: true, //視差效果 標籤內加上data-swiper-parallax="-200"
	});
	
	
	/*銀行*/
	var Area_bank_Swiper = new Swiper('.Area2 .box_2', {
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area2.swiper-pagination',  
        paginationClickable: true, //觸擊切換
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area2 .swiper-button-next', 
        prevButton: '.Area2 .swiper-button-prev',
        //基本
        initialSlide: new Date().getDay()-1 , //停在今天
        loop: true, //無限循環
		centeredSlides: true, //目前區塊居中
        //排版
        slidesPerView: 3, //顯示幾個
        slidesPerGroup: 1, //一次切換幾個
        spaceBetween: 0, //間距
		//自動撥放
       // autoplay: 5000, //自動輪播間隔時間
      //  autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
	});
	
		/*各課鋪底*/
	var Area_acty_04_Swiper = new Swiper('.Area4 .box .content', {
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area4 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area4 .swiper-button-next', 
        prevButton: '.Area4 .swiper-button-prev',
        //基本
        loop: true, //無限循環
		        //排版
        slidesPerView: 4, //顯示幾個
        slidesPerGroup: 1, //一次切換幾個
        spaceBetween: 0, //間距
        //自動撥放
        autoplay: 3000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
	});	
	
	/*熱門活動*/
	var Area_acty_05_Swiper = new Swiper('.Area5 .box .content', {
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area5 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area5 .swiper-button-next', 
        prevButton: '.Area5 .swiper-button-prev',
        //基本
 //       loop: true, //無限循環
		//排版
        slidesPerView: 2, //顯示幾個
        slidesPerGroup:2, //一次切換幾個
        spaceBetween: 0, //間距
        //自動撥放
        autoplay: 3000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
	});	
	
	/*熱門活動*/
	var Area_acty_22_Swiper = new Swiper('.Area2_2 .box ', {
        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area2_2 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area2_2 .button-next', 
        prevButton: '.Area2_2 .button-prev',
        //基本
        loop: true, //無限循環
		        //排版
        slidesPerView: 1, //顯示幾個
        slidesPerGroup:1, //一次切換幾個
        spaceBetween: 0, //間距
        //自動撥放
        autoplay: 3000, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播
	});	
	
	
})
