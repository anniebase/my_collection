$(function () {

	/*APP限定搶*/
	var Area_topPD = new Swiper('.Area_topPD .box', {

        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area_topPD .swiper-pagination',  
        paginationClickable: true, //觸擊切換
        
        
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area_topPD .swiper-button-next', 
        prevButton: '.Area_topPD .swiper-button-prev',		
		
		//排版
		slidesPerView: '1', //顯示幾個

        //自動撥放
        //autoplay: 2500, //自動輪播間隔時間
        //autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

	});
	
	/*活動按鈕2銀行*/
	var Area_topPD = new Swiper('.Area_acty2 .box_3', {

        //小圓點-白點swiper-pagination-white, 黑點swiper-pagination-black
        pagination: '.Area_acty2 .swiper-pagination',  
        paginationClickable: true, //觸擊切換
        
        //左右切換-白色箭頭swiper-button-white, 黑色箭頭swiper-button-black
        nextButton: '.Area_acty2 .swiper-button-next', 
        prevButton: '.Area_acty2 .swiper-button-prev',		

        //基本
        grabCursor: true, //手掌游標
        loop: true, //無限循環
		
		//排版
		slidesPerView: '1', //顯示幾個

        //自動撥放
        autoplay: 2500, //自動輪播間隔時間
        autoplayDisableOnInteraction: false, //觸擊後還是會再自動輪播

	});
	
	
	//swiper.onResize();

})